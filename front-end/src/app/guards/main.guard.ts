/*  ******************************************************************************************************
    Rafael Féliz

    Main routes guard
    This will be triggered on every route change on Angular app, whether the route is defined with MainGuard or not on the routes definition.

    This guard will do:

    * Subscribe to the authState. By default, the authState.isAuthenticated is not set (null).
        - authState.isAuthenticated is set to true by the app-init.service.ts file. It will be set after logging in and validating 2 factor.
        - authState.isAuthenticated is set to false when the authentication is rejected
        - authState.isAuthenticated is null the first time you go to the app and it is still not set. This will be set as mentioned above, on the app-init.service.ts.
          The app should wait for the code that loads the customer info on the app-init service, and this is why below there is a IF statement
          that checks for true, false, or other than both. If other than both means authState.isAuthenticated is not set, so it will wait a little bit
          with a timeout to give time for the customer info to load (if it is the case).
          *** Each of above conditions are needed for specific app conditions that needs to be served.
              To test the timeout feature, you can go to below link, and add a button that navigates to the / route, and this will trigger the timeout, because isAuthenticated is still not set here (in case you're not logged in)
              http://localhost:4200/auth-actions?mode=revertSecondFactorAddition&oobCode=5eW9R4baUzs9aLfiZDZ752J1HXUH5nt-u09fepD6HMYAAAF3W3J3Cg&apiKey=AIzaSyDM8iHBN31JbyDHdTamn2UtbnfBgevdi1U&lang=en



    ******************************************************************************************************
*/

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromRoot from '../store/app.reducer';
import { AngularFireAuth } from '@angular/fire/auth';
import { PATH_LOGIN, PATH_NOT_ALLOWED } from '../constants/routing-paths.constants';

@Injectable({
  providedIn: 'root'
})
export class MainGuard implements CanActivate {

  redirectTimeout;
  subscription;
  constructor(private store: Store<fromRoot.State>, private afAuth: AngularFireAuth, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return new Promise(resolve => {
      this.subscription = this.store.select(fromRoot.getAuthState).subscribe(authState => {

        // ----------------------------------
        // IS AUTHENTICATED
        // If isAuthenticated is null, the promise will not be
        // completed until the authState.isAuthenticated has value

        if (authState.isAuthenticated === true) {

          //wait the customer info to load, because the auth state isAuthenticated property is set before the customerInfo is loaded.
          if (authState.customerInfoWasLoaded) {


            // --------------------
            // TODO: Add new guard validation code here, such as permissions, and any other router guard that works for authenticated users.
            // --------------------


            //Allow the route
            this.unsubscribe();
            resolve(true); //allow the route
          }
        }

        // ----------------------------------
        // IS NOT AUTHENTICATED
        else if (authState.isAuthenticated === false) {
          this.unsubscribe();
          console.info("Route not allowed ********");
          resolve(this.router.createUrlTree(['/' + PATH_NOT_ALLOWED])); //UrlTree is a way to reject guards, it tells the guard to redirect to that route
        }

         // ----------------------------------
        // AUTH STATE PROPERTY "isAuthenticated" is still not set and the requested route has this guard set on the route definition... Waiting for it to be set, otherwise, return to login.
        else {
          // It will allow some seconds to load customer info, and otherwise, it will redirect to login.
          console.info("Loading new route... waiting for customer info to load, if any...");
          this.redirectTimeout = setTimeout(() => {
            console.info("Route not allowed (not logged in) ********");
            resolve(this.router.createUrlTree(['/' + PATH_LOGIN]));
          }, 10000);
        }
      });
    });
  }

  // ===============================
  /**
   * Remove subscriptions if needed
   */
  unsubscribe = () => {
    //stop the redirectTimeout if it is still waiting from previous request
    if (this.redirectTimeout)
      clearTimeout(this.redirectTimeout);

    //Remove this subscription that waits until this if triggers
    if (this.subscription)
      this.subscription.unsubscribe();
  }
}

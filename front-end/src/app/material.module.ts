// ***************************************************
// Add every Angular-material modules here

/*
Common usages:

    Snackbar:
        const snack = this.snackBar.open(message, reloadStr, {
                  duration: 60000,
                  panelClass: 'warning'
                });
                snack
                  .onAction()
                  .subscribe(() => {
                    window.location.reload();
                  });


    Tooltip:
         <button type="button" mat-button 
                  matTooltip="{{'login.rememberMe' | translate}}"
                  matTooltipPosition="right">



*/


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule,MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatRippleModule } from '@angular/material/core';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatBadgeModule } from '@angular/material/badge';
import { MatChipsModule } from '@angular/material/chips';

// ***************************************************
// CONSTANTS

//Change input field style, instead of line below the field, add a rectangle box over the input field (Outline - bordered)
const appearance: MatFormFieldDefaultOptions = {
  appearance: 'outline'
};


// ***************************************************
// MODULE

@NgModule({
  declarations: [],
  imports: [CommonModule],
  exports: [
    MatButtonModule,
    MatInputModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatSelectModule,
    MatCardModule,
    MatListModule,
    MatMenuModule,
    MatCheckboxModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatButtonToggleModule,
    MatAutocompleteModule,
    MatRippleModule,
    MatProgressBarModule,
    MatBottomSheetModule,
    MatBadgeModule,
    MatChipsModule
  ],
  providers:[
        {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: appearance},
        {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 3000, horizontalPosition: 'right',verticalPosition: 'top'}}
    ]
})
export class MaterialModule { }


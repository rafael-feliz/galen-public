import { Component, OnInit, ViewEncapsulation, AfterViewInit, NgZone, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { take } from 'rxjs/operators';
import * as _ from 'lodash';
import { GICON_FA_QUESTIONCIRCLE, GICON_FA_CHECK } from 'src/app/constants/font-awesome.constants';
import { PATH_LOGIN } from 'src/app/constants/routing-paths.constants';
import { LoginService } from '../login/login.service';
import { TwoFactorSetupService } from './two-factor-setup.service';
import firebase from 'firebase/app';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import * as fromRoot from '../../store/app.reducer';
import { Store } from '@ngrx/store';
import { PhoneNumberIsValid } from 'src/app/helpers/forms.validators';
import { AppInitService } from 'src/app/services/app-init.service';
import { MiscService } from 'src/app/services/misc/misc.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-two-factor-setup',
  templateUrl: './two-factor-setup.component.html',
  styleUrls: ['./two-factor-setup.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TwoFactorSetupComponent implements OnInit, OnDestroy, AfterViewInit {

  twoStepForm: FormGroup;
  twoStepSmsCodeVerificationForm: FormGroup;
  twoStepVerificationStage: 'sendSms' | 'validateCode' | 'validated' = 'sendSms';
  message = "";
  messageType = "";
  twoFactorSubmitting = false;
  twoFactorIsDisabled = false;
  twoFactorIsDisabledIntervalRef;
  twoFactorIsDisabledIntervalCount = 60;
  twoStepHeadingTitle = "";
  verificationId = "";
  GICON_FA_QUESTIONCIRCLE = GICON_FA_QUESTIONCIRCLE;
  GICON_FA_CHECK = GICON_FA_CHECK;
  PATH_LOGIN = PATH_LOGIN;
  appVerifier;
  slowInternetTimeout;

  constructor(private formBuilder: FormBuilder,
    private tileService: Title,
    private afAuth: AngularFireAuth,
    private loginService: LoginService,
    private translateService: TranslateService,
    private twoFactorSetupService: TwoFactorSetupService,
    private router: Router,
    private store: Store<fromRoot.State>,
    private ngZone: NgZone,
    private appInitService: AppInitService,
    private miscService: MiscService) { }

  //=========================================================
  ngOnInit(): void {

    //Set the page title
    this.translateService.get('appSetup.title').pipe(take(1)).subscribe(value => { this.tileService.setTitle(value) });

    //Set the heading title
    const fullName = this.afAuth.user.pipe(take(1)).subscribe(user => {
      const firstName = user.displayName ? user.displayName.split(" ")[0] : "";
      //Set the heading title
      this.translateService.get('appSetup.2stepTitle').pipe(take(1)).subscribe(value => this.twoStepHeadingTitle = _.replace(value, '{$1}', firstName));
    });

    //Create the 2 step verification form
    this.twoStepForm = this.formBuilder.group({
      phone: ['', [Validators.required]]
    }, {
      validator: PhoneNumberIsValid('phone')
    });

    //Create the 2 step verification form (For validating the SMS code)
    this.twoStepSmsCodeVerificationForm = this.formBuilder.group({
      code: ['', [Validators.required]]
    });

    //wake get customer info to prevent cold start
    this.appInitService.wakeFromColdStartGetLoggedInUserCustomerInfo();

  }

  //===================================================================

  ngOnDestroy() {
    if (this.twoFactorIsDisabledIntervalRef)
      clearInterval(this.twoFactorIsDisabledIntervalRef);
  }


  //=========================================================
  ngAfterViewInit(): void {
    //Setup the reCAPTCHA, it is needed on the obSubmitTwoStep method
    this.appVerifier = new firebase.auth.RecaptchaVerifier('recaptchaContainer', {
      'size': 'invisible',
      'callback': function (response) {
        // reCAPTCHA solved
      }
    });
  }

  //=========================================================
  //Try sending verification code again
  tryAgain = () => {
    const context = this;

    if (!this.twoFactorIsDisabled) {
      context.twoStepVerificationStage = 'sendSms';
      context.message = '';
      context.twoStepForm.reset();
      context.twoStepForm.controls.phone.setErrors(null);
      context.twoStepSmsCodeVerificationForm.reset();
      context.twoStepSmsCodeVerificationForm.controls.code.setErrors(null);
    }

  }

  //=========================================================
  logOut = () => {
    this.loginService.logout();
  }

  //=========================================================
  onSubmitTwoStep = () => {
    if (!this.twoStepForm.valid || this.twoStepForm.controls.phone.value === "") {
      return;
    }


    this.twoFactorSubmitting = true;
    this.slowInternetTimeout = setTimeout(() => { if (this.twoFactorSubmitting) this.miscService.showSlowInternetConnectionSnackBar() }, 40000); //If processing recaptcha takes longer than usual


    this.twoFactorSetupService.validatePhone(this.twoStepForm.controls.phone.value, this.appVerifier).then(result => {
      this.twoFactorSubmitting = false;
      this.message = result.message;
      this.messageType = result.status ? 'success' : 'warning';


      //Clear the timeout because an action was completed
      if (this.slowInternetTimeout)
        clearTimeout(this.slowInternetTimeout);

      if (result.status) {
        this.verificationId = result.verificationId;
        this.twoStepVerificationStage = 'validateCode';
        this.twoFactorIsDisabled = true;

        //Now set a interval to allow the user to click try again.
        this.twoFactorIsDisabledIntervalRef = setInterval(() => {
          if (this.twoFactorIsDisabledIntervalCount < 1) {
            clearInterval(this.twoFactorIsDisabledIntervalRef);
            this.twoFactorIsDisabled = false; //allow to send sms again
            this.twoFactorIsDisabledIntervalCount = 60; //reset count
          }
          this.twoFactorIsDisabledIntervalCount--;
        }, 1000);

      }

    })

  };

  //=========================================================
  onSubmitTwoStepSmsCodeVerification = () => {
    const context = this;
    if (!this.twoStepSmsCodeVerificationForm.valid || this.twoStepSmsCodeVerificationForm.controls.code.value === "") {
      return;
    }

    //Get default name and then validate the SMS code
    this.translateService.get('appSetup.2stepDefaultName').pipe(take(1)).subscribe(twoStepDefaultName => {
      context.twoFactorSubmitting = true;
      const phoneNumber = context.twoStepForm.controls.phone.value;
      const factorName = _.replace(twoStepDefaultName, '{$1}', phoneNumber.substring(phoneNumber.length - 4)); //Takes the last 4 digits of the phone
      context.twoFactorSetupService.enroll(context.verificationId, context.twoStepSmsCodeVerificationForm.controls.code.value, factorName).then(result => {
        context.twoFactorSubmitting = false;
        context.message = result.message;
        context.messageType = result.status ? 'success' : 'warning';

        if (result.status) {
          context.twoStepVerificationStage = 'validated';

          //allow some time for the animation to complete (The animation that says that your phone has been validated)
          setTimeout(async () => {

            //Get the auth state to check whether the customerInfo is set or not. If it is not set, redirect to the page to set it up.
            await context.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(async loggedInfo => {
              if (loggedInfo.customerInfo) {
                //Redirect the user to the app
                context.ngZone.run(() => context.router.navigate(["/"]));
              }

              //If above if doesn't go, then the loggedInfo.customerInfo is not available (Customer is not created) It will be redirected to the customer-setup view by the app-init.service

            });

          }, 2000);
        }
      })
    });
  };

}

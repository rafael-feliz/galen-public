import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import { take } from 'rxjs/operators';
import { FirebaseAuthService } from '../services/firebase-auth.service';
import * as _ from 'lodash';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../store/app.reducer';
import * as authActions from '../../auth/store/auth.actions';

@Injectable({
  providedIn: 'root'
})
export class TwoFactorSetupService {

  constructor(private afAuth: AngularFireAuth,
    private firebaseAuthService: FirebaseAuthService,
    private store: Store<fromRoot.State>) { }


   //============================================================================
   //SEND A SMS TO THE PROVIDED PHONE AND GET VERIFICATION ID
  //Params usage:
  //The appVerifier is setup like this: var appVerifier = new firebase.auth.RecaptchaVerifier(recaptchaContainer);, see https://cloud.google.com/identity-platform/docs/web/mfa for help
  validatePhone = async (phoneNumber: string, appVerifier: firebase.auth.RecaptchaVerifier): Promise<{ status: boolean, message: string, verificationId: string }> => {

    const context = this;

    return new Promise(resolve => {
      context.afAuth.useDeviceLanguage();
      context.afAuth.user.pipe(take(1)).subscribe(async user => {

        user.multiFactor.getSession().then(function (multiFactorSession) {
          // Specify the phone number and pass the MFA session.
          var phoneInfoOptions = {
            phoneNumber: '+1' + _.replace(phoneNumber, /\D/g, ''),
            session: multiFactorSession
          };
          var phoneAuthProvider = new firebase.auth.PhoneAuthProvider();
          // Send SMS verification code.
          return phoneAuthProvider.verifyPhoneNumber(phoneInfoOptions, appVerifier);
        })
          .then(function (verificationId) {
            resolve({ status: true, message: '', verificationId: verificationId });
          })
          .catch(function (error) {
            context.firebaseAuthService.translateFirebaseErrorMessages(error).then(result => {
              resolve({ status: false, message: result, verificationId: '' });
            })
          });
      });
    });

  }
   //============================================================================
   //VALIDATE TWO FACTOR AUTHENTICATION ENROLLMENT

  enroll = (verificationId: string, verificationCode: string, mfaDisplayName:string): Promise<{ status: boolean, message: string }> => {
    const context = this;
    return new Promise(resolve => {
      context.afAuth.useDeviceLanguage();
      context.afAuth.user.pipe(take(1)).subscribe(async user => {
        // Ask user for the verification code.
        var cred = firebase.auth.PhoneAuthProvider.credential(verificationId, verificationCode);
        var multiFactorAssertion = firebase.auth.PhoneMultiFactorGenerator.assertion(cred);
        // Complete enrollment.
        user.multiFactor.enroll(multiFactorAssertion, mfaDisplayName).then(async result => {

            //Store in the state that it has two factor enrolled
            context.store.dispatch(new authActions.SetTwoFactorEnrolled({ twoFactorAuthenticationEnrolled: true }));

          resolve({ status: true, message: '' });
        })
        .catch(function (error) {
          context.firebaseAuthService.translateFirebaseErrorMessages(error).then(result => {
            resolve({ status: false, message: result });
          })
        });
      });
    });
  }

}

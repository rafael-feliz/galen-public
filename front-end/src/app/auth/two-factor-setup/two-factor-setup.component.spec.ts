import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoFactorSetupComponent } from './two-factor-setup.component';

describe('TwoFactorSetupComponent', () => {
  let component: TwoFactorSetupComponent;
  let fixture: ComponentFixture<TwoFactorSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoFactorSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoFactorSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

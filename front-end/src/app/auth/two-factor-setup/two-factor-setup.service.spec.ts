import { TestBed } from '@angular/core/testing';

import { TwoFactorSetupService } from './two-factor-setup.service';

describe('TwoFactorSetupService', () => {
  let service: TwoFactorSetupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TwoFactorSetupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

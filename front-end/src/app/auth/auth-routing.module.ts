import { PATH_LOGIN,
        PATH_AUTH_ACTIONS,
        PATH_PASSWORD_RESET,
        PATH_REGISTER,
        PATH_TWO_FACTOR_SETUP,
        PATH_CUSTOMER_SETUP} from '../constants/routing-paths.constants';

// AUTH ROUTING MODULE ***********************************

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component'
import { AuthActionsComponent } from './auth-actions/auth-actions.component';
import { RegisterComponent } from './register/register.component';
import { TwoFactorSetupComponent } from './two-factor-setup/two-factor-setup.component';
import { CustomerSetupComponent } from './customer-setup/customer-setup.component';
import { MainGuard } from '../guards/main.guard';

const routes: Routes = [
  {path: PATH_LOGIN, component: LoginComponent},
  {path: PATH_AUTH_ACTIONS, component: AuthActionsComponent},
  {path: PATH_PASSWORD_RESET, component: AuthActionsComponent},
  {path: PATH_REGISTER, component: RegisterComponent},
  {path: PATH_TWO_FACTOR_SETUP, component: TwoFactorSetupComponent, canActivate: [MainGuard]},
  {path: PATH_CUSTOMER_SETUP, component: CustomerSetupComponent, canActivate: [MainGuard]},
  {path: PATH_CUSTOMER_SETUP + '/:step', component: CustomerSetupComponent, canActivate: [MainGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }

/*  ******************************************************************************************************
    Rafael Féliz

    ******************************************************************************************************
*/
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { take } from 'rxjs/operators';
import * as _ from 'lodash';
import { TranslateService } from '@ngx-translate/core'
import { FirebaseAuthService } from '../services/firebase-auth.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private afAuth: AngularFireAuth,
    private translateService: TranslateService,
    private firebaseAuthService:FirebaseAuthService) { }

  //=========================================================
  //Create user and send Confirmation link

  createUserAndSendEmailConfirmLink(fullName: string,
    email: string,
    password: string): Promise<{ success: boolean, message: string }> {

    return new Promise(resolve => {
      this.afAuth.createUserWithEmailAndPassword(email, password).then(result => {
      var context = this;

        if (result.user) {

          //-----------------------
          //Set user display name

          result.user.updateProfile({
            displayName: fullName
          }).then(function () {
            // Update user information successfuly.

            context.firebaseAuthService.sendActionLink(email, fullName, 'emailVerification').then(sendingLinkResult => {
              if (sendingLinkResult.success) {
                context.afAuth.signOut();
                // send successful.
                context.translateService.get('register.success').pipe(take(1)).subscribe(value => {
                  resolve({ success: true, message: _.replace(value, '{$1}', email) });
                });
              } else {
                result.user.delete();
                resolve({ success: false, message: _.replace(sendingLinkResult.message, '{$1}', email) });
              }
            });

            //-----------------------
            //Set user display name - ERRORS

          }).catch(function (error) {
            // An error happened, remove the user that was created
            result.user.delete().then(function () {
              // delete successful.

              //Get the result from firebase, and look for a language with that string
              context.firebaseAuthService.translateFirebaseErrorMessages(error).then(value => {
                resolve({ success: false, message: value });
              });

            }).catch(function (error) {
              // Delete unsuccessful
              //Get the result from firebase, and look for a language with that string
              context.firebaseAuthService.translateFirebaseErrorMessages(error).then(value => {
                resolve({ success: false, message: value });
              });
              context.afAuth.signOut();
            });
          });
        } else {
          //The user value is not set
          context.translateService.get('general.genericError').pipe(take(1)).subscribe(value => {
            resolve({ success: false, message: value });
          });
        }
      })
        .catch((error) => {
          //Get the result from firebase, and look for a language with that string
          this.firebaseAuthService.translateFirebaseErrorMessages(error).then(value => {
            resolve({ success: false, message: value });
          });

        });
    });
  }

}

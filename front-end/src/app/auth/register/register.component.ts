/*  ******************************************************************************************************
    Rafael Féliz

    First stage of user registration

    ******************************************************************************************************
*/
import { Component, OnInit, ViewEncapsulation, NgZone } from '@angular/core';
import { RegisterService } from './register.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { take } from 'rxjs/operators';
import { MustMatch, PasswordIsValid } from 'src/app/helpers/forms.validators';
import { Router } from '@angular/router';
import { PATH_LOGIN } from '../../constants/routing-paths.constants'
import firebase from 'firebase/app';
import { MiscService } from 'src/app/services/misc/misc.service';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  registerMessage = "";
  registerMessageType = "";
  sendingValidation = false;
  PATH_LOGIN = PATH_LOGIN;
  clickedSubmit = false;
  passIsFocus = false;
  registerVerifier: firebase.auth.RecaptchaVerifier;
  slowInternetTimeout;

  constructor(private translateService: TranslateService,
    private registerService: RegisterService,
    private formBuilder: FormBuilder,
    private tileService: Title,
    private router: Router,
    private ngZone: NgZone,
    private miscService: MiscService) { }


  ngOnInit(): void {
    //Set the page title
    this.translateService.get('register.pageTitle').pipe(take(1)).subscribe(value => { this.tileService.setTitle(value) });

    //Create the request form
    this.registerForm = this.formBuilder.group({
      fullName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
      acceptTerms: [false, Validators.requiredTrue]
    }, {
      validator: [MustMatch('password', 'confirmPassword'), PasswordIsValid('password')]
    });

  }


  //===================================================================


  async onSubmit() {

    //Clear the timeout because an action was completed
    if (this.slowInternetTimeout)
      clearTimeout(this.slowInternetTimeout);


    this.clickedSubmit = true;
    if (!this.registerForm.valid || !this.registerForm.controls.fullName.value || !this.registerForm.controls.acceptTerms.value) {
      return;
    }

    if (!environment.production) {
      if (this.registerForm.controls.email.value.indexOf("@galen.app") == -1) {
        this.registerMessage = "You're on a dev environment and you should test with @galen emails";
        this.registerMessageType = "warning";
        return;
      }
    }


    this.sendingValidation = true;
    this.slowInternetTimeout = setTimeout(() => { if (this.sendingValidation) this.miscService.showSlowInternetConnectionSnackBar() }, 40000); //If processing recaptcha takes longer than usual

    //-------------------------
    //Check recaptcha challenge.
    if (!this.registerVerifier) {
      this.registerVerifier = new firebase.auth.RecaptchaVerifier('registerRecaptchaContainer', {
        'size': 'invisible'
      });
      this.registerVerifier.render();
    }

    await this.registerVerifier.verify().then(async result => {

      const context = this;
      await this.registerService.createUserAndSendEmailConfirmLink(this.registerForm.controls.fullName.value,
        this.registerForm.controls.email.value,
        this.registerForm.controls.password.value).then(result => {


          //Clear the timeout because an action was completed
          if (this.slowInternetTimeout)
            clearTimeout(this.slowInternetTimeout);


          context.registerMessage = result.message;
          context.registerMessageType = result.success ? "success" : "warning";
          context.sendingValidation = false;

          if (result.success) {
            context.clickedSubmit = false;
            setTimeout(() => {
              context.ngZone.run(() => context.router.navigate(["/" + PATH_LOGIN]));
            }, 8000);
          }

        }).catch(function (error) {
          context.registerMessage = error.message;
          context.registerMessageType = "error";
          context.sendingValidation = false;

          //Clear the timeout because an action was completed
          if (this.slowInternetTimeout)
            clearTimeout(this.slowInternetTimeout);

        });

    }).catch(error => {
      //Clear the timeout because an action was completed
      if (this.slowInternetTimeout)
        clearTimeout(this.slowInternetTimeout);

        this.translateService.get('login.recaptchaError').pipe(take(1)).subscribe(recaptchaErrorStr => {
        this.registerMessage = recaptchaErrorStr;
        this.registerMessageType = "error";
        this.sendingValidation = false;
      });
    });




  }

}

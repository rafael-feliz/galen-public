import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AuthRoutingModule } from './auth-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '../material.module';
import { I18nModule } from '../i18n/i18n.module';
import { RightPeopleComponent } from './common/right-people/right-people.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { AuthActionsComponent } from './auth-actions/auth-actions.component';
import { RegisterComponent } from './register/register.component';
import { TwoFactorSetupComponent } from './two-factor-setup/two-factor-setup.component';
import { CustomerSetupComponent } from './customer-setup/customer-setup.component';
import { SharedModule } from '../shared.module';


@NgModule({
  declarations: [
    LoginComponent,
    RightPeopleComponent,
    AuthActionsComponent,
    RegisterComponent,
    TwoFactorSetupComponent,
    CustomerSetupComponent
  ],
  imports: [
    AuthRoutingModule,
    CommonModule,
    FontAwesomeModule,
    MaterialModule,
    FlexLayoutModule,
    I18nModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  exports:[
    LoginComponent
  ]
})
export class AuthModule { }

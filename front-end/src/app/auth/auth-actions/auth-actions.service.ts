/*  ******************************************************************************************************
    Rafael Féliz

    ******************************************************************************************************
*/

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireFunctions } from '@angular/fire/functions';
import { take } from 'rxjs/operators';
import { FirebaseAuthService } from '../services/firebase-auth.service';
import { CLOUD_FUNCTION_AUTH_REGISTER_USER_WELCOME_MESSAGE, CLOUD_FUNCTION_AUTH_ACTIONS, CLOUD_FUNCTION_CREATE_CUSTOMER, CLOUD_FUNCTION_AUTH_SET_PASSWORD_RESET_SUCCESS } from '../../constants/cloud-functions.constants';
import { TranslateService } from '@ngx-translate/core'
import { ErrorService } from 'src/app/services/errors/error.service';
import { GalenFEncrypt } from '../../../../../galen-shared/functions/encrypt.functions';
import { GALENC_ENCRIPTION_SET_PASSWORD_RESET_SUCCESS } from '../../../../../galen-shared/constants/encryption.constant';

interface changePassParamObj { password: string, actionCode: string };


@Injectable({
  providedIn: 'root'
})
export class AuthActionsService {

  constructor(private afAuth: AngularFireAuth,
    private afFunctions: AngularFireFunctions,
    private translateService: TranslateService,
    private firebaseAuthService: FirebaseAuthService,
    private errorService: ErrorService) { }

  //==================================================================================

  sendRecoveryLink(email: string): Promise<{ success: boolean, message: string }> {
    return new Promise(resolve => {
      this.afAuth.useDeviceLanguage();


      this.firebaseAuthService.sendActionLink(email, '', 'passwordReset').then(result => {

        if (result.success) {
          this.translateService.get('login.resetPasswordSuccess').pipe(take(1)).subscribe(value => {
            resolve({ success: true, message: value });
          });
        } else {
          resolve({ success: false, message: result.message });
        }
      })
        .catch((error) => {
          if (error.code == "auth/user-not-found")
            this.translateService.get('login.emailNotFound').pipe(take(1)).subscribe(value => {
              resolve({ success: false, message: value });
            });
          else if (error.code == "auth/network-request-failed")
            this.translateService.get('login.networkRequestFailed').pipe(take(1)).subscribe(value => {
              resolve({ success: false, message: value });
            });
          else
          this.translateService.get('general.genericError').pipe(take(1)).subscribe(value => {
              let message = value + error.message;
              resolve({ success: false, message: message });
            });
        });
    });
  }


  //==================================================================================

  changePassword(changePassParamsObj): Promise<{ success: boolean, message: string }> {
    return new Promise(resolve => {
      this.afAuth.useDeviceLanguage();

      //First, verify the action code
      this.afAuth.verifyPasswordResetCode(changePassParamsObj.actionCode).then(() => {

        //--------------------
        //Set the user email that it should not change the password anymore (set flag to false)
        const encryptedEmail = GalenFEncrypt(changePassParamsObj.email, GALENC_ENCRIPTION_SET_PASSWORD_RESET_SUCCESS);
        this.afFunctions.httpsCallable(CLOUD_FUNCTION_AUTH_SET_PASSWORD_RESET_SUCCESS)(encryptedEmail).toPromise().then(() => { return true });

        //---------------------
        //Now change the password and confirm result
        this.afAuth.confirmPasswordReset(changePassParamsObj.actionCode,
          changePassParamsObj.password).then(result => {
            this.translateService.get('login.resetPasswordChangePasswordSuccess').pipe(take(1)).subscribe(value => {

              resolve({ success: true, message: value });
            });
          })
          .catch((error) => {
            //Get the result from firebase, and look for a language with that string
            this.firebaseAuthService.translateFirebaseErrorMessages(error).then(value => {
              resolve({ success: false, message: value });
            });

          });
      }).catch((error) => {
        //Get the result from firebase, and look for a language with that string
        this.firebaseAuthService.translateFirebaseErrorMessages(error).then(value => {
          resolve({ success: false, message: value });
        });

      });

    });
  }


  //==================================================================================
  //Validates the user email after registering, with Firebase.
  //Sends an email with a cloud function passing the userEmail and other params

  async validateEmail(actionCode: string, userEmail: string, fullName: string, language: string): Promise<{ success: boolean, message: string }> {

    const context = this;

    return new Promise(async resolve => {
      context.afAuth.useDeviceLanguage();

      await context.afAuth.applyActionCode(actionCode).then(async result => {

        //---------------------------
        //Create the customer if the user does not have claims

        await context.afFunctions.httpsCallable(CLOUD_FUNCTION_CREATE_CUSTOMER)(userEmail).toPromise().then(async response => {
          if (response.success) {

            //---------------------------
            //Send a welcome email
            const data = {
              email: userEmail,
              name: fullName,
              lang: language
            }
            await context.afFunctions.httpsCallable(CLOUD_FUNCTION_AUTH_REGISTER_USER_WELCOME_MESSAGE)(data).toPromise().then(res => {
              resolve({ success: true, message: '' });
            }).catch(() => {
              resolve({ success: false, message: '' });
            });

          } else {
            //***** This should never be triggered *****
            //Create customer error
            context.errorService.showCustomErrorMessages("Could not create the customer");
            await context.translateService.get('register.verifyingEmailErrorCreatingCustomer').pipe(take(1)).subscribe(value => {
              resolve({ success: false, message: value });
            });
          }
        });



      }).catch((error) => {
        context.translateService.get('register.verifyingEmailError').pipe(take(1)).subscribe(value => {
          resolve({ success: false, message: value });
        });
      });
    });
  }


  //==================================================================================
  //Revert two factor authentication

  async otherActionCodeProcessing(actionCode: string, apiKey: string, language: string): Promise<{ success: boolean, message: string }> {

    const context = this;
    return new Promise(async resolve => {
      context.afAuth.useDeviceLanguage();

      await context.afAuth.applyActionCode(actionCode).then(async result => {
        context.translateService.get('general.validatingLinkSuccess').pipe(take(1)).subscribe(value => {
          resolve({ success: true, message: value });
        });
      }).catch((error) => {
        context.translateService.get('general.validatingLinkError').pipe(take(1)).subscribe(value => {
          resolve({ success: false, message: value });
        });
      });
    });
  }
}

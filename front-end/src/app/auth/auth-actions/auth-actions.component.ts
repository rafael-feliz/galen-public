/*  ******************************************************************************************************
    Rafael Féliz

    Used to:
  - Start password reset, continue password reset after getting the email and clicking the link
  - Validate email after user creation
  - Others...

  https://firebase.google.com/docs/auth/custom-email-handler
  https://firebase.google.com/docs/reference/js/firebase.auth.ActionCodeInfo

    ******************************************************************************************************
*/



import { Component, OnInit, NgZone, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { take } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthActionsService } from './auth-actions.service'
import { MustMatch, PasswordIsValid } from '../../helpers/forms.validators';
import { PATH_LOGIN } from '../../constants/routing-paths.constants';
import { TranslateService } from '@ngx-translate/core'
import * as _ from 'lodash';
import firebase from 'firebase/app';
import { MiscService } from 'src/app/services/misc/misc.service';

@Component({
  selector: 'app-auth-actions',
  templateUrl: './auth-actions.component.html',
  styleUrls: ['./auth-actions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AuthActionsComponent implements OnInit {

  sendingReset = false;
  verifying = true;
  message = "";
  messageType = "";
  resetPasswordForm: FormGroup;
  resetPasswordCompleteForm: FormGroup;
  rightPeopleType = "forgot-password";
  welcomeAnimation = "";
  mode = "";
  actionCode = "";
  apiKey = "";
  continueUrl = "";
  lang = "en";
  continueUrlParams: URLSearchParams;
  PATH_LOGIN = PATH_LOGIN;
  resetPasswordVerifier: firebase.auth.RecaptchaVerifier;
  passIsFocus: boolean;
  slowInternetTimeout;

  constructor(private titleService: Title,
    private translateService: TranslateService,
    private authActionsService: AuthActionsService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private ngZone: NgZone,
    private miscService: MiscService) { }

  ngOnInit(): void {

    //Set the page title
    this.translateService.get('login.resetPasswordPageTitle').pipe(take(1)).subscribe(value => this.titleService.setTitle(value));

    //Get query string parameters
    this.route.queryParams.pipe(take(1)).subscribe(params => {
      this.mode = params['mode'];
      this.actionCode = params['oobCode'];
      this.apiKey = params['apiKey'];
      this.continueUrl = params['continueUrl'];

      //This query string is a URL containing the EMAIL and NAME from the user
      //that was registered. The URL is not used to redirect, instead just to get these parameters
      this.continueUrlParams = new URLSearchParams(this.continueUrl);

      //NOTE: Use language from CONTINUE URL, not from the actual url...
      this.lang = decodeURIComponent(this.continueUrlParams.get('lang'));

      //-----------------------------------
      //Handle other requests
      this.validateRequest();
    });

    //Create the reset password form
    this.resetPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });

    //Create the reset password set password form
    this.resetPasswordCompleteForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required]],
    }, {
      validator: [MustMatch('password', 'confirmPassword'), PasswordIsValid('password')]
    });

  }



  //===================================================================


  //===================================================================
  //Handle other requests
  //https://firebase.google.com/docs/reference/js/firebase.auth.ActionCodeInfo

  //2 factor authentication enrollment
  //https://cloud.google.com/identity-platform/docs/web/mfa#signing_users_in_with_a_second_factor

  private async validateRequest() {
    if (this.mode == "verifyEmail") {
      //EMAIL VALIDATION
      //Set the page title
      this.translateService.get('register.verifyingEmailTitle').pipe(take(1)).subscribe(value => this.titleService.setTitle(value));

      this.rightPeopleType = "register-validating";
      this.verifying = true;

      //Decode query strings from URL param
      const userEmail = decodeURIComponent(this.continueUrlParams.get('email'));
      const fullName = decodeURIComponent(this.continueUrlParams.get('name'));

      // Validate the email with Firebase
      await this.authActionsService.validateEmail(this.actionCode, userEmail, fullName, this.lang).then(result => {
        this.verifying = false;
        this.message = result.message;
        this.messageType = result.success ? "success" : "warning";

        if (result.success) {
          setTimeout(() => {
            this.welcomeAnimation = "animate__animated  animate__bounce";
          }, 1000);

          this.rightPeopleType = "register-success";
        } else {
          this.rightPeopleType = "register-error";
        }
      }).catch(err => {
        this.translateService.get('general.genericError').pipe(take(1)).subscribe(genericError => {
          this.message = genericError + err ? (err.message !== "" ? err.message : "") : "";
          this.messageType = "warning";
          this.rightPeopleType = "register-error";
        });
      });

    } else if (_.toString(this.mode) === '' || this.mode === 'resetPassword') {
      //Reset password
      this.rightPeopleType = "forgot-password";

    } else {
      //OTHER VALIDATION
      //Set the page title
      this.translateService.get('general.appTitle').pipe(take(1)).subscribe(value => this.titleService.setTitle(value));

      this.rightPeopleType = "register-validating";
      this.verifying = true;

      await this.authActionsService.otherActionCodeProcessing(this.actionCode, this.apiKey, this.lang).then(result => {
        this.verifying = false;
        this.message = result.message;
        this.messageType = result.success ? "success" : "warning";

        if (result.success) {
          setTimeout(() => {
            this.welcomeAnimation = "animate__animated  animate__bounce";
          }, 1000);

          this.rightPeopleType = "register-validated";
        } else {
          this.rightPeopleType = "register-error";
        }
      }).catch(err => {
        this.translateService.get('general.genericError').pipe(take(1)).subscribe(genericError => {
          this.message = + err ? (err.message !== "" ? err.message : "") : "";
          this.messageType = "warning";
          this.rightPeopleType = "register-error";
        });
      });
    }
  }

  //===================================================================
  async onSubmit() {
    //Clear the timeout because an action was completed
    if (this.slowInternetTimeout)
      clearTimeout(this.slowInternetTimeout);

    // stop here if form is invalid
    if (this.resetPasswordForm.invalid || !this.resetPasswordForm.controls.email.value) {
      return;
    }
    this.sendingReset = true;
    this.slowInternetTimeout = setTimeout(() => { if (this.sendingReset) this.miscService.showSlowInternetConnectionSnackBar() }, 40000); //If processing recaptcha takes longer than usual
    //-------------------------
    //Check recaptcha challenge.
    if (!this.resetPasswordVerifier) {
      this.resetPasswordVerifier = new firebase.auth.RecaptchaVerifier('resetPasswordRecaptchaContainer', {
        'size': 'invisible'
      });
      this.resetPasswordVerifier.render();
    }
    await this.resetPasswordVerifier.verify().then(async result => {
      const context = this;

      await this.authActionsService.sendRecoveryLink(this.resetPasswordForm.controls.email.value).then(result => {

        //Clear the timeout because an action was completed
        if (this.slowInternetTimeout)
          clearTimeout(this.slowInternetTimeout);

        context.message = result.message;
        context.messageType = result.success ? "success" : "warning";
        context.sendingReset = false;

      }).catch(function (error) {


        //Clear the timeout because an action was completed
        if (this.slowInternetTimeout)
          clearTimeout(this.slowInternetTimeout);
        context.message = error.message;
        context.messageType = "error";
        context.sendingReset = false;
      });

    }).catch(error => {
      //Clear the timeout because an action was completed
      if (this.slowInternetTimeout)
        clearTimeout(this.slowInternetTimeout);

        this.translateService.get('login.recaptchaError').pipe(take(1)).subscribe(recaptchaErrorStr => {
        this.message = recaptchaErrorStr;
        this.messageType = "error";
        this.sendingReset = false;
      });
    });


  }


  //===================================================================
  async onSubmitNewPassword() {

    // stop here if form is invalid
    if (this.resetPasswordCompleteForm.invalid || !this.resetPasswordCompleteForm.controls.password.value) {
      return;
    }
    this.sendingReset = true;
    const userEmail = decodeURIComponent(this.continueUrlParams.get('email'));

    let params = {
      email: userEmail,
      password: this.resetPasswordCompleteForm.controls.password.value,
      actionCode: this.actionCode
    }

    await this.authActionsService.changePassword(params).then(result => {
      this.message = result.message;
      this.messageType = result.success ? "success" : "warning";
      this.sendingReset = false;

      if (result.success) {
        setTimeout(() => {
          this.ngZone.run(() => this.router.navigate(["/" + PATH_LOGIN]));
        }, 3000);
      }
    });

  }

}


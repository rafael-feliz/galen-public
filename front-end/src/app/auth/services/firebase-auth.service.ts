/*  ******************************************************************************************************
    Rafael Féliz

    All the firebase auth related code requests in one place

    ******************************************************************************************************
*/

import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core'
import { take } from 'rxjs/operators';
import { CLOUD_FUNCTION_AUTH_ACTIONS } from 'src/app/constants/cloud-functions.constants';
import { AngularFireFunctions } from '@angular/fire/functions';
import * as fromRoot from '../../store/app.reducer';
import { Store } from '@ngrx/store';

@Injectable({
  providedIn: 'root'
})
export class FirebaseAuthService {


  constructor(private translateService: TranslateService,
    private afFunctions: AngularFireFunctions,
    private store: Store<fromRoot.State>) { }


  //==================================================================================
  //Translate firebase error messages into custom string

  translateFirebaseErrorMessages = (error: any): Promise<string> => {
    const context = this;
    return new Promise(resolve => {

      //Get the result from firebase, and look for a language with that string
      if ((error.code == "auth/email-already-exists" || error.code == "auth/email-already-in-use"))
        context.translateService.get('register.emailExists').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/user-not-found")
        context.translateService.get('login.emailNotFound').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/invalid-password" || error.code == "auth/wrong-password")
        context.translateService.get('login.invalidPassword').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/too-many-requests")
        context.translateService.get('login.tooManyRequests').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/user-disabled" || error.code == "auth/operation-not-allowed")
        context.translateService.get('login.userDisabled').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/network-request-failed")
        context.translateService.get('login.networkRequestFailed').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/expired-action-code" || error.code == "auth/invalid-action-code")
        context.translateService.get('login.resetPasswordChangePasswordInvalidParams').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/invalid-verification-code" || error.code == "auth/missing-verification-code")
        context.translateService.get('twoFactor.invalidVerificationCode').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/requires-recent-login")
        context.translateService.get('twoFactor.requiresRecentLogin').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/invalid-phone-number" || error.code == "auth/missing-phone-number")
        context.translateService.get('twoFactor.invalidPhoneNumber').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/captcha-check-failed")
        context.translateService.get('twoFactor.captchaCheckFailed').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/quota-exceeded")
        context.translateService.get('twoFactor.quotaExceeded').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/maximum-second-factor-count-exceeded")
        context.translateService.get('twoFactor.maximunTwoFactorQuotaCountExceeded').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else if (error.code == "auth/second-factor-already-in-use")
        context.translateService.get('twoFactor.secondFactorAlreadyInUse').pipe(take(1)).subscribe(value => {
          resolve(value);
        });
      else
        context.translateService.get('general.genericError').pipe(take(1)).subscribe(value => {
          resolve(value + error.message ? error.message : '');
        });
    });
  }



  //=========================================================
  //Send an action link
  //It would send the email verification link, or the password reset link

  sendActionLink = async (userEmail: string,
    userFullName: string,
    type: 'emailVerification' | 'passwordReset'): Promise<{ success: boolean, message: string }> => {

    const context = this;
    return new Promise(async resolve => {

      this.store.select(fromRoot.getBrowserLanguage).subscribe(async val => {
        const language = val;

        const data = {
          email: userEmail,
          name: userFullName,
          lang: language,
          type: type ? type : 'emailVerification'
        }

        await context.afFunctions.httpsCallable(CLOUD_FUNCTION_AUTH_ACTIONS)(data).toPromise().then(res => {
          if (res.success) {
            resolve({ success: true, message: '' });
          } else {
            resolve({ success: false, message: res.message });
          }
        }).catch(function (error) {
          context.translateFirebaseErrorMessages(error).then(value => {
            resolve({ success: false, message: value });
          });
        });

        // user.sendEmailVerification(actionCodeSettings).then(function () {
        //   resolve({ success: true, message: '' });
        // }).catch(function (error) {
        //   //Get the result from firebase, and look for a language with that string
        //   context.translateFirebaseErrorMessages(error).then(value => {
        //     resolve({ success: false, message: value });
        //   });
        // });

      });

    });

  }
}

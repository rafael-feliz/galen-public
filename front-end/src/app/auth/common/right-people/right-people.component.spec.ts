import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightPeopleComponent } from './right-people.component';

describe('RightPeopleComponent', () => {
  let component: RightPeopleComponent;
  let fixture: ComponentFixture<RightPeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightPeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightPeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

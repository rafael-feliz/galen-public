/*
The right panel for login pages that contain people illustration

Attributes:

type =>               login, forgot-password... see below all the types
small-background =>   true to move the background a little to the right

*/


import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'auth-right-people',
  templateUrl: './right-people.component.html',
  styleUrls: ['./right-people.component.scss']
})
export class RightPeopleComponent implements OnInit {

  @Input("type")  type: "login" | "forgot-password" | "register" | "register2" | "register-validating" | "two-factor-setup" | "register-validated";
  @Input("small-background")  smallBackground: boolean;

  loaded = false;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    if(!this.loaded)
      setTimeout(() => {
          this.loaded = true;
      },10);
  }

}

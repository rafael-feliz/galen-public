/*  ******************************************************************************************************
    Rafael Féliz

    ******************************************************************************************************
*/

import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';
import { LoginService } from './login.service'
import { GICON_FA_TOGGLEON, GICON_FA_TOGGLEOFF } from '../../constants/font-awesome.constants'
import { PATH_PASSWORD_RESET, PATH_REGISTER } from '../../constants/routing-paths.constants'
import { MatSelectHasValue } from 'src/app/helpers/forms.validators';
import { TranslateService } from '@ngx-translate/core'
import firebase from 'firebase/app';
import { MiscService } from 'src/app/services/misc/misc.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: []
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  twoStepForm: FormGroup;
  signingIn = false;
  twoFactorSendingSms = false;
  twoFactorSubmitting = false;
  twoFactorIsDisabled = false;
  twoFactorIsDisabledIntervalRef;
  twoFactorIsDisabledIntervalCount = 60;
  rememberMe = false;
  twoStepRequired = false;
  twoStepResolver: firebase.auth.MultiFactorResolver;
  loginMessage = "";
  loginMessageType = "";
  ligthFaToggleOff = GICON_FA_TOGGLEOFF;
  ligthFaToggleOn = GICON_FA_TOGGLEON;
  PATH_PASSWORD_RESET = PATH_PASSWORD_RESET;
  PATH_REGISTER = PATH_REGISTER;
  loginVerifier: firebase.auth.RecaptchaVerifier;
  twoStepAppVerifier: firebase.auth.RecaptchaVerifier;
  sendSmsStr = '';
  twoStepSmsVerificationId = '';
  slowInternetTimeout;
  @ViewChild("code") codeInput: ElementRef;


  constructor(private titleService: Title,
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private translateService: TranslateService,
    private miscService: MiscService) { }


  //===================================================================
  ngOnInit() {

    //Set the page title
    this.translateService.get('login.appTitle').pipe(take(1)).subscribe(value => this.titleService.setTitle(value));

    //Load buttons text
    this.translateService.get('general.sendSms').pipe(take(1)).subscribe(value => { this.sendSmsStr = value });

    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.twoStepForm = this.formBuilder.group({
      code: ['', [Validators.required]],
      selectedFactor: [-1, [Validators.required]] //set -1 as nothing selected
    }, {
      validator: MatSelectHasValue('selectedFactor')
    });

    this.twoStepForm.controls.code.disable();


  }


  //===================================================================

  ngOnDestroy() {
    if (this.twoFactorIsDisabledIntervalRef)
      clearInterval(this.twoFactorIsDisabledIntervalRef);
  }


  //===================================================================
  onSubmit = async () => {


    if (this.slowInternetTimeout)
      clearTimeout(this.slowInternetTimeout);

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.signingIn = true;
    this.slowInternetTimeout = setTimeout(() => { if (this.signingIn) this.miscService.showSlowInternetConnectionSnackBar() }, 40000); //If processing recaptcha takes longer than usual

    //-------------------------
    //Check recaptcha challenge.
    if (!this.loginVerifier) {
      this.loginVerifier = new firebase.auth.RecaptchaVerifier('loginRecaptchaContainer', {
        'size': 'invisible'
      });
      this.loginVerifier.render();
    }
    //Verify recaptcha
    await this.loginVerifier.verify().then(async result => {
      const context = this;

      let loginObj = {
        email: context.loginForm.controls.email.value,
        password: context.loginForm.controls.password.value,
        preserve: context.rememberMe
      }

      //Log in the user
      await context.loginService.login(loginObj).then(result => {
        context.signingIn = false;
        //Clear the timeout because an action was completed
        if (context.slowInternetTimeout)
          clearTimeout(this.slowInternetTimeout);

        //----------------------------------------------------------------------
        //If result is asking for two step authentication, display the 2 step fields
        if (result.twoFactorRequired) {
          context.loginMessage = "";
          context.twoStepRequired = true;
          context.twoStepResolver = result.twoFactorResolver;

          //Update form value to auto select the first item from the list
          context.twoStepForm.patchValue({
            selectedFactor: 0
          });

          //Setup the reCAPTCHA, it is needed on the obSubmitTwoStep method
          context.twoStepAppVerifier = new firebase.auth.RecaptchaVerifier('twoStepRecaptchaContainer', {
            'size': 'invisible'
          });
          //----------------------------------------------------------------------

        } else {
          //If this ELSE is triggered is due to wrong password or error.

          //Possible an error message or success
          context.loginMessage = result.message;
          context.loginMessageType = result.success ? 'success' : 'warning';

        }



      }).catch(function (error) {
        //Clear the timeout because an action was completed
        if (context.slowInternetTimeout)
          clearTimeout(context.slowInternetTimeout);

        context.loginMessage = error.message;
        context.loginMessageType = "error";
        context.signingIn = false;
      });
    }).catch(error => {
      //Clear the timeout because an action was completed
      if (this.slowInternetTimeout)
        clearTimeout(this.slowInternetTimeout);

        this.translateService.get('login.recaptchaError').pipe(take(1)).subscribe(recaptchaErrorStr => {
        this.loginMessage = recaptchaErrorStr;
        this.loginMessageType = "error";
        this.signingIn = false;
      });
    });
    //-------------------------


  }


  //===================================================================
  sendTwoFactorSms = () => {
    const context = this;

    //If the message was sent before, and the user try again, warn the user to hold on and wait
    if (!this.twoFactorIsDisabled) {
      this.processSmsSending();
    }

  }

  //===================================================================
  processSmsSending = () => {

    //stop here if form is invalid
    if (this.twoStepForm.controls.selectedFactor.value == -1) {
      this.twoStepForm.controls.selectedFactor.markAllAsTouched();
      return;
    }


    this.twoStepForm.controls.code.enable();
    this.twoFactorIsDisabled = true; //Set to true so that the button is disabled. Set a timer so that it is auto enabled after elapsed time
    this.twoFactorSendingSms = true;
    const context = this;

    //set timeout
    this.slowInternetTimeout = setTimeout(() => { if (this.twoFactorSendingSms) this.miscService.showSlowInternetConnectionSnackBar() }, 40000); //If processing recaptcha takes longer than usual


    //set the SMS SENT label value
    this.translateService.get('general.smsSent').pipe(take(1)).subscribe(value => { this.sendSmsStr = value });

    this.loginService.sendTwoFactorSms(this.twoStepResolver, this.twoStepForm.controls.selectedFactor.value, this.twoStepAppVerifier).then(result => {
      this.twoFactorSendingSms = false;

      //Set sent message
      setTimeout(() => {
        //Return the send sms label text
        context.translateService.get('general.sendSms').pipe(take(1)).subscribe(value => { this.sendSmsStr = value });

      }, 3000);

      //Clear the timeout because an action was completed
      if (this.slowInternetTimeout)
        clearTimeout(this.slowInternetTimeout);


      //Now set a interval to allow the user to click again to resend.
      this.twoFactorIsDisabledIntervalRef = setInterval(() => {
        if (this.twoFactorIsDisabledIntervalCount < 1) {
          clearInterval(this.twoFactorIsDisabledIntervalRef);
          this.twoFactorIsDisabled = false; //allow to send sms again
          this.twoFactorIsDisabledIntervalCount = 60; //reset count
        }
        this.twoFactorIsDisabledIntervalCount--;

      }, 1000);

      if (result.status) {
        //If success, just save the verification ID
        context.twoStepSmsVerificationId = result.verificationId;

        //focus the code input
        context.codeInput.nativeElement.focus();

      } else {
        //If error, display message
        context.loginMessage = result.message;
        context.loginMessageType = 'warning';
      }
    }).catch(error => {
      context.translateService.get('general.genericError').pipe(take(1)).subscribe(value => {
        context.loginMessage = value + error.message;
        context.loginMessageType = 'error';
      });
    });

  }

  //===================================================================
  validateTwoFactor = () => {
    // stop here if form is invalid
    if (this.twoStepForm.invalid || this.twoStepSmsVerificationId == '' || this.twoStepForm.controls.code.value == '') {
      return;
    }
    this.twoFactorSubmitting = true;

    this.loginService.validateTwoFactorCode(this.twoStepResolver, this.twoStepSmsVerificationId, this.twoStepForm.controls.code.value).then(result => {

      if (result.status) {
        //If success, don't do anything, because the redirect will be done by the app module > appInit service > initAuthenticatedListener

      } else {
        //If error, display message
        this.loginMessage = result.message;
        this.loginMessageType = 'warning';

        this.twoFactorSubmitting = false;
      }

    }).catch(error => {
      this.translateService.get('general.genericError').pipe(take(1)).subscribe(value => {
        this.loginMessage = value + error.message;
        this.loginMessageType = 'error';
      });
    });

  }
}

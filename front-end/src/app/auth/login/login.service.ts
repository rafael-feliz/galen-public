/*  ******************************************************************************************************
    Rafael Féliz

    ******************************************************************************************************
*/

import { NgZone } from '@angular/core';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import { take } from 'rxjs/operators';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../store/app.reducer';
import * as authActions from '../../auth/store/auth.actions';
import * as globalActions from '../../store/global.actions';
import { FirebaseAuthService } from '../services/firebase-auth.service';
import { PATH_LOGIN, PATH_TWO_FACTOR_SETUP, PATH_CUSTOMER_SETUP } from 'src/app/constants/routing-paths.constants';
import { GALENC_EMAIL_TEMPLATE_GENERIC, GalenFDateUtc, GalenFDateUtcUnix, GalenISendEmailQueue } from 'galen-shared';
import { MiscService } from 'src/app/services/misc/misc.service';
import BrowserDtector from 'browser-dtector';
import { TranslateService } from '@ngx-translate/core'
import { AngularFirestore } from '@angular/fire/firestore';

interface loginObj { email: string, password: string, preserve: boolean };

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  userInfoSubscription;
  initAuthenticatedListenerSubscription;

  constructor(private router: Router,
    private afAuth: AngularFireAuth,
    private store: Store<fromRoot.State>,
    private firebaseAuthService: FirebaseAuthService,
    private ngZone: NgZone,
    private miscService: MiscService,
    private afFirestore: AngularFirestore,
    private translateService: TranslateService) { }


  //==================================================================================
  //Create a listener that will redirect the user whether it is authenticated or not.
  //Used in appModule
  initAuthenticatedListener = () => {
    const context = this;

    console.info("Galen App Init - Init auth listener");

    if (this.initAuthenticatedListenerSubscription)
      this.initAuthenticatedListenerSubscription.unsubscribe(); //just in case, unsubscribe if called more than once

    this.initAuthenticatedListenerSubscription = context.afAuth.authState.subscribe(async user => {

      var takeToLogin = false;

      if (user) {


        if (user.emailVerified) {
          //USER EMAIL IS VERIFIED

          //Store user info and auth state
          //(Initially. This information will be updated on the subscribeToUserInfo function in realtime)
          await context.store.dispatch(new authActions.LogIn(
            {
              email: user.email,
              isAuthenticated: true,
              firstName: user.displayName ? user.displayName.split(" ")[0] : "",
              fullName: user.displayName
            }));


          //----------------------
          //CHECK 2 STEP AUTHENTICATION IF IT IS ENABLED ON THIS USER
          if (user.multiFactor.enrolledFactors.length > 0) {
            //It has 2 factor auth enabled

            //Store that it has two factor enrolled
            await context.store.dispatch(new authActions.SetTwoFactorEnrolled(
              { twoFactorAuthenticationEnrolled: true }));


            //----------------------
            //Get customer info from logged in user, and then redirect to the requested path or root path, or the path to setup the customer
            //This is done on the app/services/app-init.service.ts, there is a subscription there that listen to the state that will detect this step

          } else {
            //Multifactor not enabled, the user should set it up.

            //Store that it has two factor enrolled
            await context.store.dispatch(new authActions.SetTwoFactorEnrolled({ twoFactorAuthenticationEnrolled: false }));

            this.ngZone.run(() => context.router.navigate(["/" + PATH_TWO_FACTOR_SETUP]));
          }
        } else {
          await context.logout();
          takeToLogin = true;
        }
      } else {
        takeToLogin = true;
      }

      if (takeToLogin) {
        // check whether the current ROUTE has CAN ACTIVATE with an AUTH GUARD.
        //If has CAN ACTIVATE, then it should redirect to login
        //If it doesn't have, then don't redirect to login as because this route does not need auth (i.e. not found page),
        var currentRouteConfig = context.router.config.find(f => f.path == context.router.url.substr(1));
        if (currentRouteConfig != null && currentRouteConfig.canActivate != null) {
          context.store.dispatch(new authActions.LogOut);
          window.location.href = "/" + PATH_LOGIN; //prefer to do a real redirect to reload the app after logging out, because if not, it will throw an error that says: database is closing, because the databses are deleted after logging out or refreshing the page if login REMEMBER ME was not set.
        }
        takeToLogin = false;

        //Remove loading indicator for the first time
        this.store.dispatch(new globalActions.ShowFullScreenLoader(false));
      }

    });
  };

  //==================================================================================
  //Logs the user in
  login = (param: loginObj): Promise<{ success: boolean, message: string, twoFactorRequired?: boolean, twoFactorResolver?: firebase.auth.MultiFactorResolver }> => {

    //*********NOTE:::::::::  The login redirect is performed on the initAuthenticatedListener on the app-init.service.ts

    //Clear the customer info storage item. This item is set on the app-init.service.ts
    localStorage.setItem("cinf", "");

    var context = this;

    return new Promise((resolve) => {
      context.afAuth.setPersistence(param.preserve ? firebase.auth.Auth.Persistence.LOCAL : firebase.auth.Auth.Persistence.SESSION).then(async () => {
        //store the persistence on the state
        await this.store.dispatch(new authActions.SetAuthPersistence({ authenticationIsPersistent: param.preserve }));
        if (param.preserve)
          localStorage.setItem("lpers", "1"); //lpers = 1 login is persistent
        else
          localStorage.setItem("lpers", "0"); //lpers = 0 login is not persistent

        // Now sign-in using your chosen method.
        context.afAuth.signInWithEmailAndPassword(param.email.trim(), param.password.trim())
          .then(async result => {
            //SUCCESS
            //*********NOTE:::::::::  initAuthenticatedListener(), that is the one who process the login redirect

            //Check whether the user email is validated or not
            if (result.user.emailVerified)
              resolve({ success: true, message: "" });

            else {

              //Email is not verified, send it again...
              context.firebaseAuthService.sendActionLink(param.email, result.user.displayName, 'emailVerification').then(sendingLinkResult => {
                if (sendingLinkResult.success) {
                  context.afAuth.signOut();
                  // send successful.
                  context.translateService.get('register.success').pipe(take(1)).subscribe(value => {
                    resolve({ success: false, message: _.replace(value, '{$1}', param.email) });
                  });
                } else {
                  resolve({ success: false, message: _.replace(sendingLinkResult.message, '{$1}', param.email) });
                }
              });
              await context.logout();
            }

          })
          .catch((error) => {
            //See: https://cloud.google.com/identity-platform/docs/web/mfa#signing_users_in_with_a_second_factor
            if (error.code == 'auth/multi-factor-auth-required') {
              //TWO FACTOR VERIFICATION IS REQUIRED
              resolve({ success: false, message: '', twoFactorRequired: true, twoFactorResolver: error.resolver });
            } else {
              //ERROR
              //Get the result from firebase, and look for a language with that string
              context.firebaseAuthService.translateFirebaseErrorMessages(error).then(value => {
                resolve({ success: false, message: value });
              });
            }
          });

      }).catch((error) => {
        resolve({ success: false, message: error.message });
      });
    });
  }

  //==================================================================================
  //Logs the user out

  logout = async (signOut: boolean = true, clearDb: boolean = false, clearStorage: boolean = false) => {


    if (clearStorage) {
      await localStorage.clear();

      //Clear service worker
      if ('caches' in window) {
        caches.keys()
          .then(function (keyList) {
            return Promise.all(keyList.map(function (key) {
              return caches.delete(key);
            }));
          })
      }
    }

    //Clear indexDB's
    if (clearDb) {
      //Clear storage
      await this.afFirestore.firestore.terminate();
      await this.afFirestore.firestore.clearPersistence();

      await window.indexedDB.deleteDatabase("firestore/[DEFAULT]/galenappl/main");
      await window.indexedDB.deleteDatabase("firebaseLocalStorageDb");

      console.log("Disposed Galen App");
      //perform a real redirect to reload the app after clearing the dbs, because if not, it will throw an error that says: database is closing, because the databses are deleted after logging out or refreshing the page if login REMEMBER ME was not set.
      setTimeout(() => {
        console.log("Kicked to login");
        window.location.href = "/" + PATH_LOGIN;
      }, 2000);
    }

    if (signOut) {
      await this.afAuth.signOut();
    }
  }

  //==================================================================================
  //Send login email

  sendLoginEmail = async () => {

    let subject = "";
    let preHeader = "";
    let body = "";
    const browserInfo = this.miscService.getBrowserInfo();

    //Get language strings
    this.translateService.get('login.emailSubject').pipe(take(1)).subscribe(value => { subject = value });
    this.translateService.get('login.emailPreHeader').pipe(take(1)).subscribe(value => { preHeader = value });
    this.translateService.get('login.emailBody').pipe(take(1)).subscribe(value => {
      body = value;
      body = _.replace(body, "${1}", browserInfo.name);
      body = _.replace(body, "${2}", browserInfo.platform);
    });


    //Await a second before proceeding, to give time to the auth state to change
    setTimeout(async () => {
      const browser = new BrowserDtector(window.navigator.userAgent);
      const browserInfo = browser.getBrowserInfo();

      await this.afAuth.authState.subscribe(async user => {
        if (user) {
          let loggedInUser = user;

          await this.store.select(fromRoot.getBrowserLanguage).pipe(take(1)).subscribe(async lang => {
            //Send an email indicating the new login
            //Prepare the email queue params
            let emailData: GalenISendEmailQueue = {
              customerId: "",
              userEmail: loggedInUser.email,
              date: GalenFDateUtc(),
              dateUnix: GalenFDateUtcUnix(),
              language: lang,
              subject: subject,
              preHeader: preHeader,
              body: body,
              toName: loggedInUser.displayName,
              toEmail: loggedInUser.email,
              emailTemplate: GALENC_EMAIL_TEMPLATE_GENERIC,
              customerClinicLogoUrl: "",
              customerClinicName: ""
            };
            this.miscService.sendEmail(emailData);
          });
        }
      });
    }, 1000);

  }



  //==================================================================================
  //Sends the 2 factor auth SMS
  //https://cloud.google.com/identity-platform/docs/web/mfa#signing_users_in_with_a_second_factor

  sendTwoFactorSms = (resolver: firebase.auth.MultiFactorResolver, selectedFactorIndex: number, appVerifier: firebase.auth.RecaptchaVerifier): Promise<{ status: boolean, message: string, verificationId: string }> => {
    const context = this;

    return new Promise(resolve => {
      if (resolver.hints[selectedFactorIndex]) {
        if (resolver.hints[selectedFactorIndex].factorId ===
          firebase.auth.PhoneMultiFactorGenerator.FACTOR_ID) {

          var phoneInfoOptions = {
            multiFactorHint: resolver.hints[selectedFactorIndex],
            session: resolver.session
          };

          var phoneAuthProvider = new firebase.auth.PhoneAuthProvider();

          // Send SMS verification code
          phoneAuthProvider.verifyPhoneNumber(phoneInfoOptions, appVerifier)
            .then(function (verificationId) {
              resolve({ status: true, message: '', verificationId: verificationId });
            }).catch(function (error) {
              context.firebaseAuthService.translateFirebaseErrorMessages(error).then(result => {
                resolve({ status: false, message: result, verificationId: '' });
              })
            });

        } else {
          resolve({ status: false, message: 'Unsupported 2-Factor', verificationId: '' }); //This code should be never hit, but just in case...
        }
      } else {
        resolve({ status: false, message: 'Select the 2-factor method', verificationId: '' }); //This code should be never hit, but just in case...
      }

    });
  }



  //==================================================================================
  //Validate the 2 step code that was received and set
  //https://cloud.google.com/identity-platform/docs/web/mfa#signing_users_in_with_a_second_factor

  validateTwoFactorCode = (resolver: firebase.auth.MultiFactorResolver, verificationId: string, verificationCode: string): Promise<{ status: boolean, message: string }> => {
    const context = this;

    return new Promise(resolve => {
      var cred = firebase.auth.PhoneAuthProvider.credential(
        verificationId, verificationCode);

      var multiFactorAssertion = firebase.auth.PhoneMultiFactorGenerator.assertion(cred);

      // Complete the sign in. This will trigger the initAuthenticatedListener on this service, handled with the app module. This will handle the login redirect to main page.
      resolver.resolveSignIn(multiFactorAssertion).then(async result => {

        await this.sendLoginEmail();

        resolve({ status: true, message: '' });
      }).catch(function (error) {
        context.firebaseAuthService.translateFirebaseErrorMessages(error).then(result => {
          resolve({ status: false, message: result });
        })
      });

    });
  }
}

import { TestBed } from '@angular/core/testing';

import { CustomerSetupService } from './customer-setup.service';

describe('CustomerSetupService', () => {
  let service: CustomerSetupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomerSetupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { FC_CUSTOMERSDATA, FC_CUSTOMERSDATA_CLINICS, GALENC_MAIN_CLINIC_COLLECTION_DOCUMENT_ID, GalenICustomer, GalenICustomerCreate, GalenIFile } from 'galen-shared';
import { CLOUD_FUNCTION_FINISH_CREATE_CUSTOMER } from 'src/app/constants/cloud-functions.constants';
import { ErrorService } from '../../services/errors/error.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerSetupService {

  constructor(private afFunctions: AngularFireFunctions,
    private afAuth: AngularFireAuth,
    private afFirestore: AngularFirestore,
    private errorService: ErrorService) { }

  /**
   * Calls the cloud function to create a customer
   * @param customer
   */
  createCustomer = async (customer: GalenICustomerCreate): Promise<{ success: boolean, message: string, customerId?}> => {
    const context = this;
    return new Promise(async resolve => {
      await context.afFunctions.httpsCallable(CLOUD_FUNCTION_FINISH_CREATE_CUSTOMER)(customer).toPromise().then(async res => {
         //Finish the promise
         resolve({ success: res.success, message: res.message, customerId: res.customerId });
      }).catch(error => {
         //Finish the promise
         resolve({ success: false, message: error.message });
      });
    });
  };

  /** Wakes up the cloud function to prevent cold start when creating the customer */
  wakeCreateCustomerCloudFunction = (): void => {
    this.afFunctions.httpsCallable(CLOUD_FUNCTION_FINISH_CREATE_CUSTOMER)({}).toPromise().then().catch();
  };
}

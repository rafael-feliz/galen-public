/*  ******************************************************************************************************
    Rafael Féliz

    After registering a new user, it should create the customer info and initial data. This is asked and
    configured here.

    ******************************************************************************************************
*/


import { Component, NgZone, OnChanges, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { GALENC_COUNTRIES_LIST_EN, GALENC_CURRENCIES, GALENC_DATE_FORMATS, GALENC_TIME_FORMATS, GalenFGetCurrencyFromCountry, GalenICurrency, GALENC_LANGUAGES, GalenICustomerCreate, GALENC_FILE_LOCATION_CLINIC_LOGO, GalenIFileUploaded, GALENC_MAIN_CLINIC_COLLECTION_DOCUMENT_ID, FC_CUSTOMERSDATA_CLINICS } from 'galen-shared';
import { take } from 'rxjs/operators';
import { GICON_DENTAL, GICON_MEDICAL } from 'src/app/constants/font-awesome.constants';
import { PhoneNumberIsValid } from 'src/app/helpers/forms.validators';
import { MiscService } from '../../services/misc/misc.service';
import { CustomerSetupService } from './customer-setup.service';
import * as fromRoot from '../../store/app.reducer';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core'
import { ErrorService } from '../../services/errors/error.service';
import { PATH_CUSTOMER_SETUP } from 'src/app/constants/routing-paths.constants';



@Component({
  selector: 'app-customer-setup',
  templateUrl: './customer-setup.component.html',
  styleUrls: ['./customer-setup.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CustomerSetupComponent implements OnInit, OnChanges, OnDestroy {

  fileLocation = GALENC_FILE_LOCATION_CLINIC_LOGO;
  setupCustomerForm: FormGroup;
  countryCodeSubscription;
  message = "";
  messageType = "";
  submitting = false;
  countriesList = GALENC_COUNTRIES_LIST_EN;
  currencyList = GALENC_CURRENCIES;
  dateFormats = GALENC_DATE_FORMATS;
  timeFormats = GALENC_TIME_FORMATS;
  languagesList = GALENC_LANGUAGES;
  businessIsDental = true;
  GICON_DENTAL = GICON_DENTAL;
  GICON_MEDICAL = GICON_MEDICAL;
  userDate;
  imageCode = "";
  mainClinicId = GALENC_MAIN_CLINIC_COLLECTION_DOCUMENT_ID;
  mainClinicName = "";
  userFirstName = "";
  step = "";
  firebaseStorageLocationPath = `/${FC_CUSTOMERSDATA_CLINICS}/${GALENC_MAIN_CLINIC_COLLECTION_DOCUMENT_ID}`;


  constructor(private titleService: Title,
    private formBuilder: FormBuilder,
    private customerSetupService: CustomerSetupService,
    private miscService: MiscService,
    private store: Store<fromRoot.State>,
    private ngZone: NgZone,
    private router: Router,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private errorService: ErrorService) { }

  ngOnInit() {

    this.route.params.pipe(take(1)).subscribe(params => {
      if (typeof params['step'] != 'undefined') {
        this.step = params['step'];
      }

      //Set the page title
      this.translateService.get('customerSetup.pageTitle').pipe(take(1)).subscribe(value => this.titleService.setTitle(value));

      //Create the request form
      this.setupCustomerForm = this.formBuilder.group({
        businessIsDental: [true],
        clinicName: ['', [Validators.required]],
        userSpecialty: ['', [Validators.required]],
        address: [''],
        city: [''],
        state: [''],
        countryCode: ['', [Validators.required]],
        zip: [''],
        phone: [''],
        language: ['', [Validators.required]],
        currency: ['', [Validators.required]],
        dateFormatId: [1, [Validators.required]],
        timeFormatId: [1, [Validators.required]]
      }, {
        validator: PhoneNumberIsValid('phone')
      });

      this.countryCodeSubscription = this.setupCustomerForm.controls.countryCode.valueChanges.subscribe(countryCode => {
        var currencyFromCountry: GalenICurrency = GalenFGetCurrencyFromCountry(countryCode);
        this.setupCustomerForm.controls.currency.setValue(currencyFromCountry.code);
      })

      //Load the user current country
      this.miscService.getCountryFromIp().pipe(take(1)).subscribe(ipInformation => {
        if (ipInformation)
          this.setupCustomerForm.controls.countryCode.setValue(ipInformation.country_code);
      });

      //Load the current user browser language
      this.store.select(fromRoot.getBrowserLanguage).pipe(take(1)).subscribe(lang => {
        this.setupCustomerForm.controls.language.setValue(lang);
      });

      //Get the logged in user info
      this.store.select(fromRoot.getAuthState).pipe(take(5)).subscribe(async authState => {
        this.userFirstName = authState.firstName.split(" ")[0];

        if (authState.customerInfo)
          this.mainClinicName = authState.customerInfo.clinicName;

        //If customer already setup the step is not specified, exit this page...
        if (authState.customerInfo && this.step === '')
          window.location.href = "/";

      });

      //Just call this function to prevent cold start
      this.customerSetupService.wakeCreateCustomerCloudFunction();
    });
  }


  ngOnChanges() {

  };

  ngOnDestroy() {
    if (this.countryCodeSubscription)
      this.countryCodeSubscription.unsubscribe();
  };


  //=====================================
  //Submit the client information (clinic)

  onSubmit = async (): Promise<void> => {

    // stop here if form is invalid
    if (this.setupCustomerForm.invalid) {
      return;
    }
    this.submitting = true;

    //Get the logged in user info
    await this.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(async authState => {

      //build the param object
      let data: GalenICustomerCreate = {
        userSpecialty: this.setupCustomerForm.controls.userSpecialty.value,
        customer: {
          businessIsDental: this.setupCustomerForm.controls.businessIsDental.value,
          clinicName: this.setupCustomerForm.controls.clinicName.value,
          address: {
            address: this.setupCustomerForm.controls.address.value,
            city: this.setupCustomerForm.controls.city.value,
            countryCode: this.setupCustomerForm.controls.countryCode.value,
            state: this.setupCustomerForm.controls.state.value,
            zip: this.setupCustomerForm.controls.zip.value
          },
          phone: [{ phone: this.setupCustomerForm.controls.phone.value }],
          currency: this.setupCustomerForm.controls.currency.value,
          language: this.setupCustomerForm.controls.language.value,
          dateFormatId: this.setupCustomerForm.controls.dateFormatId.value,
          timeFormatId: this.setupCustomerForm.controls.timeFormatId.value

        },
        userEmail: authState.email,
        userFullName: authState.fullName
      };


      await this.customerSetupService.createCustomer(data).then(async result => {
        if (result.success) {

          //Relod the page to refresh added information, and redirect to add-your-logo on customer-setup
          window.location.href = `${PATH_CUSTOMER_SETUP}/add-your-logo`; //NOTE: This should be done with window.location.href to apply the changes..

        } else {
          this.submitting = false;
          this.message = result.message;
          this.messageType = result.success ? 'success' : 'warning';
        }
      }).catch(error => {
        this.translateService.get('general.genericError').pipe(take(1)).subscribe(genericError => {
          this.submitting = false;
          this.message = genericError;
          this.messageType = 'error';

          this.errorService.showCustomErrorMessages(error);
        });
      });
    });

  };

  //=====================================
  //On file uploaded

  onFileUploaded = async (files: Array<GalenIFileUploaded>) => {
    if (files) {
      if (files.length > 0 && files[0].imageCode) {
        //set the image code to display it on the canvas
        this.imageCode = files[0].imageCode;
        this.message = "";
      }
    }
  };


  //=====================================
  //Finish the process

  finish = () => {
    this.ngZone.run(() => this.router.navigate(["/"]));
  }

}


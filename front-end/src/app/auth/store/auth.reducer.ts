//STORE For authentication data

import { AuthActions, LOG_IN, LOG_OUT, SET_AUTH_PERSISTENCE, SET_BILLING_PLAN_DETAILS, SET_CUSTOMER_INFO,  SET_CUSTOMER_INFO_WAS_LOADED,  SET_TWO_FACTOR_ENROLLED, SET_USER_INFO } from './auth.actions';
import * as _ from 'lodash';
import { GalenAdminIBillingPlans_ForPublicUsage, GalenICustomer, GalenIFile, GalenIPhone, GalenTlanguageTypes } from 'galen-shared';

//-----------------
//The state structure for Auth
export interface State {
  isAuthenticated?: boolean,
  authenticationIsPersistent?: boolean,
  twoFactorAuthenticationEnrolled?: boolean,
  email?: string,
  firstName?:string,
  fullName?:string,
  photo?:GalenIFile,
  phone?:GalenIPhone,
  nonDoctor?:boolean,
  language?: GalenTlanguageTypes,
  dateFormatId?: number,
  timeFormatId?: number,
  customerInfo?:GalenICustomer,
  customerInfoWasLoaded?:boolean, //To tell the auth guard that the customer information was loaded
  roleId?:string,
  accessToClinicsIds?: Array<string>,
  roleActionsPermissions?: Array<string>
  billingPlanDetails?:GalenAdminIBillingPlans_ForPublicUsage,
  _settings_notificationsCount?:number,
  _settings_autoDisplayInstallAppButton?:boolean,
  _settings_selectedClinicId?:string,
  shouldResetPassword?:boolean
}

//-----------------
//The initial state for Auth
const initialState: State = {
  authenticationIsPersistent: false,
  twoFactorAuthenticationEnrolled: false,
  email: null,
  _settings_notificationsCount: 0,
  customerInfoWasLoaded: false
};

//-----------------
//ACTIONS
export function authReducer(state = initialState, action: AuthActions){
  switch(action.type){
    case LOG_IN:
    return {
      ...state,
      isAuthenticated:true,
      email: action.payload.email,
      firstName: action.payload.firstName,
      fullName: action.payload.fullName
    };
    case LOG_OUT:
    return {
      ...state,
      isAuthenticated:null,
      email: null
    };
    case SET_AUTH_PERSISTENCE:
    return {
      ...state,
      authenticationIsPersistent: true
    };
    case SET_CUSTOMER_INFO:
    return {
      ...state,
      customerInfo: !_.isNull(action.payload.customerInfo) ? action.payload.customerInfo : state.customerInfo,
    };
    case SET_BILLING_PLAN_DETAILS:
    return {
      ...state,
      billingPlanDetails: !_.isNull(action.payload.billingPlanDetails) ? action.payload.billingPlanDetails : state.billingPlanDetails,
    };
    case SET_TWO_FACTOR_ENROLLED:
    return {
      ...state,
      twoFactorAuthenticationEnrolled: action.payload.twoFactorAuthenticationEnrolled,
    };
    case SET_USER_INFO:
    return {
      ...state,
      firstName: !_.isNull(action.payload.firstName) ? action.payload.firstName : state.firstName,
      fullName: !_.isNull(action.payload.fullName) ? action.payload.fullName : state.fullName,
      phone: !_.isNull(action.payload.phone) ? action.payload.phone : state.phone,
      photo: !_.isNull(action.payload.photo) ? action.payload.photo : state.photo,
      language: !_.isNull(action.payload.language) ? action.payload.language : state.language,
      dateFormatId: isFinite(action.payload.dateFormatId) ? action.payload.dateFormatId : state.dateFormatId,
      timeFormatId: isFinite(action.payload.timeFormatId) ? action.payload.timeFormatId : state.timeFormatId,
      roleId: !_.isNull(action.payload.roleId) ? action.payload.roleId : state.roleId,
      accessToClinicsIds: !_.isNull(action.payload.accessToClinicsIds) ? action.payload.accessToClinicsIds : state.accessToClinicsIds,
      roleActionsPermissions: !_.isNull(action.payload.roleActionsPermissions) ? action.payload.roleActionsPermissions : state.roleActionsPermissions,
      _settings_notificationsCount: isFinite(action.payload._settings_notificationsCount) ? action.payload._settings_notificationsCount : state._settings_notificationsCount,

      _settings_selectedClinicId: !_.isNull(action.payload._settings_selectedClinicId) ? action.payload._settings_selectedClinicId : state._settings_selectedClinicId,
      _settings_autoDisplayInstallAppButton: action.payload._settings_autoDisplayInstallAppButton,
      shouldResetPassword: action.payload.shouldResetPassword
    };
    case SET_CUSTOMER_INFO_WAS_LOADED: //To tell the auth guard that the customer information was loaded
    return {
      ...state,
      customerInfoWasLoaded: action.payload,
    };
    default:
      return state;
  }
}

//Quick access exports to use in app.reducer
export const getIsAuthenticated = (state:State) => state.isAuthenticated;
export const getCustomerId = (state:State) => state.customerInfo?.customerId;
export const getEmail = (state:State) => state.email;

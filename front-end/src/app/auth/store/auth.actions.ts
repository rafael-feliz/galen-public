import { Action } from '@ngrx/store';
import { State } from './auth.reducer';

//==========================================
//Export types
export type AuthActions = LogIn | LogOut | SetCustomerInfo | SetUserInfo | SetBillingPlanDetails | SetTwoFactorEnrolled | SetAuthPersistence | SetCustomerInfoWasLoaded;

//==========================================
//ACTIONS
export const LOG_IN = "[Auth] Log in";
export class LogIn implements Action { readonly type = LOG_IN; constructor(public payload: State){} }

export const LOG_OUT = "[Auth] Log out";
export class LogOut implements Action { readonly type = LOG_OUT; }

export const SET_CUSTOMER_INFO = "[Auth] SetCustomerInfo";
export class SetCustomerInfo implements Action { readonly type = SET_CUSTOMER_INFO; constructor(public payload: State){} }

export const SET_USER_INFO = "[Auth] SetUserInfo";
export class SetUserInfo implements Action { readonly type = SET_USER_INFO; constructor(public payload: State){} }

export const SET_BILLING_PLAN_DETAILS = "[Auth] SetBillingPlanDetails";
export class SetBillingPlanDetails implements Action { readonly type = SET_BILLING_PLAN_DETAILS; constructor(public payload: State){} }

export const SET_TWO_FACTOR_ENROLLED = "[Auth] SetTwoFactorEnrolled";
export class SetTwoFactorEnrolled implements Action { readonly type = SET_TWO_FACTOR_ENROLLED; constructor(public payload: State){} }

export const SET_AUTH_PERSISTENCE = "[Auth] SetAuthPersistence";
export class SetAuthPersistence implements Action { readonly type = SET_AUTH_PERSISTENCE; constructor(public payload: State){} }

export const SET_CUSTOMER_INFO_WAS_LOADED = "[Auth] SetCustomerInfoWasLoaded";
export class SetCustomerInfoWasLoaded implements Action { readonly type = SET_CUSTOMER_INFO_WAS_LOADED; constructor(public payload: boolean){} }


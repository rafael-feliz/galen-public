import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, APP_INITIALIZER } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { I18nModule } from './i18n/i18n.module';
import { SentryErrorHandler } from './services/errors/sentryErrorHandler';

import { AuthModule } from './auth/auth.module';
import { SharedModule } from './shared.module';
import { PagesModule } from './pages/pages.module';
import { MaterialModule } from './material.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from './store/app.reducer';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireRemoteConfigModule } from '@angular/fire/remote-config';
import { AngularFirestoreModule, SETTINGS } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AngularFireFunctions } from '@angular/fire/functions';
import { BUCKET } from '@angular/fire/storage';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpIntercept } from './interceptor/http.interceptor';
import { AppInitService } from './services/app-init.service';
import { LoginService } from './auth/login/login.service';
import { CookieService } from 'ngx-cookie-service';

export function initializeApp(appInitService: AppInitService, loginService: LoginService) {
  return (): Promise<void> => {

    if (environment.production) {
      console.log("%cGALEN - WARNING!", "color: red; font-weight: bold; font-size: 40px");
      console.log("%cThis is a browser feature intended for developers. Using this console may allow attackers to impersonate you and steal your information using an attack called Self-XSS. Do not enter or paste code that you do not understand.", "font-size: 20px");
    } else {
      console.log("%cGALEN DEVELOPMENT MODE!", "font-weight: bold; font-size: 40px");
    }
    console.info("Galen App Init");
    return new Promise<void>(async resolve => {

      //This will redirect to login or main page whether the user is authenticated or not.
      await loginService.initAuthenticatedListener();

      //----------------------
      //Get customer info from logged in user, and then redirect to the requested path or root path, or the path to setup the customer
      await appInitService.getCustomerInfoForAppInit().then(() => {
        console.info("Galen App Init Ended");
        resolve();
      }).catch(error => {
        console.info("Galen App Init Error", error);
        resolve();
      });
    });
  }
}
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    AuthModule,
    SharedModule,
    PagesModule,
    AppRoutingModule,
    I18nModule,
    MaterialModule,
    FontAwesomeModule,
    StoreModule.forRoot(reducers),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFirestoreModule.enablePersistence({ synchronizeTabs: true}),
    AngularFireRemoteConfigModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: !environment.local })
  ],
  providers: [
    CookieService,
    { provide: APP_INITIALIZER, useFactory: initializeApp, deps: [AppInitService, LoginService], multi: true },
    { provide: ErrorHandler, useClass: SentryErrorHandler },
    { provide: HTTP_INTERCEPTORS, useClass: HttpIntercept, multi: true },
    //Makes angularFire firestore works with Firebase emulator:
    //minimumFetchIntervalMillis is for Firebase remote-config
    { provide: SETTINGS, useValue: !environment.local ? undefined : { host: "localhost:8080", ssl: false } },
    // { provide: BUCKET, useValue: GALENC_FIREBASE_STORAGE_BUCKET }, //FIREBASE STORAGE: (AngularFire) the storage bucket for Galen customers

  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private fireFunctions: AngularFireFunctions,
    private fireAuth: AngularFireAuth) {
    //Set development environment to run from emulator (For cloud functions)
    if (environment.local) {
      this.fireFunctions.useFunctionsEmulator('http://localhost:5001'); //cloud functions host
      //this.fireAuth.useEmulator('http://localhost:9099'); //firebase authentication host
    }
  }

}

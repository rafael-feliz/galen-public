import { Component, HostListener, OnInit, ViewEncapsulation } from '@angular/core';
import { GICON_ADD, GICON_AGENDA, GICON_BILLING, GICON_DASHBOARD, GICON_MENUBARS, GICON_PATIENTS, GICON_PROFILE, GICON_REPORTS, GICON_SEARCH, GICON_SETTINGS, GICON_SIGNOUT, GICON_USER, GICON_WAITINGLIST } from 'src/app/constants/font-awesome.constants';
import { MiscService } from 'src/app/services/misc/misc.service';
import { FC_CUSTOMERSDATA_USERS, GALENC_FILE_LOCATION_USERS_PROFILE_PHOTOS, GALENC_SCREEN_SIZES, GalenTfileLocations } from 'galen-shared';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MobileModeMenuComponent } from './mobile-mode-menu.component';
import { MainNavigationService } from './main-navigation.service';
import * as fromRoot from '../../../store/app.reducer';
import { Store } from '@ngrx/store';
import { PATH_ACCOUNT, PATH_ACCOUNT_PROFILE, PATH_DASHBOARD } from 'src/app/constants/routing-paths.constants';

@Component({
  selector: 'main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainNavigationComponent implements OnInit {

  GICON_ADD = GICON_ADD;
  GICON_SEARCH = GICON_SEARCH;
  GICON_DASHBOARD = GICON_DASHBOARD;
  GICON_AGENDA = GICON_AGENDA;
  GICON_PATIENTS = GICON_PATIENTS;
  GICON_BILLING = GICON_BILLING;
  GICON_REPORTS = GICON_REPORTS;
  GICON_WAITINGLIST = GICON_WAITINGLIST;
  GICON_SETTINGS = GICON_SETTINGS;
  GICON_USER = GICON_USER;
  GICON_SIGNOUT = GICON_SIGNOUT;
  GICON_MENUBARS = GICON_MENUBARS;
  GICON_PROFILE = GICON_PROFILE;


  screenSize = "";
  matPosition = "right";
  userName = "";
  userPhoto;
  userEmail;
  userProfilePhotoType: GalenTfileLocations = "users-profile-photos";
  profilePictureFirestorePath = "";

  PATH_DASHBOARD = PATH_DASHBOARD
  PATH_ACCOUNT = PATH_ACCOUNT
  PATH_ACCOUNT_PROFILE = PATH_ACCOUNT_PROFILE

  constructor(private miscService: MiscService,
    private bottomSheet: MatBottomSheet,
    private mainNavigationService: MainNavigationService,
    private store: Store<fromRoot.State>) { }

  /**Listen to windows updates */
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.onWindowResize();
  }

  //---------------------------

  ngOnInit(): void {
    this.onWindowResize();

    //Subscribe to get user full name updates
    this.store.select(fromRoot.getAuthState).subscribe(authState => {
      this.userEmail = authState.email;
      this.userName = authState.fullName;
      this.userPhoto = authState.photo ? authState.photo : null;
      this.profilePictureFirestorePath = `/${FC_CUSTOMERSDATA_USERS}/${this.userEmail}`;
    });

  }

  //---------------------------

  onWindowResize = () => {
    let width = window.innerWidth;
    const screenSizes = GALENC_SCREEN_SIZES;
    this.screenSize = this.miscService.getCurrentScreenSize(width);

    if(this.screenSize === screenSizes.sm || this.screenSize === screenSizes.xs)
      this.matPosition = "above";
    else
      this.matPosition = "right";

  }

  //---------------------------

  openMobileModeMenuComponent(): void {
    this.bottomSheet.open(MobileModeMenuComponent);
  }


  //---------------------------

  onLogout() {
    this.mainNavigationService.onLogout();
  }


}

/* =============================================================================
  Rafael Féliz

  To share between main nav and other sub components

 ============================================================================= */


import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { take } from 'rxjs/operators';
import { LoginService } from 'src/app/auth/login/login.service';
import { ConfirmDialogComponent, IConfirmDialog } from '../confirm-dialog/confirm-dialog.component';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class MainNavigationService {

  constructor(private loginService: LoginService,
    private translateService: TranslateService,
    private dialog: MatDialog) { }

  //------------------------
  /** LOGS OUT THE USER */
  onLogout = async () => {

    this.translateService.get('login.confirmLogOut').pipe(take(1)).subscribe(message => {
      const dialogData: IConfirmDialog = {
        messageLangRef: message
      };

      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: dialogData
      });

      dialogRef.afterClosed().subscribe(async dialogResult => {
        if (dialogResult)
          await this.loginService.logout(true, true);
      });
    });

  };

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileModeMenuComponent } from './mobile-mode-menu.component';

describe('MobileModeMenuComponent', () => {
  let component: MobileModeMenuComponent;
  let fixture: ComponentFixture<MobileModeMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobileModeMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileModeMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

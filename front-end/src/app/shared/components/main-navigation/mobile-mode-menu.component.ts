/* =============================================================================
  Rafael Féliz

  The "more" menu that appears on mobile

 ============================================================================= */

import { Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { GICON_REPORTS, GICON_SETTINGS, GICON_SIGNOUT, GICON_PROFILE, GICON_WAITINGLIST } from 'src/app/constants/font-awesome.constants';
import { MainNavigationService } from './main-navigation.service';
import * as fromRoot from '../../../store/app.reducer';
import { Store } from '@ngrx/store';
import { PATH_ACCOUNT, PATH_ACCOUNT_HOME, PATH_ACCOUNT_PROFILE } from 'src/app/constants/routing-paths.constants';
import { FC_CUSTOMERSDATA_USERS, GalenTfileLocations } from 'galen-shared';

@Component({
  selector: 'app-mobile-mode-menu',
  templateUrl: './mobile-mode-menu.component.html',
  styleUrls: ['./mobile-mode-menu.component.scss']
})
export class MobileModeMenuComponent implements OnInit {

  GICON_REPORTS = GICON_REPORTS;
  GICON_PROFILE = GICON_PROFILE;
  GICON_SIGNOUT = GICON_SIGNOUT;
  GICON_SETTINGS = GICON_SETTINGS;
  GICON_WAITINGLIST = GICON_WAITINGLIST;
  userName = "";
  userPhoto;
  userEmail;
  userProfilePhotoType: GalenTfileLocations = "users-profile-photos";
  profilePictureFirestorePath = "";

  PATH_ACCOUNT_HOME = PATH_ACCOUNT_HOME;
  PATH_ACCOUNT = PATH_ACCOUNT;
  PATH_ACCOUNT_PROFILE = PATH_ACCOUNT_PROFILE;


// export const GICON_USERSETTINGS = faUserCog;

  constructor(private _bottomSheetRef: MatBottomSheetRef<MobileModeMenuComponent>,
    private mainNavigationService: MainNavigationService,
    private store: Store<fromRoot.State>) { }

  ngOnInit(): void {
    this.store.select(fromRoot.getAuthState).subscribe(authState => {
      this.userName = authState.fullName;
      this.userPhoto = authState.photo ? authState.photo : null;
      this.userEmail = authState.email;
      this.profilePictureFirestorePath = `/${FC_CUSTOMERSDATA_USERS}/${this.userEmail}`;
    });
  }

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }

  closeMenu(): void {
    this._bottomSheetRef.dismiss();
  }

  onLogout() {
    this.mainNavigationService.onLogout();
  }

}

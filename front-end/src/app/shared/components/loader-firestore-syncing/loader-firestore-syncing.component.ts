/*  ******************************************************************************************************
    Rafael Féliz

    Displays a loading indicator the main logo.

    This is used to show when a transaction is uploaded to the server, or an offline transaction is pending to be uploaded to the server.
    It will be shown after X elapsed second

    ******************************************************************************************************
*/
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Store } from '@ngrx/store';
import { GICON_UP } from 'src/app/constants/font-awesome.constants';
import * as fromRoot from '../../../store/app.reducer';

@Component({
  selector: 'loader-firestore-syncing',
  templateUrl: './loader-firestore-syncing.component.html',
  styleUrls: ['./loader-firestore-syncing.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoaderFirestoreSyncingComponent implements OnInit {

  constructor(private store: Store<fromRoot.State>,
    private afFirestore: AngularFirestore) { }

  showLoader = true; //Displays the loading indicator
  awaitingUpdateFinish = false; //Prevent awaiting more than once (this.afFirestore.firestore.waitForPendingWrites)
  timeoutRef;
  GICON_UP = GICON_UP;

  ngOnInit(): void {

    //--------------------
    //FIRST TIME
    //Check if firestore has pending writes from the local storage, and if not, hide the loader
    this.awaitingUpdateFinish = true;
    this.afFirestore.firestore.waitForPendingWrites().then(() => {
      this.showLoader = false;
      this.awaitingUpdateFinish = false;
    });

    //Subscribe to state values changes.
    this.store.select(fromRoot.getGlobalState).subscribe(globalState => {
      if (globalState.showSyncingFirestoreLoader == true) {

        //--------------------
        //Check again if firestore has pending writes from the local storage.
        if (!this.awaitingUpdateFinish) {

          //Await a bit to give time to finish, to prevent displaying and hiding the loader so fast when the sync is finished soon
          this.timeoutRef = setTimeout(() => {
            this.showLoader = true;
          }, 1000);

          this.awaitingUpdateFinish = true;
          this.afFirestore.firestore.waitForPendingWrites().then(() => {
            if (this.timeoutRef)
              clearTimeout(this.timeoutRef);

            this.showLoader = false;
            this.awaitingUpdateFinish = false;
          });
        }
      }

    });
  }

}

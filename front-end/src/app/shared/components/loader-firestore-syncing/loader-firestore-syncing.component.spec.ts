import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoaderFirestoreSyncingComponent } from './loader-firestore-syncing.component';

describe('LoaderFirestoreSyncingComponent', () => {
  let component: LoaderFirestoreSyncingComponent;
  let fixture: ComponentFixture<LoaderFirestoreSyncingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoaderFirestoreSyncingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderFirestoreSyncingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceWorkerInstallButtonComponent } from './service-worker-install-button.component';

describe('ServiceWorkerInstallButtonComponent', () => {
  let component: ServiceWorkerInstallButtonComponent;
  let fixture: ComponentFixture<ServiceWorkerInstallButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiceWorkerInstallButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceWorkerInstallButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

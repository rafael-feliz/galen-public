import { Component, HostListener, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { Store } from '@ngrx/store';
import { GICON_FA_CLOUD_DOWNLOAD } from 'src/app/constants/font-awesome.constants';
import * as fromRoot from '../../../store/app.reducer';
import { ServiceWorkerInstallButtonService } from './service-worker-install-button.service';
import * as globalActions from '../../../store/global.actions';

@Component({
  selector: 'service-worker-install-button',
  templateUrl: './service-worker-install-button.component.html',
  styleUrls: ['./service-worker-install-button.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ServiceWorkerInstallButtonComponent implements OnInit, OnDestroy {

  constructor(private store: Store<fromRoot.State>,
    private service: ServiceWorkerInstallButtonService) { }

  @ViewChild(MatMenuTrigger) installAppMenu: MatMenuTrigger;
  gIcon_cloudDownload = GICON_FA_CLOUD_DOWNLOAD;
  customerId = "";
  userId = "";
  authStateSubscription;
  globalStateSubscription;
  showButton = false;
  settings_autoDisplayInstallAppButton = false;
  itWasAlreadyDisplayedOnThisSession = false;

  ngOnInit(): void {
    const context = this;

    this.authStateSubscription = context.store.select(fromRoot.getAuthState).subscribe(authState => {
      if (authState && authState.customerInfo) {
        context.userId = authState.email;
        context.customerId = authState.customerInfo.customerId;
        context.settings_autoDisplayInstallAppButton = authState._settings_autoDisplayInstallAppButton;


        //If this setting is true, then open the menu automatically, otherwise, just leave the button and the user to click it
        if (context.userId !== "" && context.customerId !== "") {

          //Prevent subscribing multiple times
          if (!context.globalStateSubscription) {

            //Get the browser install event from the state (It was set on the app.component.ts file), if available
            context.globalStateSubscription = context.store.select(fromRoot.getGlobalState).subscribe(globalState => {
              //Is the event set?
              if (globalState.browserInstallAppEnabledEvent) {
                context.showButton = true;
                //Automatically open the menu so that the user see it
                setTimeout(() => {
                  if (context.installAppMenu && !this.itWasAlreadyDisplayedOnThisSession && context.settings_autoDisplayInstallAppButton === true) {
                    context.installAppMenu.openMenu();
                    this.itWasAlreadyDisplayedOnThisSession = true;
                  }
                }, 4000);
              }
            });
          }
        }
      }
    });

  }

  // -----------------------------------

  ngOnDestroy(): void {

    if (this.authStateSubscription)
      this.authStateSubscription.unsubscribe();

    if (this.globalStateSubscription)
      this.globalStateSubscription.unsubscribe();

  }



  // -----------------------------------
  // Triggers the browser menu to install the app

  addToHomeScreen() {
    const context = this;
    this.globalStateSubscription = this.store.select(fromRoot.getGlobalState).subscribe(globalState => {
      var browserInstallAppEnabledEvent = globalState.browserInstallAppEnabledEvent;
      if (browserInstallAppEnabledEvent) {

        try {
          // Show the prompt
          browserInstallAppEnabledEvent.prompt();
          // Wait for the user to respond to the prompt
          browserInstallAppEnabledEvent.userChoice
            .then((choiceResult) => {
              if (choiceResult.outcome === 'accepted') {
                // If accepted, hide the button
                context.installAppMenu.closeMenu();
                this.showButton = false;
                //clear event
                this.store.dispatch(new globalActions.SetBrowserInstallButtonEvent(null));
              }
            });
        } catch (err) { }
      }
    });
  }


  // -----------------------------------
  //Close the menu and save to the database so that this windows is not shown again
  closeMenu() {
    this.service.dontAutomaticallyShowAgain(this.customerId, this.userId).then().catch();

    if (this.installAppMenu)
      this.installAppMenu.closeMenu();
  }

}

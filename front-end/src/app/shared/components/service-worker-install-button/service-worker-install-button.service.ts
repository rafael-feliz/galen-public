import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FC_CUSTOMERSDATA, FC_CUSTOMERSDATA_USERS, GalenICustomerUser } from 'galen-shared';
import { ErrorService } from 'src/app/services/errors/error.service';

@Injectable({
  providedIn: 'root'
})
export class ServiceWorkerInstallButtonService {

  constructor(private afFirestore: AngularFirestore, private errorService:ErrorService) { }

  /** Prevent the menu to auto open again */
  dontAutomaticallyShowAgain(customerId:string, userId:string):Promise<boolean> {
    const context = this;

    return new Promise(async resolve => {
      await this.afFirestore.doc<GalenICustomerUser>(`${FC_CUSTOMERSDATA}/${customerId}/${FC_CUSTOMERSDATA_USERS}/${userId}`)
      .update({_settings_autoDisplayInstallAppButton: false})
      .then(() => {
        resolve(true);
      }).catch(error => {
        context.errorService.showCustomErrorMessages(error);
        resolve(false);
      });
    });
  }
}

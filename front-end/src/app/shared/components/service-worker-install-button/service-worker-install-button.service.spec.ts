import { TestBed } from '@angular/core/testing';

import { ServiceWorkerInstallButtonService } from './service-worker-install-button.service';

describe('ServiceWorkerInstallButtonService', () => {
  let service: ServiceWorkerInstallButtonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceWorkerInstallButtonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

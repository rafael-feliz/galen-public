/* ===========================================================================
  Rafael Féliz

  To add a search button that when clicked, opens a search bar, and when typing or clicking the search button on the search bar, it
  will trigger the event onSearchEvent with the search value.

  Params:

  withBackground  (boolean)   Determines whether to show the button gray background or not

  Usage:

    On view:

    <icon-search-bar (onSearchEvent)="onSearch($event)"></icon-search-bar>

    On the ts file:

    onSearch(value:string) {
      console.log(value);
    }

   ===========================================================================
*/

import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { GICON_FA_ARROW_LEFT, GICON_SEARCH } from 'src/app/constants/font-awesome.constants';

@Component({
  selector: 'icon-search-bar',
  templateUrl: './icon-search-bar.component.html',
  styleUrls: ['./icon-search-bar.component.scss']
})
export class IconSearchBarComponent implements OnInit, OnDestroy {

  GICON_SEARCH = GICON_SEARCH;
  GICON_FA_ARROW_LEFT = GICON_FA_ARROW_LEFT;
  barIsVisible = false;
  searchTimeout;
  searchForm;
  searchSubscription;

  @Input("withBackground") withBackground:boolean = false;
  @Output() onSearchEvent = new EventEmitter<string>(); //to return the event with the typed text
  @ViewChild("searchElement") searchElement: ElementRef; //to focus the element

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    //create the search form
    this.searchForm = this.formBuilder.group({
      search: ['', [Validators.required]]
    });

    //Subscribe to input changes. It will run a timeout before returning the typed text, so that this event doesn't fire
    //every time you type a character
    this.searchSubscription = this.searchForm.get("search").valueChanges.subscribe(selectedValue => {
      if (this.searchTimeout)
        clearTimeout(this.searchTimeout);

      this.searchTimeout = setTimeout(() => {
        this.onSearch();
      }, 1000);
    });
  }

  //Unsubscribe from things
  ngOnDestroy(): void {
    //Clear the search subscription (The listener for search field changes)
    if (this.searchSubscription)
      this.searchSubscription.unsubscribe();

    //clear the search timeout in case it's still waiting before closing
    if (this.searchTimeout)
      clearTimeout(this.searchTimeout);
  }

  //Show/hide search bar
  toggle() {
    this.searchForm.controls.search.value = "";
    this.searchForm.reset();
    this.barIsVisible = !this.barIsVisible;
    setTimeout(() => {
      if(this.searchElement)
        this.searchElement.nativeElement.focus(); }, 1000);
  }

  //To emit the event with the typed search text
  onSearch() {
    if(this.searchForm.controls.search.value)
      this.onSearchEvent.emit(this.searchForm.controls.search.value);
  }

}

/* =============================================================================
  Rafael Féliz

  Just an angular material reusable confirm dialog.

    USAGE:
    --------------------
    constructor(private dialog: MatDialog) {}

  confirmDialog(): void {
    const dialogData: IConfirmDialog = {
      titleLangRef: "The modal title or title language reference",
      messageLangRef: "the modal message or message language reference",
      isWarning: true
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {data: dialogData});
    dialogRef.afterClosed().subscribe(dialogResult => {
      console.log(dialogResult);
    });
  }


 ============================================================================= */


 import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  title: string;
  message: string;
  isWarning: boolean;

  constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IConfirmDialog) {
    // Update view with given values
    this.title = data.titleLangRef;
    this.message = data.messageLangRef;
    this.isWarning = data.isWarning;
  }

  ngOnInit(): void {
  }

  onConfirm(): void {
    // Close the dialog, return true
    this.dialogRef.close(true);
  }

  onDismiss(): void {
    // Close the dialog, return false
    this.dialogRef.close(false);
  }
}

export interface IConfirmDialog {
  titleLangRef?: string,
  messageLangRef: string,
  isWarning?: boolean
}

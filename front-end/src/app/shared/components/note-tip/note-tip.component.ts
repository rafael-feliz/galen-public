/*  ================================================================

    Rafael Feliz

    If you want to add a note, like a help tip that is always displayed as a note, then use this component
    Usage:
    <note-tip  message=""></inline-notification>

    Params:

    message           The message to display on the note
    show-line         To show or hide a line above the comment
    show-icon         To show or hide the icon
    show-smyle        If true, the smyle message icon button will appear. Otherwise, the info icon appears

    ================================================================
*/


import { Component, Inject, Input, OnInit } from '@angular/core';
import { GICON_NOTETIPICON, GICON_FA_INFOSQUARE } from '../../../constants/font-awesome.constants';

@Component({
  selector: 'note-tip[message]',
  templateUrl: './note-tip.component.html',
  styleUrls: ['./note-tip.component.scss']
})
export class NoteTipComponent implements OnInit {

  @Input('message') message = "";
  @Input('show-line') showLine = true;
  @Input('show-icon') showIcon = true;
  @Input('show-smyle') showSmyle = false;

  GICON_NOTETIPICON = GICON_NOTETIPICON;
  GICON_FA_INFOSQUARE = GICON_FA_INFOSQUARE;

  constructor() { }

  ngOnInit(): void {
  }

}

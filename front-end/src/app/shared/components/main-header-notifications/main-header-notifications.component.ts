import { AfterContentInit, AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngrx/store';
import { GalenICustomerUserNotifications } from 'galen-shared';
import { GICON_FA_CIRCLE, GICON_NOTIFICATION, GICON_FA_SOLID_CIRCLE } from 'src/app/constants/font-awesome.constants';
import * as fromRoot from '../../../store/app.reducer';
import { MainHeaderNotificationsService } from './main-header-notifications.service';
import * as _ from 'lodash';
import { NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { PATH_ACCOUNT, PATH_ACCOUNT_EMAILSQUEUE } from 'src/app/constants/routing-paths.constants';
import { ErrorService } from 'src/app/services/errors/error.service';

@Component({
  selector: 'main-header-notifications',
  templateUrl: './main-header-notifications.component.html',
  styleUrls: ['./main-header-notifications.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainHeaderNotificationsComponent implements OnInit, OnDestroy {

  GICON_NOTIFICATION = GICON_NOTIFICATION;
  GICON_FA_CIRCLE = GICON_FA_CIRCLE;
  GICON_FA_SOLID_CIRCLE = GICON_FA_SOLID_CIRCLE;
  notificationsCount = 0;
  customerId;
  userId;
  isOffline = false;
  notifications: GalenICustomerUserNotifications[];
  loading = true;
  authStateSubscription;
  globalStateSubscription;

  constructor(private store: Store<fromRoot.State>,
    private notificationsService: MainHeaderNotificationsService,
    private ngZone:NgZone,
    private router: Router,
    private errorService: ErrorService ) { }

  // -------------------
  ngOnInit(): void {

    this.authStateSubscription = this.store.select(fromRoot.getAuthState).subscribe(authState => {
      this.notificationsCount = authState._settings_notificationsCount;
      this.customerId = authState.customerInfo ? authState.customerInfo.customerId : null;
      this.userId = authState.email;
    });

    this.globalStateSubscription = this.store.select(fromRoot.getGlobalState).subscribe(globalState => {
      this.isOffline = globalState.isOffline;
    });

  }

  // -------------------
  ngOnDestroy() {
    if(this.globalStateSubscription)
      this.globalStateSubscription.unsubscribe();

    if(this.authStateSubscription)
      this.authStateSubscription.unsubscribe();
  }


  // -------------------
  async loadNotifications() {
    this.loading = true;
    const context = this;

    await this.notificationsService.getNotifications().then(notifications => {
      this.notifications = notifications;
      this.loading = false;

    }).catch(error => {
      context.errorService.showCustomErrorMessages(error);
      this.loading = false;
    });
  }

  //-------------------------
  //Reset the notifications count when the menu is closed
  resetNotifications() {
    this.notificationsService.resetUserNotificationsCount();
  }

  // ---------------------
  /**Gets the notification avatar, it may be a custom url or none. If none, it will get the default icons for the status */
  getAvatarUrl(notification:GalenICustomerUserNotifications) {
    let returnValue = "";

    if(notification.avatarUrl)
      returnValue = notification.avatarUrl;
    else {
      if(_.indexOf(["success", "warning", "info", "error"], notification.status) > -1) {
        return `/assets/img/image-icons/galen-asset_notifications-${notification.status}.svg`;
      } else {
        returnValue = "/assets/img/image-icons/galen-asset_notifications-default.svg";
      }
    }
    return returnValue;
  }


  handleAction(notification:GalenICustomerUserNotifications) {
    if(notification.action === "goto.emailqueue"){
      this.ngZone.run(()=>this.router.navigate(["/" + PATH_ACCOUNT + "/" + PATH_ACCOUNT_EMAILSQUEUE]));
    }
  }

}

import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { Store } from '@ngrx/store';
import { firestore } from 'firebase-admin/lib/firestore';
import { GalenICustomerUserNotifications, FC_CUSTOMERSDATA, FC_CUSTOMERSDATA_USERS, FC_CUSTOMERSDATA_USERS_NOTIFICATIONS, GalenICustomerUser, GALENC_EMAIL_NOTIFICATIONS_ADDR, FCP_DATEUNIX } from 'galen-shared';
import { take } from 'rxjs/operators';
import { CLOUD_FUNCTION_USERNOTIFICATIONS_RESETCOUNT } from 'src/app/constants/cloud-functions.constants';
import { ErrorService } from 'src/app/services/errors/error.service';
import * as fromRoot from '../../../store/app.reducer';

@Injectable({
  providedIn: 'root'
})
export class MainHeaderNotificationsService {

  constructor(private store: Store<fromRoot.State>,
    private afFirestore: AngularFirestore,
    private afFunctions: AngularFireFunctions,
    private errorService: ErrorService) { }


  //-------------------------

  async getNotifications(): Promise<GalenICustomerUserNotifications[]> {

    let notifications: GalenICustomerUserNotifications[] = [];
    const context = this;

    return new Promise(async resolve => {
      await this.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(async authState => {
        let customerId = authState.customerInfo.customerId;
        let userId = authState.email;

        await this.afFirestore.collection<GalenICustomerUserNotifications>(`${FC_CUSTOMERSDATA}/${customerId}/${FC_CUSTOMERSDATA_USERS}/${userId}/${FC_CUSTOMERSDATA_USERS_NOTIFICATIONS}/`).ref.orderBy(FCP_DATEUNIX, "desc").get().then(querySnapshot => {
          querySnapshot.forEach(notification => {
            if(notification.exists)
              notifications.push(notification.data());
          });
          resolve(notifications);
        }).catch(error => {
          context.errorService.showCustomErrorMessages(error);
          resolve([]);
        });
      });
    });
  }


  //-------------------------
  //Reset the notifications count

  async resetUserNotificationsCount() {
    await this.afFunctions.httpsCallable(CLOUD_FUNCTION_USERNOTIFICATIONS_RESETCOUNT)({}).toPromise().then(() => { }).catch(() => { });
  }
}

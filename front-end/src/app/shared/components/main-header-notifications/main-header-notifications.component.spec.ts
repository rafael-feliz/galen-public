import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainHeaderNotificationsComponent } from './main-header-notifications.component';

describe('MainHeaderNotificationsComponent', () => {
  let component: MainHeaderNotificationsComponent;
  let fixture: ComponentFixture<MainHeaderNotificationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainHeaderNotificationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainHeaderNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

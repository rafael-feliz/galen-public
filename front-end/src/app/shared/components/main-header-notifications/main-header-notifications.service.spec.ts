import { TestBed } from '@angular/core/testing';

import { MainHeaderNotificationsService } from './main-header-notifications.service';

describe('MainHeaderNotificationsService', () => {
  let service: MainHeaderNotificationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MainHeaderNotificationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

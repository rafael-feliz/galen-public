/** =========================================================================================

    Rafael Féliz 2020

    A component to display a nothing found icon.

    Params:

    height        -->     Determine the size of the image
    titleRef      -->     The language reference for the title, or just a text
    subtitleRef   -->     The language reference for the sub title, or just a text
    image         -->     #1 to display the big image, and #2 to display the smaller image

    Usage:
      <nothing-found titleRef="mainHeader.notifications.nothingFoundTitle"
        subtitleRef="mainHeader.notifications.nothingFoundSubTitle">
      </nothing-found>

    =========================================================================================
*/

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'nothing-found[titleRef]',
  templateUrl: './nothing-found.component.html',
  styleUrls: ['./nothing-found.component.scss']
})
export class NothingFoundComponent implements OnInit {

  @Input("height") height:number = 500;
  @Input("titleRef") titleRef:string = "";
  @Input("subtitleRef") subtitleRef:string = "";
  @Input("image") image:number = 1;

  constructor() { }

  ngOnInit(): void {
  }

}

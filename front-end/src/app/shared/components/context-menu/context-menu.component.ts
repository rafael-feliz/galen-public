/* ====================================================================
  Rafael Feliz

  This creates a mat-icon-button with 3 dots, and when you click it, it will show a mat-menu with the options you add as params.


      Params:
      ---------

        content (GalenIContextMenuParams)   An array of options. If you set
        data    (any)                       Any data object that you want the function to return on the onClickEvent

      Events
      ---------

        onClickEvent (It will return the action that was clicked. The action is taken from the params item actionName that was clicked (See GalenIContextMenuParams))


      Usage:
      ---------

        On the view:
        ---------

            If using a list, is good that you add the <span class="actions"> as seen below, so that the context menu is set to the end

              <span class="actions">
                  <context-menu [content]="contextMenu" [data]="user" (onClickEvent)="onContextMenuClick($event)"></context-menu>
              </span>

        On controller:
        ---------

              //The params
              contextMenu: Array<GalenIContextMenuParams> = [
                  { actionName: "view", iconRef: GICON_VIEW, langRef: 'general.view' },
                  { actionName: "edit", iconRef: GICON_EDIT, langRef: 'general.edit' },
                  { isSeparator: true },
                  { actionName: "delete", iconRef: GICON_DELETE, langRef: 'general.delete' }
                ];

              //When an item is clicked
              onContextMenuClick(result): void {
                const action = result.action;
                const user = result.data as GalenICustomerUser;
                ...
              }


   ==================================================================== */

import { EventEmitter, Input, Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { GICON_DOTS_OPTIONS } from 'src/app/constants/font-awesome.constants';
import { GalenIContextMenuParams } from '../../../../../../galen-shared/interfaces/customersData/common.interface';

@Component({
  selector: 'context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.scss']
})
export class ContextMenuComponent implements OnInit {

  GICON_DOTS_OPTIONS = GICON_DOTS_OPTIONS;

  @Input("content") content:Array<GalenIContextMenuParams> = [];
  @Input("data") data:any;
  @Output() onClickEvent = new EventEmitter<{action: string, data: any}>(); //to return the event with the typed text

  constructor() { }

  ngOnInit(): void {
  }

  //prevent the button that opens the menu to propagate the click event
  onMenuTriggerButtonClick(event:any) {
    event.stopPropagation();
    event.preventDefault();
    return false;
  }

  //sends the event to the parent
  onClick(action:string) {
    this.onClickEvent.emit({action: action, data: this.data});
  }
}

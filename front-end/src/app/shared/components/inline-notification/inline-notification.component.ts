/*
  Displays a message notification.

  Usage: <inline-notification message="" type=""></inline-notification>

  Attributes:
    message: the message to display
    type: the style to apply that can be: success, error, warning, info.
*/

import { Component, OnInit, OnChanges, Input } from '@angular/core';

@Component({
  selector: 'inline-notification',
  templateUrl: './inline-notification.component.html',
  styleUrls: ['./inline-notification.component.scss']
})
export class InlineNotificationComponent implements OnInit, OnChanges {

  visible = -1;

  @Input('message') message = "";
  @Input('type') type: "success" | "error" | "warning" | "info" = "success";

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(){
    this.showHide();
  }

  showHide(){
    if(this.message !== ""){
      this.visible = 2;
      setTimeout(() =>{
        this.visible = 1;
      },20);
    } else {
      this.visible = 0;
      setTimeout(() =>{
        this.visible = -1;
      },100);
    }
  }
}

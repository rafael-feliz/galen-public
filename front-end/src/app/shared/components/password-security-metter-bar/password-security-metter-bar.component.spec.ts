import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordSecurityMetterBarComponent } from './password-security-metter-bar.component';

describe('PasswordSecurityMetterBarComponent', () => {
  let component: PasswordSecurityMetterBarComponent;
  let fixture: ComponentFixture<PasswordSecurityMetterBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PasswordSecurityMetterBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordSecurityMetterBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

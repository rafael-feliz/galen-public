/* =============================================================================
  Rafael Féliz

  To display a percentage bar that indicates how strong is a password. It is used on register and user creation.

  Usage:

  Input: <input type="password" formControlName="password" matInput (focus)="passIsFocus=true" (blur)="passIsFocus=false">
  component: <password-security-metter-bar [passwordString]="registerForm.controls.password.value" [inputFocused]="passIsFocus"></password-security-metter-bar>

  Required on teh @Component definition if you will modify CSS:
    encapsulation: ViewEncapsulation.None

 ============================================================================= */

import { Component, Input, OnChanges, OnInit, ViewEncapsulation } from '@angular/core';
import { GalenFPasswordStrongResult, GalenIPasswordStrongResult } from 'galen-shared';
import { GICON_FA_TIMESCIRCLE, GICON_FA_SOLIDCHECKCIRCLE } from 'src/app/constants/font-awesome.constants';

@Component({
  selector: 'password-security-metter-bar[passwordString][inputFocused]',
  templateUrl: './password-security-metter-bar.component.html',
  styleUrls: ['./password-security-metter-bar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PasswordSecurityMetterBarComponent implements OnInit, OnChanges {

  @Input("passwordString") passwordString:string = "";
  @Input("inputFocused") inputFocused:boolean;

  GICON_FA_TIMESCIRCLE = GICON_FA_TIMESCIRCLE;
  GICON_FA_SOLIDCHECKCIRCLE = GICON_FA_SOLIDCHECKCIRCLE;
  passwordStrongResult:GalenIPasswordStrongResult = { isSecure: false, percent: 0, hasEightChars: false, hasLower: false, hasNumber: false, hasSpecialChar: false, hasUpper: false };

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    //Reset
    this.passwordStrongResult = { isSecure: false, percent: 0, hasEightChars: false, hasLower: false, hasNumber: false, hasSpecialChar: false, hasUpper: false };
    this.passwordStrongResult = GalenFPasswordStrongResult(this.passwordString);
  }


}

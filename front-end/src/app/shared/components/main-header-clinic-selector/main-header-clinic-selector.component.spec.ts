import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainHeaderClinicSelectorComponent } from './main-header-clinic-selector.component';

describe('MainHeaderClinicSelectorComponent', () => {
  let component: MainHeaderClinicSelectorComponent;
  let fixture: ComponentFixture<MainHeaderClinicSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainHeaderClinicSelectorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainHeaderClinicSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

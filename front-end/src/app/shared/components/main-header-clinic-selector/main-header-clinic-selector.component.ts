import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FC_CUSTOMERSDATA_CLINICS, GALENC_MAIN_CLINIC_COLLECTION_DOCUMENT_ID, GalenICustomerClinic } from 'galen-shared';
import { GICON_FA_ANGLEDOWN, GICON_FA_EXTERNAL_LINK, GICON_CLINIC } from 'src/app/constants/font-awesome.constants';
import { CustomerService } from '../../../services/customer/customer.service';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../store/app.reducer';
import { take } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core'
import * as globalActions from '../../../store/global.actions';
import { PATH_ACCOUNT, PATH_ACCOUNT_CLINICS } from 'src/app/constants/routing-paths.constants';


@Component({
  selector: 'main-header-clinic-selector',
  templateUrl: './main-header-clinic-selector.component.html',
  styleUrls: ['./main-header-clinic-selector.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainHeaderClinicSelectorComponent implements OnInit, OnDestroy {

  PATH_ACCOUNT = PATH_ACCOUNT;
  PATH_ACCOUNT_CLINICS = PATH_ACCOUNT_CLINICS;
  GICON_FA_ANGLEDOWN = GICON_FA_ANGLEDOWN;
  GICON_FA_clinic = GICON_CLINIC;
  GICON_FA_externalLink = GICON_FA_EXTERNAL_LINK;
  clinicInfo: GalenICustomerClinic = {};
  clinics: Array<GalenICustomerClinic> = [];
  clinicDocSubscription;
  clinicCollectionsSubscription;
  authStateSubscription;
  customerId = "";
  userId = "";
  selectedClinicId = GALENC_MAIN_CLINIC_COLLECTION_DOCUMENT_ID;
  isLoading = true;
  firebaseStorageLocationPath = "";

  constructor(private customerService: CustomerService,
    private store: Store<fromRoot.State>,
    private snackBar: MatSnackBar,
    private translateService: TranslateService) { }

  // ---------------------------------

  ngOnInit(): void {
    //Get the selected user details
    this.authStateSubscription = this.store.select(fromRoot.getAuthState).subscribe(authState => {

      if (authState && authState.customerInfo) {
        if (authState.customerInfo.customerId && authState._settings_selectedClinicId) {

          //Unsubscribe from authState because the data was loaded.
          if (this.authStateSubscription)
            this.authStateSubscription.unsubscribe();

          this.selectedClinicId = authState._settings_selectedClinicId ? authState._settings_selectedClinicId : GALENC_MAIN_CLINIC_COLLECTION_DOCUMENT_ID;
          this.customerId = authState.customerInfo.customerId;
          this.userId = authState.email;

          //Get clinic logo file firestore path
         this.firebaseStorageLocationPath = `/${FC_CUSTOMERSDATA_CLINICS}/${this.selectedClinicId}`;

          /**Subscribe to clinic document changes */
          this.clinicDocSubscription = this.customerService.subscribeToClinicDocumentChanges(this.selectedClinicId, this.customerId).subscribe(snapshot => {
            this.isLoading = false;
            this.clinicInfo = snapshot.payload.data();
          });

          /**Subscribe to the clinic collection list changes */
          this.clinicCollectionsSubscription = this.customerService.subscribeToClinicCollectionsChanges(this.customerId).subscribe(snapshot => {
            this.isLoading = false;
            this.clinics = [];
            snapshot.forEach(doc => {
              if (doc.payload.doc.data())
                if (doc.payload.doc.id !== this.selectedClinicId) //Don't add the current selected clinic
                  this.clinics.push(doc.payload.doc.data());
            });
          });
        }
      }
    });
  }


  // ---------------------------------

  ngOnDestroy() {
    if (this.clinicDocSubscription)
      this.clinicDocSubscription.unsubscribe();

    if (this.clinicCollectionsSubscription)
      this.clinicCollectionsSubscription.unsubscribe();
  }


  // ---------------------------------

  async setSelectedClinic(newClinicId: string, newClinicName: string) {
    this.store.dispatch(new globalActions.ShowFullScreenLoader(true));
    await this.customerService.setSelectedClinic(newClinicId, newClinicName, this.customerId, this.userId).then(result => {
      if (result) {
        window.location.href = window.location.href; //reload the page
      } else {
        this.store.dispatch(new globalActions.ShowFullScreenLoader(false)); //remove the loading indicator if error
        this.translateService.get('general.genericError').pipe(take(1)).subscribe(message => {
          const snack = this.snackBar.open(message, "", {
            duration: 6000,
            panelClass: 'error'
          });
          snack;
        })

      }
    });
  }


}

/* =============================================================================
  Rafael Féliz

  This is the component to be used to display the upload button.
  Usage: Just add the <file-upload></file-upload> element, with the following attributes.

  Note: It will return the uploaded items to the store. You should listen to file-upload store changes in order to
        read uploaded data


    EVENTS
    --------------------
      on-file-uploaded              (function)        A function that will be triggered when the upload is finished. A "files" param will be available and it will hold the files list with the MODEL in models/file-upload.model.ts.
                                                    Usage: (onFileUploaded)="onFileUploaded($event)"
                                                    Note: You don't need to manage firestore document update because is automatically handled. This is used if you want to do something after upload

    PARAMS (Required):
    --------------------

    firebase-storage-path             (string)         Where to store the attachment.
                                                      USE: fileLocations types from galen-shared/firebase/storage.ts

    firestore-path                    (string)         The firestore path to update/insert the file reference url, and add the real file url with firebase storage token
                                                      (Dont include customerID. Start with /)

                                                      IMPORTANT NOTE:
                                                      -------------------
                                                      It will use the allowed-files-count to know whether it should insert or update firestore references.

                                                      If files count is more than 1 file (default), it will insert. Firestore PATH should end with a COLLECTION.
                                                      If files count is 1, it will update. Firestore PATH should end with a DOCUMENT

    firestore-property-name            (string)         The propery to set the file on firestore.
                                                      Leave empty if you don't want to use a property, and set the photo model directly on the document
                                                      { firestorePropertyName: theValueThatWillBeSet }



    PARAMS (Optional):
    --------------------

      record-id                     (string)        If the attachment should be inside a record ID (i.e. a patient ID to store attachments inside of it)
      allowed-files-count           (number)        how many files are allowed to upload. Default 1
      allow-any-file                (bool)          Default false. It will allow to upload only images...

      button-type                   (string)        The angular material button color
      button-text                   (string)        Default is UPLOAD. To change, provide the language string or text here.
      show-icon                     (Bool)          Show an upload icon?
      icon-button-only              (bool)          Show an angular icon button instead a button. Button-text will appear as tooltip if set.
      max-file-size-mb              (int)           If you want to change the default upload file size


    USAGE:
    --------------------
    <file-upload [file-location]="fileLocation" (on-file-uploaded)="onFileUploaded($event)"></file-upload>


    //=====================================
    //On file uploaded

    onFileUploaded = async (files: Array<GalenIFileUploaded>) => {

      if (files) {
        if (files.length > 0) {

        }
      }
    };


 ============================================================================= */


import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GICON_FA_CLOUDUPLOAD } from 'src/app/constants/font-awesome.constants';
import { FileUploadContentComponent } from './file-upload-content.component';
import { FileUploadDialogContentParamsModel } from './file-upload.model';
import * as fromRoot from '../../../store/app.reducer';
import { Store } from '@ngrx/store';
import { GalenAdminIBillingPlans_ForPublicUsage, GalenTfileLocations } from 'galen-shared';
import { FileUploadService } from './file-upload.service';
import { environment } from 'src/environments/environment';
import { MiscService } from 'src/app/services/misc/misc.service';

// https://material.angular.io/components/dialog/overview

@Component({
  selector: 'file-upload[firebase-storage-path][firestore-path]', //Add required attributes here...
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit, OnDestroy {

  //EVENTS===========================================

  @Output() onFileUploaded = new EventEmitter<any>();

  //PARAMS===========================================

  //REQUIRED
  @Input("firebase-storage-path") firebaseStoragePath: GalenTfileLocations;
  @Input("firestore-path") firestorePath: string = "";


  //OPTIONAL
  @Input("firestore-property-name") firestorePropertyName: string = "";
  @Input("record-id") recordId: string;
  @Input("allowed-files-count") allowedFilesCount: number = 30;
  @Input("allow-any-file") allowAnyFile: boolean = false;

  @Input("button-type") buttonType: "primary" | "warn" | "accent" | "" = "";
  @Input("button-text") buttonText: string = "";
  @Input("show-icon") showIcon: boolean = false;
  @Input("icon-button-only") iconButtonOnly: boolean = false;
  @Input("max-file-size-mb ") maxFileSizeMb: number = 60;

  GICON_FA_CLOUDUPLOAD = GICON_FA_CLOUDUPLOAD;
  isOffline$;
  customerId = "";
  billingPlanDetails: GalenAdminIBillingPlans_ForPublicUsage;
  customerSubscription;
  billingSubscription;

  constructor(public dialog: MatDialog,
    private store: Store<fromRoot.State>,
    private fileUploadService: FileUploadService,
    private miscService: MiscService) { }

  // ----------------------------------------------
  ngOnInit() {

    //Get the current logged in user customer ID
    this.customerSubscription = this.store.select(fromRoot.getAuthState).subscribe(authState => {
      if (authState.customerInfo)
        this.customerId = authState.customerInfo.customerId;
    });

    //Get the current logged in user billing plan detail
    // this.billingSubscription = this.store.select(fromRoot.getAuthState).subscribe(authState => {
    //   if (authState.billingPlanDetails)
    //     this.billingPlanDetails = authState.billingPlanDetails;
    // });

    this.miscService.getCustomerBillingPlanDetails().then(planDetail => {
      if (planDetail)
        this.billingPlanDetails = planDetail;
    });

  }

  ngOnChanges() {
  }

  // ----------------------------------------------
  openDialog() {

    //Remember the developer to create function to update files
    if (environment.local)
      console.log("%cFILE UPLOAD WARNING: Remember to create cloud function trigger to remove files after they are updated or removed. See, in functions project, the removeFileAfterUpdateOrDelete method. An example can be seen in the clinics.onUpdate.trigger.ts file.", "color: orange; font-size: 15px");

    if (this.customerId !== "") {
      //Prepare the params to be added to the dialog content component
      const dataParams: FileUploadDialogContentParamsModel = {
        customerId: this.customerId,
        billingPlanDetails: this.billingPlanDetails,
        firebaseStoragePath: this.firebaseStoragePath,
        recordId: this.recordId,
        allowedFilesCount: this.allowedFilesCount,
        allowAnyFile: this.allowAnyFile,
        maxFileSizeMb: this.maxFileSizeMb,
        files: []
      };

      const dialogRef = this.dialog.open(FileUploadContentComponent, {
        data: dataParams,
        autoFocus: false,
        disableClose: true
      });


      // ---------------
      //Wait the modal to close, then run the after Upload event
      //Also submit files upload to firebase firestore
      dialogRef.beforeClosed().subscribe(async files => {
        if (files && files.length > 0) {
          //Update/set firestore data
          await this.fileUploadService.updateFirestoreFilesReferences(this.allowedFilesCount > 1, this.firestorePath, this.firestorePropertyName, files);

          //Return and send the onFileUploaded event
          if (this.onFileUploaded) {
            this.onFileUploaded.emit(files);
          }
        }
      });


    } else {
      console.error('Missing customer ID. Please refresh your browser and try again.');
    }
  }


  // ----------------------------------------------
  ngOnDestroy() {

    if(this.customerSubscription)
      this.customerSubscription.unsubscribe();

    if(this.billingSubscription)
       this.billingSubscription.unsubscribe();
  }
}

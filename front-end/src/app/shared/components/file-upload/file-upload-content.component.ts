/* =============================================================================
  Rafael Féliz

  The file upload Dialog Content. This component is dependant of the file-upload.component.ts
  Files are uploaded to firebase storage path: customerID/fileLocation/recordID (If available)/fileName

  Angular Fire Storage Reference
  https://github.com/angular/angularfire/blob/HEAD/docs/storage/storage.md
  https://medium.com/@mariemchabeni/angular-7-drag-and-drop-simple-file-uploadin-in-less-than-5-minutes-d57eb010c0dc


 ============================================================================= */


import { HostListener, Inject, OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FileUploadDialogContentParamsModel } from './file-upload.model';
import { GICON_FA_CLOUDUPLOAD, GICON_FA_FILE, GICON_FA_CHECK, GICON_X } from '../../../constants/font-awesome.constants';
import * as _ from 'lodash';
import { AngularFireStorage } from '@angular/fire/storage';
import { GalenFCheckIfFileIsAllowed, GALENC_IMAGES_MIME_TYPES, GALENC_ALL_FILES_MIME_TYPES, GalenIFileMetadata } from 'galen-shared';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core'
import { take } from 'rxjs/operators';
import { MiscService } from 'src/app/services/misc/misc.service';
import { environment } from 'src/environments/environment';
import * as fromRoot from '../../../store/app.reducer';
import { Store } from '@ngrx/store';
import { ErrorService } from 'src/app/services/errors/error.service';

@Component({
  selector: 'app-file-upload-content',
  templateUrl: './file-upload-content.component.html',
  styleUrls: ['./file-upload-content.component.scss']
})
export class FileUploadContentComponent implements OnInit, OnDestroy {

  //FA ICONS
  GICON_FA_CLOUDUPLOAD = GICON_FA_CLOUDUPLOAD;
  GICON_X = GICON_X;
  GICON_FA_FILE = GICON_FA_FILE;
  GICON_FA_CHECK = GICON_FA_CHECK;

  //VARS
  currentQuotaSubscription;
  uploadStage: number = 1;
  draggingClass: string = "not-dragging";
  allFilesMimeTypes: string = GALENC_ALL_FILES_MIME_TYPES;
  imagesMimeTypes: string = GALENC_IMAGES_MIME_TYPES;
  files = [];
  globalPercent: number = 0;
  usedSpace: number = 0;
  totalSpace: number = 0;
  additionalSpaceMb: number = 0;
  finalized = false;
  lodashReplace = _.replace; //to use in the template
  devEnvironment = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: FileUploadDialogContentParamsModel,
    private storage: AngularFireStorage,
    private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<FileUploadContentComponent>,
    private translateService: TranslateService,
    private miscService: MiscService,
    private store: Store<fromRoot.State>,
    private errorService:ErrorService) { }

  ngOnInit() {


    //Refresh
    this.getCurrentQuota();

    //Set the plan total space

    this.totalSpace = this.data.billingPlanDetails.quotas ? this.data.billingPlanDetails.quotas.storageSizeMb : 100; //This IF will be only when testing the app localhost that the plans will not exist for the first time. This prevents an error.

    if(environment.local)
      this.devEnvironment = true;
  }

  // ----------------------------------------------------
  // START DRAG EVENTS

  //Dragover listener
  @HostListener('dragover', ['$event']) onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.draggingClass = "dragging";
  }

  //Dragleave listener
  @HostListener('dragleave', ['$event']) public onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.draggingClass = "not-dragging";
  }
  //Drop listener
  @HostListener('drop', ['$event']) public ondrop(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    const files = evt.dataTransfer.files;

    if (files.length > 0) {

      //-------------
      // Upload file
      this.uploadFiles(files);

    } else {
      this.draggingClass = "not-dragging";
    }
  }
  // END DRAG EVENTS
  // ----------------------------------------------------

  //================================
  // Upload files

  uploadFiles = (files) => {

    if (!this.validateFilesAfterDraggingOrBrowsing(files))
      return;


    this.uploadStage = 2;
    this.files = [];
    const context = this;

    //fill the files array
    for (let index = 0; index < files.length; index++) {
      const file = files[index];

      if (GalenFCheckIfFileIsAllowed(file.type, this.data.allowAnyFile)) {
        this.files.push(file);
      }
    }

    // ---------------
    //UPLOAD TO FIREBASE AND BUILD RETURN DATA
    this.files.forEach(async file => {

      //Set the image preview base 64 string
      this.previewImage(file);

      file.modifiedFileName = _.replace(file.name, / /g, "_");
      //Add a random number before the extension
      file.modifiedFileName = file.modifiedFileName.replace(/(\.[\w\d_-]+)$/i, "_" + _.random(1000, 999999) + '$1');


      //To store the firebase path without the customer ID. The customer ID is always added while reading data, as has been done below when uploading
      const firebasePath =  this.data.firebaseStoragePath + (this.data.recordId ? '/' + this.data.recordId : '') + '/' + file.modifiedFileName;

      //Upload path on firebase: customerID/fileLocation/recordID (If available)/fileName
      const firebaseBucketPath = this.data.customerId  + '/' + firebasePath;
      const ref = this.storage.ref(firebaseBucketPath);

      //Set the file metadata. This is used in cloud functions that run when files are created/deleted
      const fileMetadata: GalenIFileMetadata = {
        customerId: this.data.customerId,
        snapshot_useSpc: this.usedSpace.toString(), //used space
        snapshot_toSpc: (this.totalSpace + this.additionalSpaceMb).toString()  //total space
      }

      file.firebasePath = firebasePath;
      file.task$ = ref.put(file, { customMetadata: { ...fileMetadata }, cacheControl: "public,max-age=1296000", contentType: file.type });

      // observe percentage changes
      file.uploadPercent$ = file.task$.percentageChanges();

      //Subscribe to percentage changes and apply to a non observable variable, and regresh the totals
      file.uploadPercentSubscription = file.uploadPercent$.subscribe(value => {
          if(value > 0){ //update only when value is more than 0. There are some cases with very small files that the value returns from 100 to 0 (Unkown why)
            file.uploadPercent = value;
            context.getTotalPercent();
          }
      });

      file.task$.snapshotChanges().toPromise().then(async result => {
        //Modify the params model to include the files so that they're accessed from the file-upload-component. It will be returned via the [mat-dialog-close] attribute on the template file-upload-content.component.html
        context.data.files.push({
          name: file.modifiedFileName,
          sizeMb: file.size * 0.000001, //convert to mb
          type: file.type,
          file: {
            imageLarge: await this.miscService.getFileUrl(file.firebasePath, 1200, true),
            imageMed: await this.miscService.getFileUrl(file.firebasePath, 400, true),
            imageSmall: await this.miscService.getFileUrl(file.firebasePath, 200, true),
            originalFilePath: file.firebasePath
          },
          imageCode: file.imageCode ? file.imageCode : null,
          isImage: _.indexOf(file.type, "image") > -1
        });

      }).catch(error => {
        context.errorService.showCustomErrorMessages(error);
        return;
      });

    });

  }


  //================================
  // validate files after dragging or browsing

  validateFilesAfterDraggingOrBrowsing = (files: any): boolean => {

    const context = this;
    //-------------
    //If no space available
    if (this.getSpaceUsedPercent() >= 100) {
      context.translateService.get('upload.quotaExceeded').pipe(take(1)).subscribe(value => {
        const snack = this.snackBar.open(value, '', {
          duration: 8000,
          panelClass: 'error'
        });
        snack;
      });
      this.draggingClass = "dragging-not-allowed"
      setTimeout(() => { context.draggingClass = "not-dragging" }, 3000);
      return false;
    }

    //-------------
    //If space available is less than files you're trying to upload
    const uploadSizeMb = _.sumBy(files, function (f) { return f.size; }) * 0.000001; //convert to mb;

    if (this.getAvailableSpaceMb() < uploadSizeMb) {
      context.translateService.get('upload.willExceedQuota').pipe(take(1)).subscribe(value => {
        const snack = this.snackBar.open(value, '', {
          duration: 8000,
          panelClass: 'error'
        });
        snack;
      });

      this.draggingClass = "dragging-not-allowed"
      setTimeout(() => { context.draggingClass = "not-dragging" }, 3000);
      return false;
    }

    //-------------
    //Check if you're uploading more files than allowed
    if (files.length > this.data.allowedFilesCount) {
      context.translateService.get('upload.allowedFilesCountWarning').pipe(take(1)).subscribe(value => {
        const snack = this.snackBar.open(value + this.data.allowedFilesCount, '', {
          duration: 8000,
          panelClass: 'error'
        });
        snack;
      });
      this.draggingClass = "dragging-not-allowed"
      setTimeout(() => { context.draggingClass = "not-dragging" }, 3000);
      return false;
    }

    //-------------
    //Check if you're uploading a file size more than allowed maximun file size
    const foundFileLargerThanAllowed = _.findIndex(files, function (f) { return (f.size * 0.000001) > context.data.maxFileSizeMb; }) > -1; //Find a file with a size more than the allowed
    if (foundFileLargerThanAllowed) {
      context.translateService.get('upload.fileSizeBig').pipe(take(1)).subscribe(value => {
        const snack = this.snackBar.open(_.replace(value, '{$1}', context.data.maxFileSizeMb), '', {
          duration: 8000,
          panelClass: 'error'
        });
        snack;
      });
      this.draggingClass = "dragging-not-allowed"
      setTimeout(() => { context.draggingClass = "not-dragging" }, 3000);
      return false;
    }

    //-------------
    //Validate whether file mime type is allowed
    let showFileNotAllowedWarning = false;
    let removedFiles = 0;

    for (let index = 0; index < files.length; index++) {
      let file = files[index];

      if (file) {
        if (!GalenFCheckIfFileIsAllowed(file.type, this.data.allowAnyFile)) {
          showFileNotAllowedWarning = true;
          file = null; // remove this file
          removedFiles++;
          this.draggingClass = "dragging-not-allowed"
          setTimeout(() => { context.draggingClass = "not-dragging" }, 3000);
        }
      }
    };
    if (showFileNotAllowedWarning) {
      context.translateService.get('upload.fileTypeNotAllowed').pipe(take(1)).subscribe(value => {
        const snack = this.snackBar.open(value, '', {
          duration: 10000,
          panelClass: 'warning'
        });
        snack;
      });
    }

    //If removed files are equal than the files to be uploaded, then don't proceed, stop here. Otherwise, just proceed with the upload but without these files
    if (removedFiles == files.length)
      return false;
    else
      return true;
  }

  //===========================
  //Get the file to upload image preview (Only for images)
  previewImage = (file) => {
    if (!file)
      return;

    var mimeType = file.type;
    if (mimeType.match(/image\/*/) !== null) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        file.imageCode = reader.result;
      }
    }
  };

  //===========================
  //Get the total upload percent
  getTotalPercent = () => {
    this.globalPercent = 0;
    if (this.files) {
      this.files.forEach(async file => {
        if (file && file.uploadPercent >= 0) {
          this.globalPercent += file.uploadPercent;

          //After every uploaded file, get the current quota...
          // if (file.uploadPercent == 100) {
          // }
        }
      });
    }
    this.globalPercent = this.globalPercent / this.files.length;

    //close the dialog when the upload is finished
    if (this.globalPercent === 100) {
        this.finalized = true;
        //Second timer to display a finished message
        setTimeout(() => {
          this.dialogRef.close(this.data.files);
        }, 2000);
    }
  };

  //============================
  //Check current quota

  getCurrentQuota = () => {

    //Get fresh result from the server, and update the state, but don't wait for it to finish, it was uploaded before when the system loaded.
    this.miscService.getCustomerUsedAndAdditionalQuotas();

    //subscribe to the state changes to update values
    this.currentQuotaSubscription = this.store.select(fromRoot.getGlobalState).subscribe(globalState => {
      if(globalState.quotas){
        this.usedSpace = globalState.quotas.usedQuota ? globalState.quotas.usedQuota.storageSizeMb : 0;
        this.additionalSpaceMb = globalState.quotas.additionalStorageMb ?? 0;
      }
    });

  };

  //===========================
  //Cancel the upload for specific file
  cancelUpload = (file) => {
    if (file) {
      if (file.task$) {
        file.task$.cancel();
        file.uploadPercent = 100;
        file.isCancelled = true;
        this.getTotalPercent();
      }
    }
  };

  //===========================
  //Cancel the upload for all the pending files
  cancellAllUploads = () => {
    if (this.files) {
      this.files.forEach(file => {
        if (file) {

          if (file.task$) {
            file.task$.cancel();
          }

          if (file.uploadPercentSubscription)
            file.uploadPercentSubscription.unsubscribe();

        }
      });
    }
  }

  //===========================
  getSpaceUsedPercent(): number {
    let percent = Math.round((this.usedSpace * 100) / (this.totalSpace + this.additionalSpaceMb));
    return percent > 100 ? 100 : percent;
  }
  getAvailableSpaceMb(): number {
    return this.totalSpace - this.usedSpace + this.additionalSpaceMb;
  }

  //===========================
  //Unsubscribe subscriptions and cancel upload tasks...
  ngOnDestroy() {
    this.cancellAllUploads();

    if(this.currentQuotaSubscription)
      this.currentQuotaSubscription.unsubscribe();
  }

}

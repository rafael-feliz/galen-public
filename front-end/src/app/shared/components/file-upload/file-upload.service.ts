/* =============================================================================
  Rafael Féliz

  //File upload service

 ============================================================================= */

import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { GalenIFile, FC_CUSTOMERSDATA, FC_CUSTOMERSDATA_USERS, GalenICustomerUser, GalenIFileUploaded } from 'galen-shared';
import { take } from 'rxjs/operators';
import { ErrorService } from 'src/app/services/errors/error.service';
import { TranslateService } from '@ngx-translate/core'
import { MiscService } from 'src/app/services/misc/misc.service';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(private miscService: MiscService,
    private afFirestore: AngularFirestore,
    private errorService: ErrorService,
    private translateService: TranslateService) { }



  /**
   * UPDATES FIRESTORE REFERENCE AFTER FILE UPLOADED
   * This will update the firestore document if is updating (uploading only one file), or it will insert a new item to the given collection in path
   * @param firestorePath
   * @param firestorePropertyName
   * @param files
   * @returns
   */
  updateFirestoreFilesReferences = (multipleFiles:boolean, firestorePath: string, firestorePropertyName: string, files: Array<GalenIFileUploaded>): Promise<void> => {

    const context = this;

    this.miscService.setSyncingFirestoreLoader();

    return new Promise(resolve => {
      this.miscService.getCustomerId().pipe(take(1)).subscribe(async customerId => {

        //--------------------
        //Check if firestorePath start with /
        if (firestorePath.charAt(0) !== "/") {
          firestorePath = "/" + firestorePath; //insert the / because it's required to start with /
        }

        //--------------------
        //update firestore

        if (multipleFiles) {
          //If more than one file, an insert will be performed to a collection.
          //The expected path should be a collection.
          const collectionRef = this.afFirestore.collection(`${FC_CUSTOMERSDATA}/${customerId}${firestorePath}`);

          await files.forEach(async fileObj => {

            let file: GalenIFile = { ...fileObj.file };

            file.name = fileObj.name;
            file.type = fileObj.type;
            file.sizeMb = fileObj.sizeMb;
            //Get the file public url and set to the file object
            file.publicUrlOriginalFilePath = await this.miscService.getFileUrl(file.originalFilePath);

          //if is an image, it will contain imageLarge and others properties
            if (file.imageLarge) {
              file.publicUrlImageLarge = await this.miscService.getFileUrl(file.imageLarge);
              file.publicUrlImageMed = await this.miscService.getFileUrl(file.imageMed);
              file.publicUrlImageSmall = await this.miscService.getFileUrl(file.imageSmall);
            }
            //----
            //Value to be set. Use a property to assign the file if the firestorePropertyName is set. If not set, set the file object directly on the object.
            let value = {};
            if (firestorePropertyName !== "")
              value[firestorePropertyName] = { ...file };
            else
              value = { ...file };

            //----
            //Update firestore
            collectionRef.doc().set(value).catch(function (error) {
              context.translateService.get('upload.updateFirestoreError').pipe(take(1)).subscribe(text => {
                context.errorService.showCustomErrorMessages(error, text);
              });
            });

            //Continue to next file

          }); //end for

        } else {
          //If just one file, use an update, and if property doesn't exixst, firebase will insert it.
          //The expected path should be a document
          const documentRef = this.afFirestore.doc(`${FC_CUSTOMERSDATA}/${customerId}${firestorePath}`);

          //get the only file
          let file: GalenIFile = files[0].file;

          file.name = files[0].name;
          file.type = files[0].type;
          file.sizeMb = files[0].sizeMb;

          //Get the file public url and set to the file object
          file.publicUrlOriginalFilePath = await this.miscService.getFileUrl(file.originalFilePath);

          //if is an image, it will contain imageLarge and others properties
          if (file.imageLarge) {
            file.publicUrlImageLarge = await this.miscService.getFileUrl(file.imageLarge);
            file.publicUrlImageMed = await this.miscService.getFileUrl(file.imageMed);
            file.publicUrlImageSmall = await this.miscService.getFileUrl(file.imageSmall);
          }

          //----
          //Value to be set. Use a property to assign the file if the firestorePropertyName is set. If not set, set the file object directly on the object.
          let value = {};
          if (firestorePropertyName !== "")
            value[firestorePropertyName] = { ...file };
          else
            value = { ...file };

          //----
          //Update firestore
          documentRef.update(value).catch(function (error) {
            context.translateService.get('upload.updateFirestoreError').pipe(take(1)).subscribe(text => {
              context.errorService.showCustomErrorMessages(error, text);
            });
          });

        }
        resolve();
      });
    })
  }
}

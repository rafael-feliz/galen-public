import { AngularFireUploadTask } from '@angular/fire/storage';
import { GalenAdminIBillingPlans_ForPublicUsage, GalenIFileUploaded } from 'galen-shared';
import { Observable } from 'rxjs';


//The modal required params
export interface FileUploadDialogContentParamsModel {
  customerId:string,
  billingPlanDetails:GalenAdminIBillingPlans_ForPublicUsage,
  firebaseStoragePath:string,
  recordId?:string,
  allowedFilesCount:number,
  allowAnyFile:boolean,
  maxFileSizeMb:number,
  files: Array<GalenIFileUploaded>
}


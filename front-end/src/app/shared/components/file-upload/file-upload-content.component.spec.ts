import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileUploadContentComponent } from './file-upload-content.component';

describe('FileUploadContentComponent', () => {
  let component: FileUploadContentComponent;
  let fixture: ComponentFixture<FileUploadContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileUploadContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/*  ******************************************************************************************************
    Rafael Féliz

    Displays a loading indicator above all the content with a white background.
    CSS is located on the scss/genera/loader.scss file, due it is also used by the index.html file

    Usage: <loader-full-screen [loading]="loading"></loader-full-screen>


    PARAMS:
    see below

    ******************************************************************************************************
*/

import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'loader-full-screen',
  templateUrl: './loader-full-screen.component.html'
})
export class LoaderFullScreenComponent implements OnChanges {

  @Input("loading") loading: boolean;
  @Input("showG") showG: boolean = false;
  @Input("showSpinner") showSpinner: boolean = true;
  @Input("spinnerBlue") spinnerBlue: boolean = true;
  @Input("fastTransition") fastTransition: boolean = true;
  @Input("relative") relative: boolean = true;  //If true it requires the parent element to have a position:relative css property, otherwise it will show it as fullscreen in fixed position.

  show = -1;
  timer1;
  timer2;

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {

    if(!changes.loading)
    return;

    // ---------------------
    // Set loading current value

    let loadingCurrentValue = false;
    if (changes.loading) {
      loadingCurrentValue = changes.loading.currentValue;
    } else {
      loadingCurrentValue = false;

    }

    // ---------------------
    // show / hide loading indicator

    if (loadingCurrentValue) {
      this.show = 1; //show the loader

      if(this.timer1) clearTimeout(this.timer1);
      if(this.timer2) clearTimeout(this.timer2);

    } else {
      if (this.show >= 0) {
        if(this.timer1) clearTimeout(this.timer1);
        if(this.timer2) clearTimeout(this.timer2);

        this.timer1 = setTimeout(() => {
          this.show = 0;//animate fade out
        }, this.fastTransition ? 50 : 1000);

        this.timer2 = setTimeout(() => {
          this.show = -1; //remove from dom
        }, this.fastTransition ? 550 : 2000);

      }
    }

  }

}

import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { GICON_HELP } from 'src/app/constants/font-awesome.constants';

@Component({
  selector: 'help-icon[langRefString]',
  templateUrl: './help-icon.component.html',
  styleUrls: ['./help-icon.component.scss']
})
export class HelpIconComponent implements OnInit {

  GICON_HELP = GICON_HELP;

  @Input("langRefString") langRefString:string = "";

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { merge, fromEvent, Observable, Observer } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { GICON_FA_WIFISLASH } from '../../../constants/font-awesome.constants';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../store/app.reducer';
import * as globalActions from '../../../store/global.actions';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-offline-notification',
  templateUrl: './offline-notification.component.html',
  styleUrls: ['./offline-notification.component.scss']
})
export class OfflineNotificationComponent implements OnInit {

  GICON_FA_WIFISLASH = GICON_FA_WIFISLASH;
  show = false;

  constructor(private store: Store<fromRoot.State>,
              private snackBar: MatSnackBar,
              private translateService: TranslateService) { }


  ngOnInit(): void {
    this.createOnline$().subscribe(isOnline => {

      if(isOnline){
        //Change the state
        this.store.dispatch(new globalActions.Online());
        this.show = false;
      }
      else{
        setTimeout(() => {
          //Set the value ater a delay to wait the snackbar to appear
          this.store.select(fromRoot.getIsOffline).pipe(take(1)).subscribe(value =>{
            this.show = value;
          });
        },4000);

        //Change the state
        this.store.dispatch(new globalActions.Offline());

        this.translateService.get('general.offlineIconMessage').pipe(take(1)).subscribe(value =>{
          const snack = this.snackBar.open(value,'', {
                      duration: 6000,
                      panelClass: 'warning'
                    });
                    snack
                      .onAction()
                      .subscribe(() => {
                          window.location.reload(true)
                      });
        });
      }
    });
  }

  createOnline$() {
    return merge<boolean>(
      fromEvent(window, 'offline').pipe(map(() => false)),
      fromEvent(window, 'online').pipe(map(() => true)),
      new Observable((sub: Observer<boolean>) => {
        sub.next(navigator.onLine);
        sub.complete();
      }));
  }
}

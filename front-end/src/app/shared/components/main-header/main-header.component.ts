import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { GICON_CHAT, GICON_ADD, GICON_SEARCH } from 'src/app/constants/font-awesome.constants';

@Component({
  selector: 'main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainHeaderComponent implements OnInit {

  GICON_CHAT = GICON_CHAT;
  GICON_SEARCH = GICON_SEARCH;
  GICON_ADD = GICON_ADD;

  constructor() { }

  ngOnInit(): void {
  }

}

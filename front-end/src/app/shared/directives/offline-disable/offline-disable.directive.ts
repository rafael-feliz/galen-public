/* =============================================================================
  Rafael Féliz

  To disable an element when working offline (No internet).
  Mostly used for buttons (matButton)

  Adds an "offline" class to the element so that you can customize if needed

  ============================================================================= */

import { AfterViewInit, Directive, ElementRef, Renderer2 } from '@angular/core';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core'
import * as fromRoot from '../../../store/app.reducer';

@Directive({
  selector: '[offline-disable]'
})
export class OfflineDisableDirective implements AfterViewInit {

  constructor(private el: ElementRef,
              private renderer: Renderer2,
              private store: Store<fromRoot.State>,
              private translateService: TranslateService) {}

ngAfterViewInit() {

  //wait a little to apply
  setTimeout(() => {
    this.store.select(fromRoot.getIsOffline).subscribe(isOffline => {
      if(isOffline){

        //load the offline title message language str
        this.translateService.get('general.offlineDisableMessage').pipe(take(1)).subscribe(offlineMessage => {
          this.renderer.setAttribute(this.el.nativeElement, "disabled", "true");
          this.renderer.addClass(this.el.nativeElement, "offline");
          this.renderer.setAttribute(this.el.nativeElement, "title", offlineMessage);
          this.renderer.setAttribute(this.el.nativeElement, "ng-reflect-disabled", "true");  //disable matbutton
        });

      } else {
        this.renderer.removeAttribute(this.el.nativeElement, "disabled");
        this.renderer.removeClass(this.el.nativeElement, "offline");
        this.renderer.removeAttribute(this.el.nativeElement, "title");
        this.renderer.removeAttribute(this.el.nativeElement, "ng-reflect-disabled");
      }
    });
  }, 500);
}

}

/*
  Set a spinner inside an angular material button when the attribute is true.

  Attributes:
    loading <boolean>

  Usage:
  <button mat-flat-button color="primary" [loading]="loading">Login</button>

  */

import { Directive, ElementRef, Renderer2, AfterViewInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[loading]'
})
export class ButtonLoadingIndicatorDirective implements AfterViewInit, OnChanges  {

  @Input('loading') setLoading: boolean;

  constructor(private _elRef: ElementRef, private _renderer: Renderer2) {
    this.setLoading = false;
  }

  ngOnChanges(changes: SimpleChanges){
     if(changes.setLoading){
      this.setLoading = changes.setLoading.currentValue;
      this.setStatus();
    }
  }

  ngAfterViewInit(){
    this.setStatus();
   }

   setStatus() {
     if(this.setLoading){
      this._renderer.setAttribute(this._elRef.nativeElement,'disabled','');
      this._renderer.addClass(this._elRef.nativeElement, 'spinner');
      this._renderer.addClass(this._elRef.nativeElement, 'white');
     } else {
      this._renderer.removeAttribute(this._elRef.nativeElement,'disabled','');
      this._renderer.removeClass(this._elRef.nativeElement, 'spinner');
      this._renderer.removeClass(this._elRef.nativeElement, 'white');
     }

   }
}


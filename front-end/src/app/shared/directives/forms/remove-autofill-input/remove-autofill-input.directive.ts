
// Auto adds an attribute to all the input fields in order to disable browser autofill

import { Directive, ElementRef, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: 'input'
})
export class RemoveAutofillInputDirective {

  constructor (private _elRef: ElementRef, private _renderer: Renderer2) {
    _renderer.setAttribute(this._elRef.nativeElement, 'autocomplete', 'off');
  }

}

/*  ================================================================

    Rafael Feliz

    Converts an unix date to customer time zone date formated on his custom format.
    Also 2 arguments are available to display date and/or display time.

    Usage without params:                             {{ notification.dateUnix | customerDate | async}}.
    Usage with params (DisplayDate, DisplayTime):     {{ notification.dateUnix | customerDate: true : true | async}}

    ================================================================
*/

import * as _ from 'lodash';
import { Pipe, PipeTransform } from '@angular/core';
import { MiscService } from 'src/app/services/misc/misc.service';
import { ErrorService } from 'src/app/services/errors/error.service';

@Pipe({
  name: 'customerDate'
})
export class CustomerDatePipe implements PipeTransform {

  constructor(private miscService:MiscService, private errorService: ErrorService){}

  async transform(value: number, includeDate?:boolean, includeTime?:boolean ):Promise<string> {
    const unixTime = value;
    const context = this;

    if(!_.isNumber(unixTime))
      return new Promise<string>(resolve => { resolve("Unix Time (number) is required");});
    else
      return await this.miscService.getCurrentDateInCustomerFormat(unixTime, "", includeDate, includeTime).then(value => { return value;}).catch(error => {
        context.errorService.showCustomErrorMessages(error);
        return error;
      });
  }

}

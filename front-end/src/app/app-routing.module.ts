import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainGuard } from './guards/main.guard'
import { ErrorPagesComponent } from './pages/error-pages/error-pages.component';
import { PATH_NOT_FOUND } from './constants/routing-paths.constants';



const routes: Routes = [
  {path: PATH_NOT_FOUND, component: ErrorPagesComponent},
  {path: '**', redirectTo: '/' + PATH_NOT_FOUND},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [MainGuard]
})
export class AppRoutingModule { }

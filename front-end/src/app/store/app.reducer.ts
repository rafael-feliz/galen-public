/*
*****************
GLOBAL STORE

Here you will import every other store from the system, so that you access the store from a sigle way (this one, the root store)

To use the store:

import * as fromRoot from './store/app.reducer';

  constructor(private store: Store<fromRoot.State>

READ:
---------
    this.store.select(fromRoot.getIsAuth);
    See that the fromRoot.getIsAuth is defined here at the end. Define one for every case as you need.

    OTHER WAY TO READ:
    ---------------------
      this.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(val => console.log(val));

UPDATE:
---------
import * as globalActions from '../../store/global.actions';

  this.store.dispatch(new authActions.LogIn({email: user.email, isAuthenticated: true, deviceId: null}));

*/

import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store'
import * as fromAuth from '../auth/store/auth.reducer';
import * as fromGlobal from './global.reducer';

//Global state structure. Note: the other states will be lazy loaded into this state, because there will be a STATE
// on the reducer which implements this state, and the ngrx will merge the state for us if you configure the states
//on the other modules with the "forFeature" instead than "forRoot".
export interface State {
  auth: fromAuth.State,
  global: fromGlobal.State
}

//To import on the app.root or any other module
export const reducers: ActionReducerMap<State> = {
  auth: fromAuth.authReducer,
  global: fromGlobal.globalReducer
};

//=================
//Export the states to be able to import from outside

export const getAuthState = createFeatureSelector<fromAuth.State>('auth');
export const getGlobalState = createFeatureSelector<fromGlobal.State>('global');

//===================
//GLOBAL STATE
export const getIsOffline = createSelector(getGlobalState, fromGlobal.getIsOffline);
export const getIsRouteChanging = createSelector(getGlobalState, fromGlobal.getIsRouteChanging);
export const getBrowserLanguage = createSelector(getGlobalState, fromGlobal.getBrowserLanguage);
export const getShowFullScreenLoader = createSelector(getGlobalState, fromGlobal.getShowFullScreenLoader);
export const GetQuotas = createSelector(getGlobalState, fromGlobal.getQuotas);
export const getBrowserIsInFullScreen = createSelector(getGlobalState, fromGlobal.getBrowserIsInFullScreen);

//===================
//AUTH STATE
export const getIsAuth = createSelector(getAuthState, fromAuth.getIsAuthenticated);
export const getLanguage = createSelector(getAuthState, fromAuth.getIsAuthenticated);
export const getEmail = createSelector(getAuthState, fromAuth.getEmail);
export const getCustomerId = createSelector(getAuthState, fromAuth.getCustomerId);


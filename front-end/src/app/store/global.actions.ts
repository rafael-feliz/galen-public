import { Action } from '@ngrx/store';
import { GalenAdminIBillingUsedQuotas, GalenTlanguageTypes } from 'galen-shared';
import { State } from './global.reducer';

//==========================================
//Export types
export type GlobalActions = Online | Offline | RouteIsChanging | RouteWasChanged | BrowserLanguage | ShowFullScreenLoader |
ShowSyncingFirestoreLoader | SetQuotas | SetIsFullScreen | SetBrowserInstallButtonEvent;

//==========================================
//ACTIONS

// export const LOG_IN = "[Auth] Log in";
// export class LogIn implements Action { readonly type = LOG_IN; constructor(public payload: State){} }

//------------
export const ONLINE = "[Global] Online";
export const OFFLINE = "[Global] Offline";
export class Online implements Action { readonly type = ONLINE; constructor(){} }
export class Offline implements Action { readonly type = OFFLINE; }

//------------
export const ROUTE_IS_CHANGING = "[Global] RouteIsChanging";
export const ROUTE_WAS_CHANGED = "[Global] RouteWasChanged";
export class RouteIsChanging implements Action { readonly type = ROUTE_IS_CHANGING; constructor(){} }
export class RouteWasChanged implements Action { readonly type = ROUTE_WAS_CHANGED; }


//------------
export const SHOW_FULLSCREEN_LOADER = "[Global] ShowFullScreenLoader";
export class ShowFullScreenLoader implements Action { readonly type = SHOW_FULLSCREEN_LOADER; constructor(public payload: boolean){} }

export const SHOW_SYNCING_FIRESTORE_LOADER = "[Global] SetSyncingFirestoreLoader";
export class ShowSyncingFirestoreLoader implements Action { readonly type = SHOW_SYNCING_FIRESTORE_LOADER; constructor(public payload: boolean){} }

//------------
export const BROWSER_LANGUAGE = "[Global] browserLanguage";
export class BrowserLanguage implements Action { readonly type = BROWSER_LANGUAGE; constructor(public payload: GalenTlanguageTypes){} }


//------------
export const SET_QUOTAS = "[Global] SetQuotas";
export class SetQuotas implements Action { readonly type = SET_QUOTAS; constructor(public payload: GalenAdminIBillingUsedQuotas){} }

//------------
export const SET_ISFULLSCREEN = "[Global] SetIsFullScreen";
export class SetIsFullScreen implements Action { readonly type = SET_ISFULLSCREEN; constructor(public payload: boolean){} }

//------------
export const SET_BROWSER_INSTALL_BUTTON_EVENT = "[Global] SetBrowserInstallButtonEvent";
export class SetBrowserInstallButtonEvent implements Action { readonly type = SET_BROWSER_INSTALL_BUTTON_EVENT; constructor(public payload: any){} }



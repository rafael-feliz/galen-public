//STORE For global and generic information (Not related to specific component).
//There is an AUTH store for authentication related information.

import { GalenAdminIBillingUsedQuotas, GalenTlanguageTypes } from 'galen-shared';
import { GlobalActions, ONLINE, OFFLINE, ROUTE_IS_CHANGING, ROUTE_WAS_CHANGED, BROWSER_LANGUAGE, SHOW_FULLSCREEN_LOADER, SET_QUOTAS, SET_ISFULLSCREEN, SET_BROWSER_INSTALL_BUTTON_EVENT, SHOW_SYNCING_FIRESTORE_LOADER } from './global.actions';

//-----------------
//The state structure
export interface State {
  showFullScreenLoader: boolean,      //If true, the full screen loading indicator will be displayed. This is located in the app.component.ts
  showSyncingFirestoreLoader: boolean,      //It will display the syncing loader when it is true. An automated process on the loader-firestore-syncing component will set it back to false.
  isOffline: boolean,                 //Indicate whether there is internet connection or not
  isRouteChanging: boolean            //Indicates whether a route is begin changed to another
  browserLanguage: GalenTlanguageTypes,                 //Indicate whether there is internet connection or not
  quotas?: GalenAdminIBillingUsedQuotas,
  browserInFullScreen?: boolean,
  browserInstallAppEnabledEvent?: any         //here is stored an event that will handle the trigger of the
}

//-----------------
//The initial state
const initialState: State = {
  showFullScreenLoader:true,
  showSyncingFirestoreLoader: false,
  isOffline: false,
  isRouteChanging: false,
  browserLanguage: 'en',
  browserInFullScreen: false
};

//-----------------
//ACTIONS
export function globalReducer(state = initialState, action: GlobalActions){
  switch(action.type){
    case ONLINE:  return {  ...state, isOffline: false };
    case OFFLINE: return {  ...state, isOffline: true };
    case ROUTE_IS_CHANGING:  return {  ...state, isRouteChanging: true };
    case ROUTE_WAS_CHANGED: return {  ...state, isRouteChanging: false };
    case BROWSER_LANGUAGE: return {  ...state, browserLanguage: action.payload };
    case SHOW_FULLSCREEN_LOADER: return {  ...state, showFullScreenLoader: action.payload };
    case SHOW_SYNCING_FIRESTORE_LOADER: return {  ...state, showSyncingFirestoreLoader: action.payload };
    case SET_QUOTAS: return {  ...state, quotas: action.payload };
    case SET_ISFULLSCREEN: return {  ...state, browserInFullScreen: action.payload };
    case SET_BROWSER_INSTALL_BUTTON_EVENT: return {  ...state, browserInstallAppEnabledEvent: action.payload };
    default:
      return state;
  }
}

//Quick access exports to use in app.reducer
export const getIsOffline = (state:State) => state.isOffline;
export const getIsRouteChanging = (state:State) => state.isRouteChanging;
export const getBrowserLanguage = (state:State) => state.browserLanguage;
export const getShowFullScreenLoader = (state:State) => state.showFullScreenLoader;
export const getQuotas = (state:State) => state.quotas;
export const getBrowserIsInFullScreen = (state:State) => state.browserInFullScreen;

import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import * as fromRoot from '../store/app.reducer';
import { Store } from '@ngrx/store';
import * as globalActions from '../store/global.actions';
import 'dayjs/locale/en';
import 'dayjs/locale/es';
import * as dayjs from 'dayjs';
import { GalenTlanguageTypes } from 'galen-shared';

@NgModule({
  imports: [
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  exports: [TranslateModule]
})
export class I18nModule {
  constructor(translate: TranslateService,
              private store: Store<fromRoot.State>) {

    translate.addLangs(['en', 'es']); //If you add more language here, then also import them above

    //Load language from the browser
    let browserLang:GalenTlanguageTypes = translate.getBrowserLang() as GalenTlanguageTypes;
    const userBrowserLang:GalenTlanguageTypes = browserLang.match(/en|es/) ? browserLang : 'en';

    //Save the browser language on the store. NOTE: This is not the language that is used to display system info. It is used when the user
    //info is not available, like on login pages, etc, to know in which language to send emails, etc.
    this.store.dispatch(new globalActions.BrowserLanguage(userBrowserLang));

    //subscribe to the language changes from the state
    this.store.select(fromRoot.getAuthState).subscribe(authState => {
      let languageToUse:GalenTlanguageTypes = userBrowserLang;

      if(authState.isAuthenticated){
        //Use the language from the user setting on database. If it is not set, it will use the browser language
        if(authState.language)
        languageToUse = authState.language;
      }
      translate.use(languageToUse);

      //Set DayJS default language
      dayjs.locale(languageToUse) // use locale

    });
  }
}

export function translateLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

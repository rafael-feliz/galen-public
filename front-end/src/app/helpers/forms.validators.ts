import { FormGroup } from '@angular/forms';
import { GalenFPasswordStrongResult } from 'galen-shared';

/* custom validator to check that two fields match
  Usage:
  , {
      validator: MustMatch('password', 'confirmPassword')
    }
*/
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}



/* custom validator to check if mat-select has value.
A Mat-select with no selection should have -1 as value

Usage: , {
            validator: MatSelectHasValue('selectedFactor')
          }
*/
export function MatSelectHasValue(controlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    // set error on matchingControl if validation fails
    if (control.value == -1) {
      control.setErrors({ required: true });
    } else {
      control.setErrors(null);
    }
  }
}


/**
 *  Validate a phone number. Usage:
 *
 *
            <mat-error *ngIf="twoStepForm.controls.phone.dirty && twoStepForm.controls.phone.hasError('required')">
              {{'general.phoneRequired' | translate}}</mat-error>
            <mat-error
              *ngIf="twoStepForm.controls.phone.dirty && twoStepForm.controls.phone.hasError('invalid')">
              {{'general.phoneInvalid' | translate}}</mat-error>
          </mat-form-field>


 *
 *  this.twoStepForm = this.formBuilder.group({
      phone: ['', [Validators.required]]
    }, {
      validator: PhoneNumberIsValid('phone')
    });

 *  */

export function PhoneNumberIsValid(controlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];

    const phoneRegex = new RegExp("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$");

    if (control.value === "") {
      control.setErrors(null);
      return;
    }

    // set error on matchingControl if validation fails
    if (!phoneRegex.test(control.value)) {
      control.setErrors({ invalid: true });
    } else if (control.value.length < 10 || control.value.length > 12) {
      control.setErrors({ invalid: true });
    } else {
      control.setErrors(null);
    }
  }
}



// Validate the password security
export function PasswordIsValid(controlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const result = GalenFPasswordStrongResult(control.value).isSecure;

    // set error on matchingControl if validation fails
    if (!result) {
      control.setErrors({ invalid: true });
    } else {
      control.setErrors(null);
    }
  }
}

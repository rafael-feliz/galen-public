import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderFullScreenComponent } from './shared/components/loader-full-screen/loader-full-screen.component';
import { MainNavigationComponent } from './shared/components/main-navigation/main-navigation.component';
import { OfflineNotificationComponent } from './shared/components/offline-notification/offline-notification.component';
import { AvatarComponent } from './shared/components/avatar/avatar.component';
import { FileUploadComponent } from './shared/components/file-upload/file-upload.component';
import { FileUploadContentComponent } from './shared/components/file-upload/file-upload-content.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RemoveAutofillInputDirective } from './shared/directives/forms/remove-autofill-input/remove-autofill-input.directive';
import { ButtonLoadingIndicatorDirective } from './shared/directives/loading-indicators/button-loading-indicator.directive';
import { InlineNotificationComponent } from './shared/components/inline-notification/inline-notification.component';
import { NoteTipComponent } from './shared/components/note-tip/note-tip.component';
import { OfflineDisableDirective } from './shared/directives/offline-disable/offline-disable.directive';
import { I18nModule } from './i18n/i18n.module';
import { PasswordSecurityMetterBarComponent } from './shared/components/password-security-metter-bar/password-security-metter-bar.component';
import { ConfirmDialogComponent } from './shared/components/confirm-dialog/confirm-dialog.component';
import { MobileModeMenuComponent } from './shared/components/main-navigation/mobile-mode-menu.component';
import { MainHeaderComponent } from './shared/components/main-header/main-header.component';
import { MainHeaderClinicSelectorComponent } from './shared/components/main-header-clinic-selector/main-header-clinic-selector.component';
import { MainHeaderNotificationsComponent } from './shared/components/main-header-notifications/main-header-notifications.component';
import { CustomerDatePipe } from './shared/pipes/customer-date.pipe';
import { NothingFoundComponent } from './shared/components/nothing-found/nothing-found.component';
import { ServiceWorkerInstallButtonComponent } from './shared/components/service-worker-install-button/service-worker-install-button.component';
import { RouterModule } from '@angular/router';
import { IconSearchBarComponent } from './shared/components/icon-search-bar/icon-search-bar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ContextMenuComponent } from './shared/components/context-menu/context-menu.component';
import { LoaderFirestoreSyncingComponent } from './shared/components/loader-firestore-syncing/loader-firestore-syncing.component';
import { HelpIconComponent } from './shared/components/help-icon/help-icon.component';


@NgModule({
  declarations: [
    LoaderFullScreenComponent,
    MainNavigationComponent,
    OfflineNotificationComponent,
    FileUploadComponent,
    FileUploadContentComponent,
    AvatarComponent,
    RemoveAutofillInputDirective,
    ButtonLoadingIndicatorDirective,
    InlineNotificationComponent,
    NoteTipComponent,
    OfflineDisableDirective,
    PasswordSecurityMetterBarComponent,
    ConfirmDialogComponent,
    MobileModeMenuComponent,
    MainHeaderComponent,
    MainHeaderClinicSelectorComponent,
    MainHeaderNotificationsComponent,
    CustomerDatePipe,
    NothingFoundComponent,
    ServiceWorkerInstallButtonComponent,
    IconSearchBarComponent,
    ContextMenuComponent,
    LoaderFirestoreSyncingComponent,
    HelpIconComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    MaterialModule,
    FlexLayoutModule,
    I18nModule,
    RouterModule,
    ReactiveFormsModule
  ],
  exports: [
    LoaderFullScreenComponent,
    MainNavigationComponent,
    OfflineNotificationComponent,
    FileUploadComponent,
    FileUploadContentComponent,
    AvatarComponent,
    RemoveAutofillInputDirective,
    ButtonLoadingIndicatorDirective,
    InlineNotificationComponent,
    NoteTipComponent,
    OfflineDisableDirective,
    PasswordSecurityMetterBarComponent,
    MainHeaderComponent,
    CustomerDatePipe,
    NothingFoundComponent,
    ServiceWorkerInstallButtonComponent,
    IconSearchBarComponent,
    ContextMenuComponent,
    LoaderFirestoreSyncingComponent,
    HelpIconComponent
  ]
})
export class SharedModule { }

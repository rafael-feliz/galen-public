import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { MiscService } from 'src/app/services/misc/misc.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {

  constructor(private miscService: MiscService) { }

  ngOnInit(): void {

    //Set the generic page title
    this.miscService.setPageTitle("");
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { GALENC_SCREEN_SIZES } from 'galen-shared';
import { GICON_PROFILE, GICON_USERS, GICON_SECURITY, GICON_CLINIC, GICON_ACCOUNT_BILLING, GICON_PAPER, GICON_FA_ARROW_LEFT, GICON_SETTINGS, GICON_EMAIL_QUEUE, GICON_FA_HOME, GICON_MENUBARS, GICON_X } from 'src/app/constants/font-awesome.constants';
import { PATH_ACCOUNT, PATH_ACCOUNT_PROFILE, PATH_ACCOUNT_SECURITY_ROLES, PATH_ACCOUNT_USERS, PATH_ACCOUNT_CLINICS, PATH_ACCOUNT_BILLING, PATH_ACCOUNT_EMAILSQUEUE, PATH_ACCOUNT_HOME } from 'src/app/constants/routing-paths.constants';
import { MiscService } from 'src/app/services/misc/misc.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  constructor() { }


  gIcon_emailQueue = GICON_EMAIL_QUEUE;
  GICON_FA_HOME = GICON_FA_HOME;
  GICON_SETTINGS = GICON_SETTINGS;
  GICON_PROFILE = GICON_PROFILE;
  GICON_USERS = GICON_USERS;
  GICON_SECURITY = GICON_SECURITY;
  GICON_CLINIC = GICON_CLINIC;
  gIcon_accountBilling = GICON_ACCOUNT_BILLING;
  GICON_PAPER = GICON_PAPER;
  screenSizes = GALENC_SCREEN_SIZES;
  GICON_FA_arrowLeft = GICON_FA_ARROW_LEFT;
  GICON_MENUBARS = GICON_MENUBARS;
  GICON_X = GICON_X;

  PATH_ACCOUNT = PATH_ACCOUNT;
  PATH_ACCOUNT_HOME = PATH_ACCOUNT_HOME;
  PATH_ACCOUNT_PROFILE = PATH_ACCOUNT_PROFILE;
  PATH_ACCOUNT_SECURITY_ROLES = PATH_ACCOUNT_SECURITY_ROLES;
  PATH_ACCOUNT_USERS = PATH_ACCOUNT_USERS;
  PATH_ACCOUNT_CLINICS = PATH_ACCOUNT_CLINICS;
  PATH_ACCOUNT_BILLING = PATH_ACCOUNT_BILLING;
  PATH_ACCOUNT_EMAILSQUEUE = PATH_ACCOUNT_EMAILSQUEUE;

  showingMenu = false;

  ngOnInit(): void {
  }

  toggleMenu = () => {
    this.showingMenu = !this.showingMenu;
  }
}

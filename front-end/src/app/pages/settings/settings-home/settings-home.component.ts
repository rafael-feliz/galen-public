import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { GICON_CLINIC, GICON_SECURITY, GICON_ACCOUNT_BILLING, GICON_USERS } from 'src/app/constants/font-awesome.constants';
import { PATH_ACCOUNT, PATH_ACCOUNT_SECURITY_ROLES, PATH_ACCOUNT_USERS, PATH_ACCOUNT_CLINICS, PATH_ACCOUNT_BILLING } from 'src/app/constants/routing-paths.constants';
import { MiscService } from 'src/app/services/misc/misc.service';
import * as fromRoot from '../../../store/app.reducer';

@Component({
  selector: 'app-settings-home',
  templateUrl: './settings-home.component.html',
  styleUrls: ['./settings-home.component.scss']
})
export class SettingsHomeComponent implements OnInit {

  userPhoto;
  userFirstName;
  userFullName;
  GICON_USERS = GICON_USERS;
  GICON_CLINIC = GICON_CLINIC;
  GICON_SECURITY = GICON_SECURITY;
  GICON_ACCOUNT_BILLING = GICON_ACCOUNT_BILLING;

  PATH_ACCOUNT = PATH_ACCOUNT;
  PATH_ACCOUNT_SECURITY_ROLES = PATH_ACCOUNT_SECURITY_ROLES;
  PATH_ACCOUNT_USERS = PATH_ACCOUNT_USERS;
  PATH_ACCOUNT_CLINICS = PATH_ACCOUNT_CLINICS;
  PATH_ACCOUNT_BILLING = PATH_ACCOUNT_BILLING;

  constructor(private store: Store<fromRoot.State>,
    private miscService: MiscService) { }

  ngOnInit() {

    this.miscService.setPageTitle("general.accountAndSettings");


    this.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(authState => {
      if (authState) {
        this.userFirstName = authState.firstName;
        this.userFullName = authState.fullName;

        if (authState.photo) {
          this.userPhoto = authState.photo;
        }
      }
    });
  }
}

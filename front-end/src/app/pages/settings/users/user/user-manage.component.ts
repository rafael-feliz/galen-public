import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GALENC_DATE_FORMATS, GALENC_TIME_FORMATS, GALENC_LANGUAGES, GalenICustomerUser, GalenISecurityRoles, GALENC_ADMIN_ROLEID, GalenICustomerDoctorSpecialties, GalenICustomerClinic } from 'galen-shared';
import { take } from 'rxjs/operators';
import { GICON_BACK } from 'src/app/constants/font-awesome.constants';
import { PATH_ACCOUNT, PATH_ACCOUNT_USERS } from 'src/app/constants/routing-paths.constants';
import { MiscService } from 'src/app/services/misc/misc.service';
import { UserService } from 'src/app/services/user/user.service';
import { SecurityService } from 'src/app/services/security/security.service';
import { TranslateService } from '@ngx-translate/core';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { PhoneNumberIsValid } from 'src/app/helpers/forms.validators';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as _ from 'lodash';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: ['./user-manage.component.scss']
})
export class UserManageComponent implements OnInit, OnDestroy {

  PATH_ACCOUNT = PATH_ACCOUNT;
  PATH_ACCOUNT_USERS = PATH_ACCOUNT_USERS;
  GICON_BACK = GICON_BACK;
  userForm: FormGroup;
  userEmail = "";
  dateFormats = GALENC_DATE_FORMATS;
  timeFormats = GALENC_TIME_FORMATS;
  languagesList = GALENC_LANGUAGES;
  user: GalenICustomerUser = {};
  subscribeToRolesCollectionSubscription;
  roles: Array<GalenISecurityRoles> = [];
  subscribeToSpecialtiesCollectionSubscription;
  specialties: Array<GalenICustomerDoctorSpecialties> = [];
  subscribeToClinicssCollectionSubscription;
  clinics: Array<GalenICustomerClinic> = [];
  disableSameUserFields = false;
  accessToClinicsIds: Array<string> = ['main-clinsic'];
  saving = false;
  loggedInUserEmail = "";

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private miscService: MiscService,
    private userService: UserService,
    private customerService: CustomerService,
    private translateService: TranslateService,
    private securityService: SecurityService,
    private snackBar: MatSnackBar) { }

  ngOnInit(): void {


    //Create the user formBuilder
    this.userForm = this.formBuilder.group({
      _email: ['', [Validators.required, Validators.email]],
      _roleId: ['', Validators.required],
      fullName: ['', Validators.required],
      language: [null],
      dateFormatId: [null],
      timeFormatId: [null],
      phone: [''],
      phoneExtension: [''],
      phoneExtensionName: [''],
      specialtyId: null,
      nonDoctor: [false],
      isDisabled: [false],
      accessToAllClinicsCheckbox: [true],
      accessToClinicsIds: []
    }, {
      validator: PhoneNumberIsValid('phone')
    });

    this.userForm.controls.accessToAllClinicsCheckbox.setValue(true);


    //Read the route Params
    this.route.params.pipe(take(1)).subscribe(routeParams => {
      this.userEmail = routeParams.id ? routeParams.id : '';

      this.miscService.setPageTitle(this.userEmail === "" ? "user.addUser" : "user.editUser");



      this.miscService.getCustomerId().pipe(take(1)).subscribe(async customerId => {

        //Check if the loaded user is the same that is logged in
        await this.miscService.getUserEmail().pipe(take(1)).subscribe(loggedInUserEmail => {
          this.loggedInUserEmail = loggedInUserEmail;
        });

        if (this.userEmail != "") {
          //--------------------------------
          //GET USER INFORMATION ONCE
          this.userService.subscribeToUserDocumentChanges(customerId, this.userEmail).pipe(take(1)).subscribe(userInfoRef => {

            this.user = userInfoRef.payload.data();

            if (this.user) {
              this.userForm.controls._email.disable();

              if (this.user._email === this.loggedInUserEmail)
                this.disableSameUserFields = true;

              //set the user to the form
              this.userForm.patchValue({
                _email: this.user._email,
                _roleId: this.user._roleId,
                fullName: this.user.fullName,
                language: this.user.language,
                dateFormatId: this.user.dateFormatId,
                timeFormatId: this.user.timeFormatId,
                phone: this.user.phone ? this.user.phone.phone : '',
                phoneExtension: this.user.phone ? this.user.phone.extension : '',
                phoneExtensionName: this.user.phone ? this.user.phone.extensionName : '',
                specialtyId: this.user.specialtyId,
                nonDoctor: this.user.nonDoctor,
                isDisabled: this.user.isDisabled,
                accessToAllClinicsCheckbox: this.user.accessToClinicsIds ? this.user.accessToClinicsIds.length === 0 : true, //Just information to show/hide clinics list
                accessToClinicsIds: this.user.accessToClinicsIds ? this.user.accessToClinicsIds : [],
              });

            }
          });
        }
        //--------------------------------
        //GET SECURITY ROLES

        this.subscribeToRolesCollectionSubscription = this.securityService.subscribeToRolesCollection(customerId).subscribe(rolesRef => {
          this.roles = [];

          //First, insert the administrator role at the beginning
          this.translateService.get("admin-role").pipe(take(1)).subscribe(administratorString => {
            this.roles.push({
              name: administratorString,
              roleId: GALENC_ADMIN_ROLEID,
              roleActionsPermissions: []
            })

            //Insert the user defined roles
            if (rolesRef) {
              rolesRef.forEach(role => {
                this.roles.push(role);
              });
            }
          });
        });

        //--------------------------------
        //GET DOCTOR SPECIALTIES

        this.subscribeToSpecialtiesCollectionSubscription = this.userService.subscribeToDoctorSpecialtiesCollection(customerId).subscribe(specialtiesRef => {
          this.specialties = [];
          if (specialtiesRef) {
            specialtiesRef.forEach(specialty => {
              this.specialties.push({
                id: specialty.payload.doc.id,
                ...specialty.payload.doc.data()
              });
            });
          }
        });

        //--------------------------------
        //GET CLINICS

        this.subscribeToClinicssCollectionSubscription = this.customerService.subscribeToClinicCollectionsChanges(customerId).subscribe(clinicsRef => {
          this.clinics = [];
          if (clinicsRef) {
            clinicsRef.forEach(clinic => {
              this.clinics.push(clinic.payload.doc.data());
            });
          }
        });
      });
    });
  }

  //--------------------------------

  ngOnDestroy() {
    if (this.subscribeToRolesCollectionSubscription)
      this.subscribeToRolesCollectionSubscription.unsubscribe();

    if (this.subscribeToSpecialtiesCollectionSubscription)
      this.subscribeToSpecialtiesCollectionSubscription.unsubscribe();

    if (this.subscribeToClinicssCollectionSubscription)
      this.subscribeToClinicssCollectionSubscription.unsubscribe();
  }


  //--------------------------------

  onSubmitUserInfo() {

    // stop here if form is invalid
    if (this.userForm.invalid) {
      return;
    }

    if (!environment.production) {
      if (this.userForm.controls.email.value.indexOf("@galen.app") == -1) {
        const snack = this.snackBar.open("You're on a dev environment and you should test with @galen emails", '', {
          duration: 4000,
          panelClass: 'warning'
        });
        return;
      }
    }

    this.saving = true;

    this.miscService.setSyncingFirestoreLoader();

    var roleName = _.filter(this.roles, (r: GalenISecurityRoles) => { return r.roleId === this.userForm.controls._roleId.value }).map((r: GalenISecurityRoles) => r.name)[0];
    var specialtyName = _.filter(this.specialties, (s: GalenICustomerDoctorSpecialties) => { return s.id === this.userForm.controls.specialtyId.value }).map((s: GalenICustomerDoctorSpecialties) => s.name)[0];

    var userToSave: GalenICustomerUser = {
      ...this.user,
      _email: this.userForm.controls._email.value,
      _roleId: this.userForm.controls._roleId.value,
      _roleName: roleName ? roleName : null,
      accessToClinicsIds: this.userForm.controls.accessToAllClinicsCheckbox.value ? [] : this.userForm.controls.accessToClinicsIds.value,
      isDisabled: this.userForm.controls.isDisabled.value ? this.userForm.controls.isDisabled.value : false,
      fullName: this.userForm.controls.fullName.value,
      nonDoctor: this.userForm.controls.nonDoctor.value,
      language: this.userForm.controls.language.value ? this.userForm.controls.language.value : null,
      dateFormatId: this.userForm.controls.dateFormatId.value ? this.userForm.controls.dateFormatId.value : null,
      timeFormatId: this.userForm.controls.timeFormatId.value ? this.userForm.controls.timeFormatId.value : null,
      phone: {
        phone: this.userForm.controls.phone.value ?? null,
        extension: this.userForm.controls.phoneExtension.value ?? null,
        extensionName: this.userForm.controls.phoneExtensionName.value ?? null
      },
      specialtyName: specialtyName ? specialtyName : null,
      _createdByUserEmail: this.loggedInUserEmail
    };

    //Add or edit the database
    this.userService.addEdit(userToSave).then(result => {
      if (!result) {
        this.translateService.get('general.genericError').pipe(take(1)).subscribe(value => {
          const snack = this.snackBar.open(value, '', {
            duration: 4000,
            panelClass: 'error'
          });
        });
      }
    });

    setTimeout(() => {
      this.router.navigate(['/' + PATH_ACCOUNT + '/' + PATH_ACCOUNT_USERS + '/view/' + this.userForm.controls._email.value]);
    }, 1000);


  }

}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FC_CUSTOMERSDATA_USERS, GALENC_DATE_FORMATS, GALENC_TIME_FORMATS, GalenFDateConvert24hToAmPm, GalenFDateConvertAmPmTo24h, GalenICustomerClinic, GalenICustomerUser, GalenIFileUploaded, GalenTfileLocations } from 'galen-shared';
import { GICON_EDIT, GICON_BACK, GICON_PASSWORD_KEY, GICON_LOCK, GICON_DELETE, GICON_NEXT, GICON_UNLOCK } from 'src/app/constants/font-awesome.constants';
import { UserService } from 'src/app/services/user/user.service';
import * as _ from 'lodash';
import { take } from 'rxjs/operators';
import { MiscService } from 'src/app/services/misc/misc.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core'
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent, IConfirmDialog } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { AuthActionsService } from 'src/app/auth/auth-actions/auth-actions.service';
import { PATH_ACCOUNT, PATH_ACCOUNT_USERS } from 'src/app/constants/routing-paths.constants';
import { CustomerService } from 'src/app/services/customer/customer.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {

  GICON_BACK = GICON_BACK;
  GICON_NEXT = GICON_NEXT;
  GICON_EDIT = GICON_EDIT;
  GICON_PASSWORD_KEY = GICON_PASSWORD_KEY;
  GICON_LOCK = GICON_LOCK;
  GICON_UNLOCK = GICON_UNLOCK;
  GICON_DELETE = GICON_DELETE;
  GalenFDateConvert24hToAmPm = GalenFDateConvert24hToAmPm;
  GalenFDateConvertAmPmTo24h = GalenFDateConvertAmPmTo24h;
  sendingResetLink = false;
  disablingUser = false;
  disableSameUserFields = false;
  profilePictureFirestorePath = "";
  PATH_ACCOUNT = PATH_ACCOUNT;
  PATH_ACCOUNT_USERS = PATH_ACCOUNT_USERS;

  userProfilePhotoType: GalenTfileLocations = "users-profile-photos";
  user: GalenICustomerUser = {};
  secondFactorAuthMethods = [];
  getUserSubscription;
  accessToClinicsNames = [];
  clinics:Array<GalenICustomerClinic> = [];

  constructor(private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private miscService: MiscService,
    private customerService: CustomerService,
    private snackBar: MatSnackBar,
    private translateService: TranslateService,
    private dialog: MatDialog,
    private authActionsService: AuthActionsService) { }

  ngOnInit(): void {

    this.miscService.setPageTitle("general.user");

    //-------------------
    //Load route params
    this.route.params.subscribe(routeParams => {
      if (routeParams.id) {

        //Subscribe to user changes
        this.miscService.getCustomerId().pipe(take(1)).subscribe(customerId => {
          this.getUserSubscription = this.userService.subscribeToUserDocumentChanges(customerId, routeParams.id).subscribe(userInfoRef => {
            this.user = userInfoRef.payload.data();

            if(this.user) {
              this.miscService.setPageTitle(this.user.fullName);
              this.profilePictureFirestorePath = `/${FC_CUSTOMERSDATA_USERS}/${this.user._email}`;

              //Check if the loaded user is the same that is logged in
              this.miscService.getUserEmail().pipe(take(1)).subscribe(loggedInUserEmail => {
                if (this.user._email === loggedInUserEmail)
                  this.disableSameUserFields = true;
              });

              //Get the clincs array
              this.customerService.subscribeToClinicCollectionsChanges(customerId).pipe(take(1)).subscribe(snapshot => {
                if(snapshot) {
                  this.clinics = [];
                  snapshot.forEach(doc => {
                    if (doc.payload.doc.data())
                        this.clinics.push(doc.payload.doc.data());
                  });
                }
              })
            }
          });
        })

      }
    })

  }

  ngOnDestroy(): void {
    if (this.getUserSubscription)
      this.getUserSubscription.unsubscribe();
  }

  getTimeFormatName = (id: number) => {
    const format = _.find(GALENC_TIME_FORMATS, function (o) { return o.id == id; });
    if (format)
      return format.name;
    else
      return "-";
  }
  getDateFormatName = (id: number) => {
    const format = _.find(GALENC_DATE_FORMATS, function (o) { return o.id == id; });
    if (format)
      return format.name;
    else
      return "-";
  }

  //Filter the clinics list depending on the clinics the user have access to
  getAccessToClinicsList = (ids:Array<string>) => {
    return _.filter(this.clinics, function(c) { return ids.indexOf(c.clinicId) > -1; }).map(c => c.clinicName);
  }


  //=====================================
  //Reset password
  confirmResetPasswordDialog(): void {
    const dialogData: IConfirmDialog = {
      titleLangRef: "user.resetPasswordTitle",
      messageLangRef: "user.resetPasswordMessage"
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { data: dialogData });
    dialogRef.afterClosed().subscribe(async dialogResult => {

      if (dialogResult) {
        this.sendingResetLink = true;

        await this.authActionsService.sendRecoveryLink(this.user._email).then(result => {
          this.sendingResetLink = false;
          this.translateService.get('user.resetPasswordSuccess').pipe(take(1)).subscribe(text => {
            const snack = this.snackBar.open(text, '', {
              duration: 3000,
              panelClass: 'success'
            });
          });

        }).catch(function (error) {
          this.sendingResetLink = false;
          this.translateService.get('user.resetPasswordError').pipe(take(1)).subscribe(text => {
            const snack = this.snackBar.open(_.replace(text, "{$1}", error.message), '', {
              duration: 6000,
              panelClass: 'error'
            });
          });
        });
      }

    });
  }

  //=====================================
  /**
   * The confirm dialog to delete a user
   */
  confirmDeleteDialog(userName: string, userEmail: string): void {
    const dialogData: IConfirmDialog = {
      titleLangRef: "users.confirmDeleteDialogTitle",
      messageLangRef: "users.confirmDeleteDialogSubtitle",
      isWarning: true
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { data: dialogData });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.translateService.get('general.deleteSuccess').pipe(take(1)).subscribe(text => {
          const snack = this.snackBar.open(userName + text, '', {
            duration: 3000,
            panelClass: 'success'
          });
        });
        this.userService.delete(userEmail);
        this.router.navigate([`${PATH_ACCOUNT}/${PATH_ACCOUNT_USERS}`]);
      }
    });
  }

  //=====================================
  //Disable
  toggleEnableDisable(): void {
    this.disablingUser = true;
    this.miscService.setSyncingFirestoreLoader();
    //Update the database
    this.userService.enableDisable(this.user._email, this.user.isDisabled).then(result => {
      this.disablingUser = false;
      if (!result) {
        this.translateService.get('general.genericError').pipe(take(1)).subscribe(value => {
          const snack = this.snackBar.open(value, '', {
            duration: 4000,
            panelClass: 'error'
          });
        });
      }
    });
  }


}

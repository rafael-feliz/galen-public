import { Component, OnInit } from '@angular/core';
import { GICON_ADD, GICON_DELETE, GICON_DOTS_OPTIONS, GICON_EDIT, GICON_SEARCH, GICON_VIEW } from 'src/app/constants/font-awesome.constants';
import * as fromRoot from '../../../store/app.reducer';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { GalenAdminIBillingPlans_ForPublicUsage, GalenAdminIBillingUsedQuotas, GalenIContextMenuParams, GalenICustomerUser } from 'galen-shared';
import * as _ from 'lodash';
import { MiscService } from 'src/app/services/misc/misc.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent, IConfirmDialog } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { PATH_ACCOUNT, PATH_ACCOUNT_USERS } from 'src/app/constants/routing-paths.constants';
import { UserService } from '../../../services/user/user.service';
import { TranslateService } from '@ngx-translate/core'
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  GICON_DOTS_OPTIONS = GICON_DOTS_OPTIONS;
  GICON_ADD = GICON_ADD;
  GICON_SEARCH = GICON_SEARCH;
  GICON_EDIT = GICON_EDIT;
  GICON_DELETE = GICON_DELETE;

  PATH_ACCOUNT = PATH_ACCOUNT;
  PATH_ACCOUNT_USERS = PATH_ACCOUNT_USERS;

  quotas:GalenAdminIBillingUsedQuotas = {
    additionalUsers: 0,
    usedQuota: {
      users: 0
    }
  };
  billingPlanDetails: GalenAdminIBillingPlans_ForPublicUsage = {
    quotas: {
      users: 0
    }
  }

  constructor(private store: Store<fromRoot.State>,
    private miscService: MiscService,
    private dialog: MatDialog,
    private ngZone: NgZone,
    private router: Router,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private translateService: TranslateService) { }

  usersSubscription;
  customerId;
  groupedUsers: Array<{ group: string, items: Array<GalenICustomerUser> }> = [];
  isLoading = true;
  loggedInUserEmail = "";
  loadedUsers;

  contextMenu: Array<GalenIContextMenuParams> = [
    { id: 1, actionName: "view", iconRef: GICON_VIEW, langRef: 'general.view' },
    { id: 2, actionName: "edit", iconRef: GICON_EDIT, langRef: 'general.edit' },
    { id: 3, isSeparator: true },
    { id: 4, actionName: "delete", iconRef: GICON_DELETE, langRef: 'general.delete' }
  ];

  ngOnInit(): void {

    this.miscService.setPageTitle("general.users");

    //Load users
    this.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(authState => {
      if (authState) {
        if (authState.customerInfo) {
          this.customerId = authState.customerInfo.customerId;
          /**Subscribe to the clinic collection list changes */
          this.usersSubscription = this.userService.subscribeToUsersCollection(this.customerId).subscribe(users => {
            this.isLoading = false;
            this.loadedUsers = users;
            this.groupedUsers = this.miscService.groupByProp(users, 'nonDoctor');
          });
        }
      }
    });

    //get current logged in user email
    this.miscService.getUserEmail().pipe(take(1)).subscribe(loggedInUserEmail => {
      this.loggedInUserEmail = loggedInUserEmail;
    });


    //Get used quotas
    this.miscService.getCustomerUsedAndAdditionalQuotas();

    this.store.select(fromRoot.getGlobalState).subscribe(globalState => {
      if (globalState) {
        if (globalState.quotas && typeof globalState.quotas !== 'undefined') {
            this.quotas = globalState.quotas;
        }
      }
    });

    //Get the current logged in user billing plan detail
    this.miscService.getCustomerBillingPlanDetails().then(billingPlanDetail => {
      if(billingPlanDetail)
        if(billingPlanDetail.quotas)
          this.billingPlanDetails = billingPlanDetail;
    });
  };

  /**
   * The confirm dialog to delete a user
   */
  confirmDeleteDialog(userName: string, userEmail:string): void {
    const dialogData: IConfirmDialog = {
      titleLangRef: "users.confirmDeleteDialogTitle",
      messageLangRef: "users.confirmDeleteDialogSubtitle",
      isWarning: true
    };
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { data: dialogData });
    dialogRef.afterClosed().subscribe(dialogResult => {
      if(dialogResult) {
        this.translateService.get('general.deleteSuccess').pipe(take(1)).subscribe(text => {
          const snack = this.snackBar.open(userName + text, '', {
            duration: 3000,
            panelClass: 'success'
          });
        });
        this.userService.delete(userEmail).then(result => {
           setTimeout(() => {
            this.miscService.getCustomerUsedAndAdditionalQuotas();
           }, 1000);
        });
      }
    });
  }

  onSearch(value: string) {
    console.log(value);
  }

  onClickUser(user): void {
    this.ngZone.run(() => this.router.navigate([`${PATH_ACCOUNT}/${PATH_ACCOUNT_USERS}/view/${user._email}`]));
  }

  onContextMenuClick(result): void {
    const action = result.action;
    const user = result.data as GalenICustomerUser;

    if (action === "delete") {
      this.confirmDeleteDialog(user.fullName, user._email);
    } else if (action === "view") {
      this.ngZone.run(() => this.router.navigate([`${PATH_ACCOUNT}/${PATH_ACCOUNT_USERS}/view/${user._email}`]));
    } else if (action === "edit") {
      this.ngZone.run(() => this.router.navigate([`${PATH_ACCOUNT}/${PATH_ACCOUNT_USERS}/edit/${user._email}`]));
    }
  }

  getContextMenu = (user: GalenICustomerUser): Array<GalenIContextMenuParams> => {
    //copy the context menu, to modify it below
    let contextMenu = [...this.contextMenu];

    //Don't allow to delete to the same user that is logged in
    if (user._email === this.loggedInUserEmail) {
      contextMenu.splice(_.findIndex(contextMenu, function (o) { return o.id === 4 }), 1);
      contextMenu.splice(_.findIndex(contextMenu, function (o) { return o.id === 4 }), 1);
    }
    return contextMenu;
  }

}

import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { PATH_LOGIN, PATH_NOT_ALLOWED } from 'src/app/constants/routing-paths.constants';
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-error-pages',
  templateUrl: './error-pages.component.html',
  styleUrls: ['./error-pages.component.scss']
})
export class ErrorPagesComponent implements OnInit {


  PATH_LOGIN = PATH_LOGIN
  errorType: 'not-found' | 'not-allowed' | '' = '';
  imageSrc = '';

  constructor(private titleService: Title,
    private translateService: TranslateService,
              private router:Router) { }


  ngOnInit(): void {

    if(this.router.url.endsWith(PATH_NOT_ALLOWED))
      this.errorType = 'not-allowed';
    else
      this.errorType = 'not-found';


     //Set the page title
     if(this.errorType === 'not-allowed'){
      this.translateService.get('errorPages.notAllowedTitle').pipe(take(1)).subscribe(value => this.titleService.setTitle(value));
      this.imageSrc = '/assets/img/misc/galen-asset_not-allowed.svg';
     }

    else {
      this.translateService.get('errorPages.resourceNotFoundTitle').pipe(take(1)).subscribe(value => this.titleService.setTitle(value));
      this.imageSrc = '/assets/img/misc/galen-asset_not-found.svg';
    }



  }

}

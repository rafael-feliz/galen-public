import {
  PATH_ACCOUNT,
  PATH_ACCOUNT_BILLING,
  PATH_ACCOUNT_CLINICS,
  PATH_ACCOUNT_EMAILSQUEUE,
  PATH_ACCOUNT_HOME,
  PATH_ACCOUNT_PROFILE,
  PATH_ACCOUNT_SECURITY_ROLES,
  PATH_ACCOUNT_USERS,
  PATH_DASHBOARD,
  PATH_NOT_ALLOWED
} from '../constants/routing-paths.constants';



// PAGES ROUTING MODULE ***********************************

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainGuard } from '../guards/main.guard';
import { SettingsComponent } from './settings/settings.component';
import { PersonalSettingsComponent } from './settings/personal-settings/personal-settings.component';
import { SecurityRolesComponent } from './settings/security-roles/security-roles.component';
import { EmailQueueComponent } from './settings/email-queue/email-queue.component';
import { MainComponent } from './main/main.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ErrorPagesComponent } from './error-pages/error-pages.component';
import { SettingsHomeComponent } from './settings/settings-home/settings-home.component';
import { UsersComponent } from './settings/users/users.component';
import { UserComponent } from './settings/users/user/user.component';
import { UserManageComponent } from './settings/users/user/user-manage.component';
import { ClinicsComponent } from './settings/clinics/clinics.component';
import { BillingComponent } from './settings/billing/billing.component';

//Routes have the MainComponent because we need to maintain the header in any module,
//and if we put it as component on every page, it will flicker when changing routes. This
//is why we have the main component and all the other as child.
const routes: Routes = [
  { path: PATH_NOT_ALLOWED, component: ErrorPagesComponent, canActivate: [MainGuard]},
  { path: '',  component: MainComponent, canActivate: [MainGuard], children: [
    { path: '', redirectTo: PATH_DASHBOARD, pathMatch: "full" },
    { path: PATH_DASHBOARD, component: DashboardComponent, canActivate: [MainGuard], pathMatch: "full"},
    { path: PATH_ACCOUNT, component: SettingsComponent, canActivate: [MainGuard], children: [
      { path: '', redirectTo: PATH_ACCOUNT_HOME, pathMatch: "full" },
      { path: PATH_ACCOUNT_HOME, component: SettingsHomeComponent, canActivate: [MainGuard], pathMatch: "full" },
      { path: PATH_ACCOUNT_PROFILE, component: PersonalSettingsComponent, canActivate: [MainGuard] },
      { path: PATH_ACCOUNT_SECURITY_ROLES, component: SecurityRolesComponent, canActivate: [MainGuard] },
      { path: PATH_ACCOUNT_EMAILSQUEUE, component: EmailQueueComponent, canActivate: [MainGuard] },
      { path: PATH_ACCOUNT_USERS, component: UsersComponent, canActivate: [MainGuard] },
      { path: PATH_ACCOUNT_USERS + '/add', component: UserManageComponent, canActivate: [MainGuard] },
      { path: PATH_ACCOUNT_USERS + '/edit/:id', component: UserManageComponent, canActivate: [MainGuard] },
      { path: PATH_ACCOUNT_USERS + '/view/:id', component: UserComponent, canActivate: [MainGuard] },
      { path: PATH_ACCOUNT_CLINICS, component: ClinicsComponent, canActivate: [MainGuard] },
      { path: PATH_ACCOUNT_BILLING, component: BillingComponent, canActivate: [MainGuard] },
    ]},
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }

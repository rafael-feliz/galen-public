import { Component, OnInit } from '@angular/core';
import { GICON_DASHBOARD } from 'src/app/constants/font-awesome.constants';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  GICON_DASHBOARD = GICON_DASHBOARD;

  constructor() { }

  ngOnInit() {
  }

}

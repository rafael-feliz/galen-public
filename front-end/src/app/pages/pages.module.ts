import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from '../pages/dashboard/dashboard.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { I18nModule } from '../i18n/i18n.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared.module';
import { PagesRoutingModule } from './pages-routing.module';
import { SettingsComponent } from './settings/settings.component';
import { PersonalSettingsComponent } from './settings/personal-settings/personal-settings.component';
import { EmailQueueComponent } from './settings/email-queue/email-queue.component';
import { SecurityRolesComponent } from './settings/security-roles/security-roles.component';
import { MainComponent } from './main/main.component';
import { ErrorPagesComponent } from '../pages/error-pages/error-pages.component';
import { SettingsHomeComponent } from '../pages/settings/settings-home/settings-home.component';
import { UsersComponent } from './settings/users/users.component';
import { ClinicsComponent } from './settings/clinics/clinics.component';
import { BillingComponent } from './settings/billing/billing.component';
import { UserComponent } from './settings/users/user/user.component';
import { UserManageComponent } from './settings/users/user/user-manage.component';


@NgModule({
  declarations: [
    DashboardComponent,
    SettingsComponent,
    PersonalSettingsComponent,
    EmailQueueComponent,
    SecurityRolesComponent,
    MainComponent,
    ErrorPagesComponent,
    SettingsHomeComponent,
    UsersComponent,
    ClinicsComponent,
    BillingComponent,
    UserComponent,
    UserManageComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    MaterialModule,
    FlexLayoutModule,
    I18nModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    PagesRoutingModule
  ]
})
export class PagesModule { }

//For emails
//This is repeated on the cloud functions
export const HTML_BUTTON = `
<table>
<tr>
<td align="center" bgcolor="#5FA8FF" class="inner-td"
style="border-radius:6px; font-size:16px; text-align:center; background-color:inherit;">
  <a style="background-color:#5FA8FF; border:1px solid #5FA8FF; border-color:#5FA8FF; border-radius:0px; border-width:1px; color:#ffffff; display:inline-block; font-family:verdana,geneva,sans-serif; font-size:16px; font-weight:normal; letter-spacing:1px; line-height:30px; padding:12px 20px 12px 20px; text-align:center; text-decoration:none; border-style:solid;"
  href="{{{link}}}" target="_blank">
  <span style="color: #FFFFFF">{{{text}}}</span>
  </a></td>
</tr>
</table>
`;

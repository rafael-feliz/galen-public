/*
  ************************************
  Rafael Féliz 2020
  Routing paths constants.

  Please add all the system paths here and don't use strings outside here.

  ************************************
*/

export const PATH_NOT_FOUND = 'not-found';
export const PATH_NOT_ALLOWED = 'not-allowed';
export const PATH_LOGIN = 'login';
export const PATH_AUTH_ACTIONS = 'auth-actions';
export const PATH_PASSWORD_RESET = 'password-reset';
export const PATH_REGISTER = 'register';

export const PATH_TWO_FACTOR_SETUP = 'enroll-two-factor-authentication';
export const PATH_CUSTOMER_SETUP = 'setup-account';

export const PATH_DASHBOARD = 'dashboard';

export const PATH_ACCOUNT = 'account';
export const PATH_ACCOUNT_HOME = 'home';
export const PATH_ACCOUNT_PROFILE = 'profile';
export const PATH_ACCOUNT_SECURITY_ROLES = 'security-roles';
export const PATH_ACCOUNT_USERS = 'users';
export const PATH_ACCOUNT_CLINICS = 'clinics';
export const PATH_ACCOUNT_BILLING = 'billing';
export const PATH_ACCOUNT_EMAILSQUEUE = 'emails-queue';

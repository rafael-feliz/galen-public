/*  ******************************************************************************************************
    Rafael Féliz

    Cloud functions names constants.
    The names that you should use here are these from the index.ts file under functions/src folder.

    ******************************************************************************************************
*/

export const CLOUD_FUNCTION_USED_QUOTAS = "a_uqpl";
export const CLOUD_FUNCTION_AUTH_REGISTER_USER_WELCOME_MESSAGE = "auth_registerUser_welcomeMessage";
export const CLOUD_FUNCTION_AUTH_ACTIONS = "auth_actions";
export const CLOUD_FUNCTION_AUTH_SET_PASSWORD_RESET_SUCCESS = "auth_setPasswordResetSuccess";
export const CLOUD_FUNCTION_GET_CHANGE_USER_PASSWORD_LINK = "userGetChangeUPLink";
export const CLOUD_FUNCTION_GET_CUSTOMER_INFO_FROM_USER = "gcifu";
export const CLOUD_FUNCTION_CREATE_CUSTOMER = "customer_create";
export const CLOUD_FUNCTION_FINISH_CREATE_CUSTOMER = "customer_finishCreate";
export const CLOUD_FUNCTION_USERNOTIFICATIONS_RESETCOUNT = "userNotifications_resetCount";
export const CLOUD_FUNCTION_USERGETSECONDFACTORS = "userGetSecondFactors";

/*
  ************************************
  Rafael Féliz 2020

  https://github.com/FortAwesome/angular-fontawesome/blob/HEAD/docs/usage.md

  Place here all the fontawesome icons references so that you only import from one place and have access to everything.
  Usage: <fa-icon [icon]="GICON_FA_ARROW_RIGHT"></fa-icon>

  The g on const name means Galen.

  ************************************
*/



import {
  faToggleOff,
  faToggleOn,
  faWifiSlash,
  faInfoSquare,
  faQuestionCircle,
  faCheck,
  faCloudUpload,
  faTimes,
  faTimesCircle,
  faTrash,
  faFile,
  faTooth,
  faUserMd,
  faSearch,
  faTachometerAltFast,
  faCalendarAlt,
  faUser,
  faUsers,
  faUsersCog,
  faSackDollar,
  faChartPie,
  faUserClock,
  faUserCircle,
  faPlus,
  faCog,
  faSignOut,
  faBars,
  faCommentAltSmile,
  faSpinner,
  faCommentAltLines,
  faBellOn,
  faAngleDown,
  faClinicMedical,
  faCircle,
  faCloudDownload,
  faExternalLink,
  faShieldCheck,
  faWallet,
  faChevronLeft,
  faChevronRight,
  faChevronUp,
  faChevronDown,
  faMailBulk,
  faHome,
  faEllipsisV,
  faEdit,
  faFileSearch,
  faArrowLeft,
  faKey,
  faLock,
  faLockOpen,
  faArrowRight,
  faArrowUp,
  faArrowDown

} from '@fortawesome/pro-light-svg-icons';

import {
  faCheckCircle as faSolidCheckCircle,
  faCircle as faSolidCircle

} from '@fortawesome/pro-solid-svg-icons';

//import { faSave } from '@fortawesome/pro-solid-svg-icons';

//====================================================
// Galen system specific features icons (GICON = Galen Icon)

export const GICON_DENTAL = faTooth;
export const GICON_MEDICAL = faUserMd;
export const GICON_SEARCH = faSearch;
export const GICON_ADD = faPlus;
export const GICON_EDIT = faEdit;
export const GICON_VIEW = faFileSearch;
export const GICON_DELETE = faTrash;
export const GICON_X = faTimes;
export const GICON_DASHBOARD = faTachometerAltFast;
export const GICON_AGENDA = faCalendarAlt;
export const GICON_PATIENTS = faUsers;
export const GICON_USERS = faUsersCog;
export const GICON_USER = faUser;
export const GICON_PROFILE = faUserCircle;
export const GICON_BILLING = faSackDollar;
export const GICON_REPORTS = faChartPie;
export const GICON_WAITINGLIST = faUserClock;
export const GICON_SETTINGS = faCog;
export const GICON_SIGNOUT = faSignOut;
export const GICON_MENUBARS = faBars;
export const GICON_NOTETIPICON = faCommentAltSmile;
export const GICON_CHAT = faCommentAltLines;
export const GICON_NOTIFICATION = faBellOn;
export const GICON_CLINIC = faClinicMedical;
export const GICON_SECURITY = faShieldCheck;
export const GICON_ACCOUNT_BILLING = faWallet;
export const GICON_PAPER = faFile;
export const GICON_EMAIL_QUEUE = faMailBulk;
export const GICON_DOTS_OPTIONS = faEllipsisV;
export const GICON_BACK = faArrowLeft;
export const GICON_NEXT = faArrowRight;
export const GICON_UP = faArrowUp;
export const GICON_DOWN = faArrowDown;
export const GICON_PASSWORD_KEY = faKey;
export const GICON_LOCK = faLock;
export const GICON_UNLOCK = faLockOpen;
export const GICON_HELP = faQuestionCircle;


//====================================================
// Generic Icons (GFA = Galen Font Awesome)

export const GICON_FA_TOGGLEON = faToggleOn;
export const GICON_FA_TOGGLEOFF = faToggleOff;
export const GICON_FA_WIFISLASH = faWifiSlash;
export const GICON_FA_INFOSQUARE = faInfoSquare;
export const GICON_FA_QUESTIONCIRCLE = faQuestionCircle; // HELP
export const GICON_FA_CHECK = faCheck;
export const GICON_FA_SOLIDCHECKCIRCLE = faSolidCheckCircle;
export const GICON_FA_CLOUDUPLOAD = faCloudUpload;
export const GICON_FA_TIMESCIRCLE = faTimesCircle;
export const GICON_FA_FILE = faFile;
export const GICON_FA_SPINNER = faSpinner;
export const GICON_FA_ANGLEDOWN = faAngleDown;
export const GICON_FA_SOLID_CIRCLE = faSolidCircle;
export const GICON_FA_CIRCLE = faCircle;
export const GICON_FA_CLOUD_DOWNLOAD = faCloudDownload;
export const GICON_FA_EXTERNAL_LINK = faExternalLink;

export const GICON_FA_ARROW_UP = faChevronUp;
export const GICON_FA_ARROW_DOWN = faChevronDown;
export const GICON_FA_ARROW_LEFT = faChevronLeft;
export const GICON_FA_ARROW_RIGHT = faChevronRight;
export const GICON_FA_HOME = faHome;






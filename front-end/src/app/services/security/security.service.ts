import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { TranslateService } from '@ngx-translate/core';
import { FC_CUSTOMERSDATA, FC_CUSTOMERSDATA_ROLES, GalenISecurityRoles } from 'galen-shared';
import { Observable } from 'rxjs';
import { ErrorService } from '../errors/error.service';
import { MiscService } from '../misc/misc.service';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {


  constructor(private afFirestore: AngularFirestore,
    private afFunctions: AngularFireFunctions,
    private miscService: MiscService,
    private errorService: ErrorService,
    private translateService: TranslateService) { }

  // ---------------------------------
  /**Subscribe to changes */
  subscribeToRolesCollection(customerId: string): Observable<GalenISecurityRoles[]> {
    //Subscribe to firestore document
    return this.afFirestore.collection<GalenISecurityRoles>(`${FC_CUSTOMERSDATA}/${customerId}/${FC_CUSTOMERSDATA_ROLES}`).valueChanges();
  }


}

/* =============================================================================
  Rafael Féliz

  General service for CUSTOMER CLINIC INFORMATION.
  It will contain things for:
  - The customer main clinic (Galen customer --> GalenICustomer)
  - The customer other branch clinics ---> GalenICustomerClinic

 ============================================================================= */

import { Injectable } from '@angular/core';
import { Action, AngularFirestore, DocumentChangeAction, DocumentSnapshot } from '@angular/fire/firestore';
import { FC_CUSTOMERSDATA, FC_CUSTOMERSDATA_CLINICS, FC_CUSTOMERSDATA_USERS,  GALENC_MAIN_CLINIC_COLLECTION_DOCUMENT_ID,  GalenICustomerClinic, GalenICustomerUser } from 'galen-shared';
import { Observable } from 'rxjs';
import { ErrorService } from '../errors/error.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private afFirestore: AngularFirestore, private errorService: ErrorService) { }

  // ---------------------------------
  /**Subscribe to clinic document changes */
  subscribeToClinicDocumentChanges(selectedClinicId:string, customerId:string): Observable<Action<DocumentSnapshot<GalenICustomerClinic>>> {
    //Subscribe to firestore document
    return this.afFirestore.doc<GalenICustomerClinic>(`${FC_CUSTOMERSDATA}/${customerId}/${FC_CUSTOMERSDATA_CLINICS}/${selectedClinicId}`).snapshotChanges();
  }

  // ---------------------------------
  /**Subscribe to clinic collections changes */
  subscribeToClinicCollectionsChanges(customerId:string): Observable<DocumentChangeAction<GalenICustomerClinic>[]> {
    //Subscribe to firestore document
    return this.afFirestore.collection<GalenICustomerClinic>(`${FC_CUSTOMERSDATA}/${customerId}/${FC_CUSTOMERSDATA_CLINICS}/`).snapshotChanges();
  }

  // ---------------------------------
  /** Assign the current selected clinic on the user document */
  setSelectedClinic(selectedClinicId:string, selectedClinicName:string, customerId:string, userId:string): Promise<boolean> {
    const context = this;
    return new Promise(async resolve => {

      //If clinic ID is not set, automatically set the default clinic ID
      if(!selectedClinicId || selectedClinicId === "") {
        selectedClinicId = GALENC_MAIN_CLINIC_COLLECTION_DOCUMENT_ID;
      }

      //Set the selected clinic ID
      await this.afFirestore.doc<GalenICustomerUser>(`${FC_CUSTOMERSDATA}/${customerId}/${FC_CUSTOMERSDATA_USERS}/${userId}`)
      .update({_settings_selectedClinicId: selectedClinicId, _settings_selectedClinicName: selectedClinicName})
      .then(() => { resolve(true);})
      .catch(error => {
        context.errorService.showCustomErrorMessages(error);
        resolve(false);});

    });

  }

}

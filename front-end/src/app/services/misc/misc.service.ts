/* =============================================================================
  Rafael Féliz

  General functions to share between project components etc.

  Update ANGULAR README on new functions

 ============================================================================= */

import { HttpClient } from '@angular/common/http';
import { HostListener, Injectable } from '@angular/core';
import { GalenAdminIBillingUsedQuotas, GalenAdminIBillingUsedQuotasParams, GalenFConvertNumberToCurrency, GalenFDateCustomerFormatLocal, GalenFDateConvertFromIsoOrUnixToLocal, GalenFDateUtc, GalenFDecrypt, GalenICurrentIpCountryInfo, GALENC_ENCRIPTION_GET_USED_QUOTAS, GalenFDateUtcUnix, GalenFDateConvertIsoToUnix, GALENC_SCREEN_SIZES, GalenISendEmailQueue, FC_EMAILQUEUE, GalenAdminIBillingPlans_ForPublicUsage } from 'galen-shared';
import { Observable } from 'rxjs';
import { AngularFireFunctions } from '@angular/fire/functions';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';
import { CLOUD_FUNCTION_USED_QUOTAS } from 'src/app/constants/cloud-functions.constants';
import * as fromRoot from '../../store/app.reducer';
import { AngularFireStorage } from '@angular/fire/storage';
import * as _ from 'lodash';
import * as globalActions from '../../store/global.actions';
import BrowserDtector, { BrowserInfoFull } from 'browser-dtector';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core'
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class MiscService {


  constructor(private http: HttpClient,
    private afFunctions: AngularFireFunctions,
    private afStorage: AngularFireStorage,
    private afFirestore: AngularFirestore,
    private store: Store<fromRoot.State>,
    private snackBar: MatSnackBar,
    private translateService: TranslateService,
    private pageTitle: Title) { }


  // ===========================================
  /**Gets the customer ID from the store */
  getCustomerId = (): Observable<string> => {
    //Get the customer ID from the store
    return this.store.select(fromRoot.getCustomerId)
  };

  // ===========================================
  /**Gets the user ID from the store */
  getUserEmail = (): Observable<string> => {
    //Get the customer ID from the store
    return this.store.select(fromRoot.getEmail);
  };

  // ===========================================
  /**Show the pushing data to the server loader indicator. It will be automatically removed when it's done, on the same loader-fixed component. */
  setSyncingFirestoreLoader = (): void => {
    //Get the customer ID from the store
    this.store.dispatch(new globalActions.ShowSyncingFirestoreLoader(true));
  };


  // ===========================================
  /**Get whether there is an internet connection or not */
  getIsOffline = async (): Promise<boolean> => {
    return new Promise(async resolve => {
      await this.store.select(fromRoot.getGlobalState).pipe(take(1)).subscribe(async globalState => {
        resolve(globalState.isOffline);
      });
    })
  };



  // ===========================================
  /** Get information from current IP */
  getCountryFromIp = (): Observable<GalenICurrentIpCountryInfo> => {
    //http://ip-api.com/json
    return this.http.get<GalenICurrentIpCountryInfo>('https://freegeoip.app/json/');
  };

  // ===========================================
  /** Gets the screen size string from the given param
   *
   * To get the current window size, use:
*      @HostListener('window:resize', ['$event'])
        onResize(event) {
          this.screenSize = this.miscService.getCurrentScreenSize(window.innerWidth);
        }
   *
  */
  getCurrentScreenSize = (screenWidth: number): string => {

    const screenSizes = GALENC_SCREEN_SIZES;

    //Get screen size. This is based on actual _variables.scss file.
    if (screenWidth > 2000)
      return screenSizes.xxxl;

    else if (screenWidth > 1500 && screenWidth <= 2000)
      return screenSizes.xxl;

    else if (screenWidth > 1279 && screenWidth <= 1500)
      return screenSizes.xl;

    else if (screenWidth > 1000 && screenWidth <= 1279)
      return screenSizes.lg;

    else if (screenWidth > 800 && screenWidth <= 1000)
      return screenSizes.md;

    else if (screenWidth > 500 && screenWidth <= 800)
      return screenSizes.sm;

    else if (screenWidth > 0 && screenWidth <= 500)
      return screenSizes.xs;

  };



  // ===========================================
  /**Get the current UTC DATE and return it as string (UTC TIME) (ISO FORMAT) */
  getCurrentDateUtc = (): string => {
    return GalenFDateUtc();
  };


  // ===========================================
  /**Get the current UTC DATE and return it as string (UTC TIME) (UNIX FORMAT) */
  getCurrentDateUtcUnix = (): number => {
    return GalenFDateUtcUnix();
  };

  // ===========================================
  /** Converts a  ISO FORMAT DATE to UNIX */
  getUnixDateFromIso = (isoDateToConvert: string): number => {
    return GalenFDateConvertIsoToUnix(isoDateToConvert);
  };


  // ===========================================
  /**Converts an utc date to USER DEVICE DATE on ISO FORMAT */
  getCurrentDateFromIsoOrUnixToLocal = (unixDate?: number, isoDate?: string): string => {
    return GalenFDateConvertFromIsoOrUnixToLocal(unixDate, isoDate);
  };

  // ===========================================
  /**Converts an utc date to CUSTOMER FORMAT DATE (NOT ISO)
   * You should use this function to display dates
  * @param unixDateToConvert The UNIX to format. Example: "2020-09-30T01:54:20.458Z". Use this or isoDateToConvert.
  * @param isoDateToConvert The UTC date (ISO FORMAT) to format. Example: "2020-09-30T01:54:20.458Z". Use this or unixDateToConvert.
  */

  getCurrentDateInCustomerFormat = (unixDateToConvert?: number, isoDateToConvert?: string, includeDate: boolean = true, includeTime: boolean = true): Promise<string> => {
    //Get the user's current store date and time format ID's
    return new Promise(resolve => {
      this.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(authState => {
        const dateFormatId = authState.dateFormatId;
        const timeFormatId = authState.timeFormatId;
        resolve(GalenFDateCustomerFormatLocal(unixDateToConvert, isoDateToConvert, dateFormatId, timeFormatId, includeDate, includeTime));
      })
    })
  };

  // ===========================================
  /**Convert a number to customer's currency
   * You should use this function to display money
   */
  convertNumberToCustomersCurrency = (amount: number): Promise<string> => {
    //Get the user's current store currency
    return new Promise(resolve => {
      this.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(authState => {
        const currencyCode = authState.customerInfo.currency;
        resolve(GalenFConvertNumberToCurrency(amount, currencyCode));
      })
    })
  };



  // ===========================================
  /** Gets the current used quota from the current user. It loads data from the customerAdminInfo collection, that is private.
   * This stores the result on the state (Global state)
   * A manual call is required to refresh quotas on the state.
   */
  getCustomerUsedAndAdditionalQuotas = (): Promise<void> => {

    return new Promise<void>((resolve) => {
      //First get the customer ID
      this.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(authState => {
        if (authState.customerInfo) {
          const customerId = authState.customerInfo.customerId;

          const miscData: GalenAdminIBillingUsedQuotasParams = {
            customerId: customerId
          }
          //Call the cloud function to get the private information
          this.afFunctions.httpsCallable(CLOUD_FUNCTION_USED_QUOTAS)(miscData).toPromise().then(response => {
            const result: GalenAdminIBillingUsedQuotas = { ...JSON.parse(GalenFDecrypt(response, GALENC_ENCRIPTION_GET_USED_QUOTAS)) };
            //Update the state and set the quotas information
            this.store.dispatch(new globalActions.SetQuotas(result));

            setTimeout(() => {
              resolve();
            }, 500);
          });
        }
      });
    });
  }

  // ===========================================
  /** Gets the customer actual billing plan detail
  */
  getCustomerBillingPlanDetailsSubscription;
  getCustomerBillingPlanDetails = (): Promise<GalenAdminIBillingPlans_ForPublicUsage> => {
    return new Promise(resolve => {

      this.getCustomerBillingPlanDetailsSubscription = this.store.select(fromRoot.getAuthState).subscribe(authState => {
        if (authState.billingPlanDetails) {
          if (this.getCustomerBillingPlanDetailsSubscription)
            this.getCustomerBillingPlanDetailsSubscription.unsubscribe();
          resolve(authState.billingPlanDetails);

        }
      })
    })
  };

  // ===========================================
  /**
   * Get tha real URL from firebase STORAGE BUCKET PATH ** OR ** Just get the firebase STORAGE BUCKET PATH with the given image size on it.
   * @param firebasePath The firebase path (DONT INCLUDE THE CUSTOMER ID) Example: /clinic-logo/106792732_308279130303802_1100390078691556356_n.jpg
   * @param imageSize 200, 400, 1200 or 0. If you want to get a smaller image. Don't set this param or set to 0 if you want the original file or non image file.
   * @param justReturnFileWithImageSize If true, you can use any URL, and it will just return the file path you've passed as param, with the resized image size.
   */
  getFileUrl = async (firebasePath: string, imageSize: number = 0, justReturnFileWithImageSize: Boolean = false): Promise<string> => {

    //Apply the image size to the path, to get smaller images
    if (typeof firebasePath !== 'undefined' && firebasePath) {
      if (imageSize > 0) {

        let size = "";

        if (imageSize == 200)
          size = "_200x200";
        else if (imageSize == 400)
          size = "_400x400";
        else if (imageSize == 1200)
          size = "_1200x1200";

        firebasePath = firebasePath.replace("_200x200", ""); //In case the path already have a size
        firebasePath = firebasePath.replace("_400x400", ""); //In case the path already have a size
        firebasePath = firebasePath.replace("_1200x1200", ""); //In case the path already have a size

        //add the new size to the end of the filename before the dot
        //firebasePath = firebasePath.replace(/(\.[\w\d_-]+)$/i, size + '$1');
        firebasePath = firebasePath.replace("." + firebasePath.split('.').pop(), size + "." + firebasePath.split('.').pop());

        if (justReturnFileWithImageSize)
          return firebasePath;
      }

      //First, get the customer ID from the state
      return new Promise(async resolve => {
        await this.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(async authState => {
          const customerId = authState.customerInfo.customerId;
          if (customerId !== "") {

            //Set the file storage reference path
            let ref = await this.afStorage.ref(customerId + '/' + firebasePath);

            let counter = 1;
            let getUrlSuccess = false;

            //Loop to try and try until getting the URL working. This is used so that when an image is uploaded and the avatar is going to load the thumbnails,
            //they might have not yet finished rendering, so no image would appear if I don't try again here.
            while (getUrlSuccess === false && counter < 5) {
              await this.store.select(fromRoot.getGlobalState).pipe(take(1)).subscribe(async globalState => {
                //Only request images while online
                if (!globalState.isOffline) {
                  await ref.getDownloadURL().pipe(take(1)).subscribe(async url => {
                    getUrlSuccess = true;
                    resolve(url);
                  });
                } else {
                  //Decrease counter to keep trying and basically cancell the counter++ from next lines...
                  counter--;
                }
              });
              //If testUrlPassed is still not set, wait some seconds to try again. If it is not set in the meantime, it will run the code inside the while again
              if (!getUrlSuccess) {
                await this.sleep(4000 * counter);
              }
              counter++;

            }

          } else {
            resolve("");
          }
        });
      });
    } else {
      return "";
    }

  };


  // =======================================
  /**
   * Group an array of objects by a property value
   * The group is basically the value of the propName param that is begin grouped on.
   * The items are the objects that belongs to a specific group
   */
  groupByProp(arr, propName): Array<{ group: any, items: Array<any> }> {
    const arrToReturn = arr.reduce((group, item) => {
      if (!group[item[propName]])
        group[item[propName]] = [];

      group[item[propName]].push(item);

      return group;
    }, []);
    //Now convert the return value to array of { group: ..., items: [...]}
    let modifiedObject: Array<{ group: any, items: Array<any> }> = [];
    for (let key in arrToReturn) { modifiedObject.push({ group: key, items: arrToReturn[key] }); };
    return modifiedObject;
  }

  // =======================================
  /**
   * Get the current user browser
   */
  getBrowserInfo(): BrowserInfoFull {
    //https://github.com/sibiraj-s/browser-dtector#api
    const browser = new BrowserDtector(window.navigator.userAgent);
    return browser.parseUserAgent();
  }


  // =======================================
  /**
   * Sends an email
   */
  sendEmail(data: GalenISendEmailQueue): void {
    this.afFirestore.collection<GalenISendEmailQueue>(FC_EMAILQUEUE).add(data).then(() => { });
  }



  // ===========================================
  showSlowInternetConnectionSnackBar = () => {

    let reloadStr = '';
    this.translateService.get('general.reload').pipe(take(1)).subscribe(value => reloadStr = value);
    this.translateService.get('general.slowInternet').pipe(take(1)).subscribe(slowInternetStr => {
      const snack = this.snackBar.open(slowInternetStr, reloadStr, {
        duration: 30000,
        panelClass: 'info'
      });
      snack
        .onAction()
        .subscribe(() => {
          window.location.reload();
        });

    });
  };

  // ===========================================
  /**
   * Set the page title
   * @param languageStringRef The lang string reference or custom string. You can leave it empty to only set the generic page title
   */
  setPageTitle = (languageStringRef: string) => {
    this.translateService.get("general.appTitle").pipe(take(1)).subscribe(generalAppTitle => {
      if (languageStringRef !== "") {
        this.translateService.get(languageStringRef).pipe(take(1)).subscribe(desiredAppTitle => {
          this.pageTitle.setTitle(desiredAppTitle + " | " + generalAppTitle);
        });
      } else {
        this.pageTitle.setTitle(generalAppTitle);
      }
    });
  }


  // ===========================================
  /**
   * To sleep in Javascript. Usage:  await this.miscService.sleep(2000);
   * @param ms milliseconds
   */
  sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
  }


}

import { Injectable } from '@angular/core';
import { Howl } from 'howler';

//howler.js
//https://github.com/goldfire/howler.js#documentation

@Injectable({
  providedIn: 'root'
})
export class SoundsService {

  constructor() { }

  play(type?:number){
    let src;
    if(type==2){
      src = 'assets/sounds/notifications/piece-of-cake.ogg';
    }
    else if(type==3){
      src = 'assets/sounds/notifications/pristine.ogg';
    }
    else{
      src = 'assets/sounds/notifications/juntos.ogg';
    }
    var sound = new Howl({
      src: [src],
      autoplay: true,
      volume: 0.3
    });
  }
}

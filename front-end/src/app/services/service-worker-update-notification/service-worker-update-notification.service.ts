//=========================================
//Service worker update notification
//This will trigger a snackbar when the offline support (Service worker) detects an update.
//This is called on the app.module

import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core'
import { take } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ServiceWorkerUpdateNotificationService {
  constructor(private swUpdate: SwUpdate,
    private _snackBar: MatSnackBar,
    private translateService: TranslateService) {

    let reloadStr = '';
    let message = '';

    this.translateService.get('general.reload').pipe(take(1)).subscribe(value => reloadStr = value);
    this.translateService.get('general.systemUpdate').pipe(take(1)).subscribe(value => {
      message = value;

      this.swUpdate.available.subscribe(evt => {
        const snack = this._snackBar.open(message, reloadStr, {
          duration: 60000,
          panelClass: 'warning'
        });
        snack
          .onAction()
          .subscribe(() => {
            window.location.reload(true)
          });

      });
    });

    //Check for update every 15 minutes
    setInterval(() => {
      if(this.swUpdate.isEnabled)
        this.swUpdate.checkForUpdate();
    }, 60000 * 15)

  }
}

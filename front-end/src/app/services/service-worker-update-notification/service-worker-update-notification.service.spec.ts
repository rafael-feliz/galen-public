import { TestBed } from '@angular/core/testing';

import { ServiceWorkerUpdateNotificationService } from './service-worker-update-notification.service';

describe('ServiceWorkerUpdateNotificationService', () => {
  let service: ServiceWorkerUpdateNotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceWorkerUpdateNotificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

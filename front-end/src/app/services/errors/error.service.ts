/* =============================================================================
  Rafael Féliz

  When using promises, sometimes you want to use catch to return false if the promise was successful... On this case,
  Sentry can't catch the error because you've already catch it.

  On these cases, you should call this service to catch it, and also to handle firebase not allowed messages

 ============================================================================= */

import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs/operators';
import { LoginService } from 'src/app/auth/login/login.service';
import { TranslateService } from '@ngx-translate/core'

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(private snackBar: MatSnackBar,
    private translateService: TranslateService,
    private loginService: LoginService) { }

  /**
   * Shows a snackbar with a custom message depending on some errors, like PERMISSIONS DENIED
   * @param error
   */
  showCustomErrorMessages(error: any, customSnackMessage?: string): void {

    //Just to show a custom error message
    if (customSnackMessage !== null && customSnackMessage !== '' && typeof customSnackMessage !== 'undefined') {
      this.snackBar.open(customSnackMessage, '', {
        duration: 8000,
        panelClass: 'error'
      });
    }


    if (error) {
      const errorMsj = error + (error.message ? error.message : "");
      const errorCode = error.code ? error.code : "";

      console.error("Error: ", error.message);
      console.info(error);

      //Show a snackbar with a message that you're not allowed
      if (errorMsj.indexOf("PERMISSION_DENIED") > -1 || errorCode.indexOf("permission-denied") > -1) {
        this.translateService.get('general.permissionDenied').pipe(take(1)).subscribe(textValue => {
          this.snackBar.open(textValue, '', {
            duration: 8000,
            panelClass: 'error'
          });
        });
      }

      if (errorMsj.indexOf("credential is no longer valid") > -1) {
        this.translateService.get('general.credentialsNotValid').pipe(take(1)).subscribe(textValue => {
          this.snackBar.open(textValue, '', {
            duration: 8000,
            panelClass: 'error'
          });
        });
      }


      //If firebase client has terminated, for example, if you log without the remember me option, every time you refresh the browser, the firestore
      //instance is cleared, so other tabs will trigger this error.
      if(errorMsj.indexOf("The client has already been terminated") > -1) {
          let reloadStr = "";
          this.translateService.get('general.reload').pipe(take(1)).subscribe(value => reloadStr = value);
          this.translateService.get('general.firebaseClientTerminated').pipe(take(1)).subscribe(textValue => {
            const snack = this.snackBar.open(textValue, reloadStr, {
              duration: 15000000,
              panelClass: 'warning'
            });
            snack.onAction().subscribe(() => {
              this.loginService.logout(true, true, true);
            });
          });
      }
      //Trow an error so that sentry logs it
      //try {throw new Error(error);}catch(e){}
    }
  }
}

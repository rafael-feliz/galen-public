/* =============================================================================
  Rafael Féliz

  Automatically sends errors to Sentry...

  //https://docs.sentry.io/platforms/javascript/enriching-events/user-feedback/
  //https://docs.sentry.io/platforms/javascript/guides/angular/

 ============================================================================= */



import { ErrorHandler, Injectable } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { environment } from "../../../environments/environment";
import { includes } from "lodash";
import * as Sentry from "@sentry/browser";
import { Store } from '@ngrx/store';
import * as fromRoot from '../../store/app.reducer';
import { take } from 'rxjs/operators';
import { ErrorService } from "./error.service";



//Add errors to ignore here
const ignoreStrings = [
  "storage/",
  "auth/",
  "permission/",
  "permission-denied",
  "recaptcha",
  "auth/multi-factor-auth-required",
  "The email address is already in use by another account",
  "The password is invalid or the user does not have a password",
  "Proof of ownership of a second factor",
  "Cannot read property 'style' of null",
  "Missing or insufficient permissions",
  "There is no user record corresponding to this identifier",
  "Unexpected EOF parse@[native code]",
  "AbortError: The connection was closed",
  "The prompt() method must be called with a user gesture",
  "Unexpected EOF",
  "credential is no longer valid",
  "A network error",
  "Unable to load external reCAPTCHA dependencies",
  "The user account has been disabled"
];


Sentry.init({

  dsn: "https://bc63b2ea4b054918816e4ae4cfab41bf@o431862.ingest.sentry.io/5383756", //also change on firebase.json
  // TryCatch has to be configured to disable XMLHttpRequest wrapping, as we are going to handle
  // http module exceptions manually in Angular's ErrorHandler and we don't want it to capture the same error twice.
  // Please note that TryCatch configuration requires at least @sentry/browser v5.16.0.
  integrations: [new Sentry.Integrations.TryCatch({
    XMLHttpRequest: false,
  })],
  environment: environment.production ? "Production" : ( environment.development ? "Development" : "LOCAL" ),

  // We ignore Server Errors. We have to define here since Angular
  // http client uses setTimeout to detect http call progress.
  // And when the call fails, it throws an exception inside that timeout
  // that bubbles up higher to the main Angular's error handler.
  ignoreErrors: ignoreStrings,
  denyUrls: [],
  allowUrls: [
    "my.galen.app",
    "development-my.galen.app"
  ]
});


@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor(private store: Store<fromRoot.State>, private errorService: ErrorService) { }

  extractError(error) {
    // Try to unwrap zone.js error.
    // https://github.com/angular/angular/blob/master/packages/core/src/util/errors.ts
    if (error && error.ngOriginalError) {
      error = error.ngOriginalError;
    }

    // We can handle messages and Error objects directly.
    if (typeof error === "string" || error instanceof Error) {
      return error;
    }

    // If it's http module error, extract as much information from it as we can.
    if (error instanceof HttpErrorResponse) {
      // The `error` property of http exception can be either an `Error` object, which we can use directly...
      if (error.error instanceof Error) {
        return error.error;
      }

      // ... or an`ErrorEvent`, which can provide us with the message but no stack...
      if (error.error instanceof ErrorEvent) {
        return error.error.message;
      }

      // ...or the request body itself, which we can use as a message instead.
      if (typeof error.error === "string") {
        return `Server returned code ${error.status} with body "${error.error}"`;
      }

      // If we don't have any detailed information, fallback to the request message itself.
      return error.message;
    }

    // Skip if there's no error, and let user decide what to do with it.
    return null;
  }

  //-----------------------------

  async handleError(error) {

    await this.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(authState => {


      Sentry.setUser({ email: authState.email });

      //https://docs.sentry.io/platforms/javascript/guides/angular/enriching-events/tags/
      Sentry.setTag("userFullName", authState.fullName ? authState.fullName : "Not available");
      Sentry.setTag("isDoctor", authState.nonDoctor ? "No" : "Yes");
      Sentry.setTag("persistentAuth", authState.authenticationIsPersistent ? "Yes" : "No");
      Sentry.setTag("isAuthenticated", authState.isAuthenticated ? "Authenticated" : "Not authenticated");
      Sentry.setTag("customerId", authState.customerInfo ? authState.customerInfo.customerId : "Not available");
      Sentry.setTag("customerName", authState.customerInfo ? authState.customerInfo.clinicName : "Not available");
      Sentry.setTag("language", authState.customerInfo ? authState.customerInfo.language : "Not available");


      let ignoreError = false;

      if (error) {
        ignoreStrings.forEach(stringToIgnore => {
          if (!ignoreError)
            ignoreError = includes(error.toString(), stringToIgnore);

          if (!ignoreError && error.message)
            ignoreError = includes(error.message.toString(), stringToIgnore);

          if (!ignoreError && error.code)
            ignoreError = includes(error.code.toString(), stringToIgnore);
        });
      }

      if (ignoreError) {
        console.error("%c Ignored error:", "color: green;font-size: 15px");
      }
     this.errorService.showCustomErrorMessages(error); //This will show the error message on the console and/or custom messages on a snackbar


      if (!ignoreError) {
        const extractedError = this.extractError(error) || "Sentry Message: Handled unknown error";

        if (environment.production) {

          Sentry.withScope(scope => {
            scope.setFingerprint([window.location.pathname]);

            //In production mode, log the error to Sentry
            // Capture handled exception and send it to Sentry.
            const eventId = Sentry.captureException(extractedError);
            // Optionally show user dialog to provide details on what happened.
            Sentry.showReportDialog({ eventId });
          });
        }

      }
    });
  }
}

import { Injectable } from '@angular/core';
import { Action, AngularFirestore, DocumentChangeAction, DocumentSnapshot } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { FC_CUSTOMERSDATA, FC_CUSTOMERSDATA_DOCTOR_SPECIALTIES, FC_CUSTOMERSDATA_USERS, GalenICustomerDoctorSpecialties, GalenICustomerUser, GalenIFile } from 'galen-shared';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { CLOUD_FUNCTION_USERGETSECONDFACTORS } from 'src/app/constants/cloud-functions.constants';
import { MiscService } from '../misc/misc.service';
import { ErrorService } from '../errors/error.service';
import { TranslateService } from '@ngx-translate/core'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private afFirestore: AngularFirestore,
    private afFunctions: AngularFireFunctions,
    private miscService: MiscService,
    private errorService: ErrorService,
    private translateService: TranslateService) { }

  // ---------------------------------
  /**Subscribe to changes */
  subscribeToUsersCollection(customerId: string): Observable<GalenICustomerUser[]> {
    //Subscribe to firestore document
    return this.afFirestore.collection<GalenICustomerUser>(`${FC_CUSTOMERSDATA}/${customerId}/${FC_CUSTOMERSDATA_USERS}`).valueChanges();
  }


  // ---------------------------------
  /**Subscribe to user changes */
  subscribeToUserDocumentChanges(customerId: string, userEmail: string): Observable<Action<DocumentSnapshot<GalenICustomerUser>>> {
    //Subscribe to firestore document
    return this.afFirestore.doc<GalenICustomerUser>(`${FC_CUSTOMERSDATA}/${customerId}/${FC_CUSTOMERSDATA_USERS}/${userEmail}`).snapshotChanges();
  }

  // ---------------------------------
  /**
   * Get the logged in user second factur auth methods
   */
  getUserSecondFactorAuthenticationMethods(): Promise<any> {
    return new Promise(resolve => {
      this.afFunctions.httpsCallable(CLOUD_FUNCTION_USERGETSECONDFACTORS)({}).toPromise().then(factors => {
        resolve(factors);
      }).catch(() => {
        resolve(null);
      });
    });
  }

  // ---------------------------------
  /** Enable or disable the user  */
  enableDisable = (userId: string, isEnabling: boolean): Promise<boolean> => {
    const context = this;
    return new Promise(resolve => {
      this.miscService.getCustomerId().pipe(take(1)).subscribe(customerId => {
        const userRef = this.afFirestore.doc<GalenICustomerUser>(`${FC_CUSTOMERSDATA}/${customerId}/${FC_CUSTOMERSDATA_USERS}/${userId}`);
        const userData: GalenICustomerUser = { isDisabled: !isEnabling };

        userRef.update(userData).catch(function (error) {
          context.translateService.get('user.updateError').pipe(take(1)).subscribe(text => {
            context.errorService.showCustomErrorMessages(error, text + error.message);
          });
        });

        //Resolve right away and don't wait the document to finish the action. It will first save on the cache, then on the server.
        resolve(true);

      })
    });

  }


  // ---------------------------------
  /** Delete an user. NOTE: Cloud function will do the rest of the work. */
  delete = (userId: string): Promise<boolean> => {
    const context = this;
    return new Promise(resolve => {
      this.miscService.getCustomerId().pipe(take(1)).subscribe(customerId => {
        const userRef = this.afFirestore.doc<GalenICustomerUser>(`${FC_CUSTOMERSDATA}/${customerId}/${FC_CUSTOMERSDATA_USERS}/${userId}`);
        userRef.delete().catch(function (error) {
          context.translateService.get('user.updateError').pipe(take(1)).subscribe(text => {
            context.errorService.showCustomErrorMessages(error, text + error.message);
          });
        });
        //Resolve right away and don't wait the document to finish the action. It will first save on the cache, then on the server.
        resolve(true);

      })
    });

  }

  // ---------------------------------
  /** Adds or edit the user. Note: There is a cloud function that compensate this action...  */
  addEdit = (user: GalenICustomerUser): Promise<boolean> => {
    const context = this;
    return new Promise(resolve => {
      this.miscService.getCustomerId().pipe(take(1)).subscribe(customerId => {
        const userRef = this.afFirestore.doc<GalenICustomerUser>(`${FC_CUSTOMERSDATA}/${customerId}/${FC_CUSTOMERSDATA_USERS}/${user._email}`);

        userRef.set(user).catch(function (error) {
          context.translateService.get('user.updateError').pipe(take(1)).subscribe(text => {
            context.errorService.showCustomErrorMessages(error, text + error.message);
          });
        });

        //Resolve right away and don't wait the document to finish the action. It will first save on the cache, then on the server.
        resolve(true);

      })
    });

  }


  // ---------------------------------
  /**Subscribe to changes */
  subscribeToDoctorSpecialtiesCollection(customerId: string): Observable<DocumentChangeAction<GalenICustomerDoctorSpecialties>[]> {
    //Subscribe to firestore document
    return this.afFirestore.collection<GalenICustomerDoctorSpecialties>(`${FC_CUSTOMERSDATA}/${customerId}/${FC_CUSTOMERSDATA_DOCTOR_SPECIALTIES}`).snapshotChanges();
  }


}

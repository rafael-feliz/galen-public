/*  ******************************************************************************************************
    Rafael Féliz
    This service is the main app init service, that is called from the app module, and runs before app start (the selected function on app module)

    It will:

    - Trigger the get customer info (gcifu cloud function)
    - Return to login if not authenticated
    - Return to setup customer if customer is not setup
    - After loading customer info, save to the state
    - Subscribe to user info changes (logged in user)

    ******************************************************************************************************
*/
import { Injectable, NgZone } from '@angular/core';
import * as fromRoot from '../store/app.reducer';
import * as authActions from '../auth/store/auth.actions';
import * as globalActions from '../store/global.actions';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { GalenICustomerGetInfoFromUserReturns, GalenFDecrypt, GALENC_ENCRIPTION_GET_CUSTOMER_INFO_KEY, GalenTlanguageTypes, GalenICustomerUser, FC_CUSTOMERSDATA, FC_CUSTOMERSDATA_USERS, GalenIAuthActionsDataParams } from 'galen-shared';
import { take } from 'rxjs/operators';
import { CLOUD_FUNCTION_GET_CHANGE_USER_PASSWORD_LINK, CLOUD_FUNCTION_GET_CUSTOMER_INFO_FROM_USER } from 'src/app/constants/cloud-functions.constants';
import { PATH_AUTH_ACTIONS, PATH_CUSTOMER_SETUP, PATH_LOGIN } from 'src/app/constants/routing-paths.constants';
import { ErrorService } from 'src/app/services/errors/error.service';
import { MiscService } from 'src/app/services/misc/misc.service';
import { LoginService } from '../auth/login/login.service';
import * as _ from 'lodash';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppInitService {

  userInfoSubscription;
  getCustomerInfoForAppInitSubscription;

  constructor(private router: Router,
    private afFunctions: AngularFireFunctions,
    private afFirestore: AngularFirestore,
    private store: Store<fromRoot.State>,
    private ngZone: NgZone,
    private miscService: MiscService,
    private loginService: LoginService,
    private errorService: ErrorService) { }

  //==================================================================================
  /**
   * This is excecuted before the angular app starts, on the app module > initializeApp function. This will run until the promise is resolved.
   * This will subscribe to the auth state and will resolve the promise until the user is logged in and also the customer info is loaded
   */
  getCustomerInfoForAppInit = async (): Promise<void> => {
    const context = this;
    let isAuth;

    return new Promise(resolve => {

      //Watch authentication state modifications. This will be updated after logging in.
      this.getCustomerInfoForAppInitSubscription = context.store.select(fromRoot.getAuthState).subscribe(async (loggedInfo) => {

        //Perform this once (https request from cloud function or local storage values), if (authState > customerInfo) is not set:
        if (loggedInfo.isAuthenticated && loggedInfo.email !== '' && loggedInfo.twoFactorAuthenticationEnrolled) {

          //Just unsubscribe when the customer DATA is loaded, because then this will not be needed anymore.
          //When this is needed is when the user is not logged in, that this should listen to the auth state changes, because this function is called once
          if (this.getCustomerInfoForAppInitSubscription) {
            console.info("Galen App Init - Unsubscribed");
            this.getCustomerInfoForAppInitSubscription.unsubscribe();
          }

          await context.getCustomerInfo().then(() => {
            console.info("Galen App Init - Gcifu success");

            //Tell the auth guard that the customer information was loaded
            this.store.dispatch(new authActions.SetCustomerInfoWasLoaded(true));

            //Resolve and end this to allow the app to start
            resolve();
          }).catch(() => {
            console.info("Galen App Init - Gcifu load error");

            //Tell the auth guard that the customer information was loaded
            this.store.dispatch(new authActions.SetCustomerInfoWasLoaded(true));

            resolve();
          });
        } else {
          if (isAuth !== loggedInfo.isAuthenticated) { //Prevents infinite loop because here we set the state
            isAuth = loggedInfo.isAuthenticated;
            console.info("Galen App Init - Gcifu not available");
            //Tell the auth guard that the customer information was loaded
            this.store.dispatch(new authActions.SetCustomerInfoWasLoaded(true));
          }
          resolve();

        }
      });

    });
  }



  //==================================================================================
  /**
   *  Gets the customer info from the logged in user
   *  To force an update, call this function as this: context.getCustomerInfo(false,false,true);
   * @param redirectToRoot (optional) Set to false if you don't want it to redirect the user to the root path or current url after getting the loggedInUser customer info.
   */

  getCustomerInfo = async (redirectToRoot: boolean = true, showLoadingIndicator: boolean = true, updateStorage: boolean = false): Promise<void> => {
    const context = this;
    //Set loading indicator (full screen)
    if (showLoadingIndicator)
      this.store.dispatch(new globalActions.ShowFullScreenLoader(true));

    return new Promise(async resolve => {

      //------
      //Get the data stored in storage if any
      let cInfFromStorage: GalenICustomerGetInfoFromUserReturns = null;
      try {
        cInfFromStorage = localStorage.getItem("cinf") ? JSON.parse(GalenFDecrypt(localStorage.getItem("cinf"), GALENC_ENCRIPTION_GET_CUSTOMER_INFO_KEY)) : null;
      } catch (error) { context.errorService.showCustomErrorMessages(error); }

      //------
      //GET CUSTOMER INFORMATION FROM CLOUD FUNCTION (IF STORAGE IS NOT SET) OR FROM STORAGE.
      if ((updateStorage) || (!cInfFromStorage)) {
        console.info(`Galen App Init: ${(updateStorage ? "Refreshing: " : "Initial load: ")}Gcifu`);

        //Fetch the customer information from the server and store it in the local storage
        await context.afFunctions.httpsCallable(CLOUD_FUNCTION_GET_CUSTOMER_INFO_FROM_USER)(null).toPromise().then(async response => {

          if (response.success === true) {
            const responseResult: GalenICustomerGetInfoFromUserReturns = { ...JSON.parse(GalenFDecrypt(response.result, GALENC_ENCRIPTION_GET_CUSTOMER_INFO_KEY)) }

            //store the result on the storage. Note: On Login.service.ts this item is deleted when logging in.
            try {
              localStorage.setItem("cinf", response.result); //cinf = customer info
            } catch (error) { context.errorService.showCustomErrorMessages(error); }

            //continue with the process with the data loaded from the cloud function
            await context.storeCustomerInfoInState(redirectToRoot, showLoadingIndicator, { customerInfo: responseResult.customerInfo, billingPlanDetail: responseResult.billingPlanDetail, userRoleId: responseResult.userRoleId, userAccessToClinicsIds: responseResult.userAccessToClinicsIds });

            resolve();

          }
          else if (response.success === false) {
            //The user setup is not complete (customer info missing), ask for setup here

            //Remove loading indicator (full screen)
            if (showLoadingIndicator)
              context.store.dispatch(new globalActions.ShowFullScreenLoader(false));

            if (context.router.url !== '/' + PATH_CUSTOMER_SETUP)
              context.ngZone.run(() => context.router.navigate(['/' + PATH_CUSTOMER_SETUP])); //window.location.href = '/' + PATH_CUSTOMER_SETUP; //Here is needed the window.location.href because route change does not work here context.ngZone.run(() => context.router.navigate(["/"]));


            resolve();
          }
          else {

            // ****** This should never happen on production ****** but it can happen while on development, that the data may be deleted so many times.

            //The user custom claims points to a customer that does not exist. Return him to login
            //and tell the user was deleted.


            //Remove loading indicator (full screen)
            if (showLoadingIndicator)
              context.store.dispatch(new globalActions.ShowFullScreenLoader(false));

            alert(response.result);
            await context.loginService.logout(true, false, false);
            window.location.href = "/" + PATH_LOGIN;
            resolve();
          }
        });

      } else {
        console.info("Galen App Init: Gcifu from storage");
        console.info("**** Your customer ID is: ", cInfFromStorage.customerInfo.customerId);


        //Perform this once (load from storage)
        //continue with the process with the data loaded from storage
        await context.storeCustomerInfoInState(redirectToRoot, showLoadingIndicator, cInfFromStorage);

        //refresh the info from the customer by reloading from the cloud function
        context.getCustomerInfo(false, false, true);
        //resolve();
      }

    });

  }

  //==================================================================================

  wakeFromColdStartGetLoggedInUserCustomerInfo() {
    //Wake to prevent cold start. This function is used on getCustomerInfo, only for the login page.
    this.afFunctions.httpsCallable(CLOUD_FUNCTION_GET_CUSTOMER_INFO_FROM_USER)({ wakeUp: true }).toPromise().then();
  }

  //==================================================================================
  /**
   * This is a continuation of: getCustomerInfo.
   * After reading the customer info, after the user has logged in, this will perform the actions needed after reading these information.
   * @param redirectToRoot (optional) Set to false if you don't want it to redirect the user to the root path or current url after getting the loggedInUser customer info.
   */
  storeCustomerInfoInState = async (redirectToRoot: boolean = true, showLoadingIndicator: boolean = true, customerInfo: GalenICustomerGetInfoFromUserReturns): Promise<void> => {
    const context = this;

    return new Promise<void>(async resolve => {
      //Store the customer info in the auth state
      await context.store.dispatch(new authActions.SetCustomerInfo({ customerInfo: customerInfo.customerInfo }));

      //Store the billing detail in the auth state. If billing is not set, it means you're in local environment and the firestore DB is empty, and the cloud function will auto add the billing plans
      if(customerInfo.billingPlanDetail.quotas)
        await context.store.dispatch(new authActions.SetBillingPlanDetails({ billingPlanDetails: customerInfo.billingPlanDetail }));
      else {
        //This should only happen in local dev environment, because the emulator's DB is empty by default
        console.info("Billing plans not available when this function was called... (Local environment firestore empty db) Retrying...");

        //Force refresh customer info and billing again
        setTimeout(() =>{
          context.getCustomerInfo(false,true,true);
        },100);
      }

      //Confirm the state was updated by subscribing for the next value only
      context.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(async authState => {

        //Remove loading indicator (full screen)
        if (showLoadingIndicator)
          this.store.dispatch(new globalActions.ShowFullScreenLoader(false));


        //Assign quotas state. This function will run in the background. It run and be refreshed every time the upload modal is open, or when you run manually.
        this.miscService.getCustomerUsedAndAdditionalQuotas();

        //------------------
        //Get Language, date/time format from the customer.
        //This will be overridden if the user has some of these set. It will be overridden on the subscribeToUserInfo function
        const dateFormatId = authState.customerInfo.dateFormatId;
        const timeFormatId = authState.customerInfo.timeFormatId;
        const language = authState.customerInfo.language as GalenTlanguageTypes;
        const roleId = customerInfo.userRoleId;
        const accessToClinicsIds = customerInfo.userAccessToClinicsIds;
        const roleActionsPermissions = customerInfo.userRoleActionsPermissions;

        let authStateToModify = { ...authState };
        authStateToModify.language = language;
        authStateToModify.dateFormatId = dateFormatId;
        authStateToModify.timeFormatId = timeFormatId;
        authStateToModify.roleId = roleId;
        authStateToModify.accessToClinicsIds = accessToClinicsIds;
        authStateToModify.roleActionsPermissions = roleActionsPermissions;


        //Update the state with defaults from customer.
        await context.store.dispatch(new authActions.SetUserInfo(authStateToModify));

        //Redirect the user to the app
        if (redirectToRoot) {
          if (context.router.url == '/' + PATH_LOGIN)
            //If current route is login, then redirect to the root
            context.ngZone.run(() => context.router.navigate(["/"]));
          else {
            //If the route is not the login route, reload route so that AUTH GUARD CAN APPLY METHOD refresh
            context.ngZone.run(() => context.router.navigateByUrl(context.router.url));
          }
        }
        resolve();
      });
    });

  }

  //==================================================================================
  /**Get the user info in real time
   * This is used to have the user info in the app wide state. Note: this is not customer Info, just user info.
   * Also apply the date/time format, language from user, or from customer.
   *
   * Note: It will first subscribe to the auth state to know when the user customer info is available (i.eif on login page, it is not set)
   *       Then it subscribe to realtime updates from the database, but after a realtime update comes, it reads the state again to also get updated data from it and then merge both
   */
  subscribeToUserInfo = async () => {
    const context = this;


    // ----------------------------------------------
    //SET LOGIN PERSISTENCE ON THE STATE
    // local storage item "lpers" is set  on the login method on this file. This will only set when the user logs in, after he logs in, that method is not
    // called if the login state is PERSISTENT, so this is why we store it on the storage so that we know that the authentication was persistent or not
    if (localStorage.getItem("lpers") === "1") {
      await this.store.dispatch(new authActions.SetAuthPersistence({ authenticationIsPersistent: true }));
    } else {
      await this.store.dispatch(new authActions.SetAuthPersistence({ authenticationIsPersistent: false }));
    }

    // ----------------------------------------------
    //Read the auth state, to get the customer ID and the user ID.
    //It subscribes because it may have not been set when this function is called (For example, on the login view), so this should be here.

    this.store.select(fromRoot.getAuthState).subscribe(async authState => {


      if (authState.customerInfo) {
        //------------------
        //If user info was previously subscribed, return and don't subscribe again
        if (context.userInfoSubscription) {
          return;
        }

        //------------------
        //Subscribe to realtime updates on the user info.
        //This will run only after an update from the DB
        //This is where the realtime user updates comes from.
        //Note: Roles are not read from the user collection directly here for security. instead from getUserInfoFromUser (Cloud function)
        if (context.afFirestore) {
          context.userInfoSubscription = context.afFirestore.doc<GalenICustomerUser>(`${FC_CUSTOMERSDATA}/${authState.customerInfo.customerId}/${FC_CUSTOMERSDATA_USERS}/${authState.email}`)
            .snapshotChanges().subscribe(async userInfoSnap => {

              //------------------
              //update the state with new values
              if (userInfoSnap) {

                //------------------
                //Update the state with user information, but load the state here because the user info can change at any
                //time and I should update the state values with also the latest state changes.
                await context.store.select(fromRoot.getAuthState).pipe(take(1)).subscribe(async authStateForUserInfo => {

                  if (authStateForUserInfo.customerInfo && authStateForUserInfo.isAuthenticated) {

                    const userInfo: GalenICustomerUser = userInfoSnap.payload.data() as GalenICustomerUser;

                    if (userInfo) {

                      //------------------
                      //Get Language, date/time format from the customer... See next code,
                      //that will read this but from the userInfo, because the data on the user have more importance than
                      //from the customer, in case the user override the customer definition (i.e. timezone, dateformat, language, etc).
                      let dateFormatId = authStateForUserInfo.customerInfo.dateFormatId;
                      let timeFormatId = authStateForUserInfo.customerInfo.timeFormatId;
                      let language: GalenTlanguageTypes = authStateForUserInfo.customerInfo.language as GalenTlanguageTypes;

                      //------------------
                      //OVERRIDE
                      //Check for language, date/time format from the user, to see if there is some to override from the customer
                      dateFormatId = userInfo.dateFormatId ? userInfo.dateFormatId : dateFormatId;
                      timeFormatId = userInfo.timeFormatId ? userInfo.timeFormatId : timeFormatId;
                      language = userInfo.language ? userInfo.language as GalenTlanguageTypes : language;


                      //------------------
                      //Update the state
                      let userInfoToModify = { ...authStateForUserInfo };

                      userInfoToModify.firstName = userInfo.fullName ? userInfo.fullName.split(" ")[0] : "";
                      userInfoToModify.fullName = userInfo.fullName;
                      userInfoToModify.photo = userInfo.photo;
                      userInfoToModify.phone = userInfo.phone;
                      userInfoToModify.language = language;
                      userInfoToModify.dateFormatId = dateFormatId;
                      userInfoToModify.timeFormatId = timeFormatId;
                      userInfoToModify._settings_notificationsCount = userInfo._settings_notificationsCount;
                      userInfoToModify._settings_selectedClinicId = userInfo._settings_selectedClinicId;
                      userInfoToModify._settings_autoDisplayInstallAppButton = userInfo._settings_autoDisplayInstallAppButton;
                      userInfoToModify.shouldResetPassword = userInfo.shouldResetPassword;

                      await context.store.dispatch(new authActions.SetUserInfo(userInfoToModify));

                      //-------------------
                      //Check whether the user should change the password or not
                      if (authStateForUserInfo.isAuthenticated &&
                        userInfoToModify.shouldResetPassword === true &&
                        userInfo.shouldResetPassword !== authStateForUserInfo.shouldResetPassword &&
                        window.location.href.indexOf(PATH_AUTH_ACTIONS) < 0) {

                        const params: GalenIAuthActionsDataParams = {
                          email: userInfoToModify.email,
                          lang: userInfoToModify.language,
                          name: userInfoToModify.firstName,
                          type: "passwordReset"
                        }

                        //Get the reset password link, and redirect.
                        await context.afFunctions.httpsCallable(CLOUD_FUNCTION_GET_CHANGE_USER_PASSWORD_LINK)(params).toPromise().then(async link => {
                          if (link && link != "") {
                            await this.loginService.logout(true, false, true);
                            window.location.href = link;
                            //context.ngZone.run(() => context.router.navigateByUrl(_.replace(link, environment.appDomain, "")));
                          }
                        });
                      }
                      //------------
                    }
                  }
                });
              }
            });
        }
      }
    })
  };
}

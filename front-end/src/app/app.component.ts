import { Component, HostListener, OnInit } from '@angular/core';
import { LoginService } from './auth/login/login.service';
import { ServiceWorkerUpdateNotificationService } from './services/service-worker-update-notification/service-worker-update-notification.service';
import { NavigationEnd, NavigationStart, NavigationError, NavigationCancel, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as fromRoot from './store/app.reducer';
import * as globalActions from './store/global.actions';
import { AppInitService } from './services/app-init.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  constructor(private loginService: LoginService,
    private serviceWorkerUpdateNotificationService: ServiceWorkerUpdateNotificationService,
    private router: Router,
    private store: Store<fromRoot.State>,
    private appInitService:AppInitService) { }

  loading;
  showLoadingGalenLogo = true;
  showLoadingSpinner = false;
  initialLoad = true;
  spinnerTimer;
  showLoaderOnRouteChange = true;
  hasPendingWrites = true;
  isDevelopment = false;
  isLocal = false;

  // =====================================
  // START EVENTS LISTENER

  // ----------------------
  /**Listen to windows updates */
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.onWindowResize();
  }

  // Detect browser resize changes and save to the store whether the browser is in fullscreen state or not

  onWindowResize = () => {
    if (window.innerHeight == screen.height) {
      this.store.dispatch(new globalActions.SetIsFullScreen(true));
    } else {
      this.store.dispatch(new globalActions.SetIsFullScreen(false));
    }
  }

  // ----------------------
  /** Listen to the event that handles the INSTALL PWA APP (To add the app to home screen and install like a native app). This event should be located in here, not on his componennt due to issues. */
  @HostListener('window:beforeinstallprompt', ['$event'])
  onbeforeinstallprompt(e) {
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    e.preventDefault();

    // Store the event so it can be triggered later.
    this.store.dispatch(new globalActions.SetBrowserInstallButtonEvent(e));
  }


  // ----------------------
  /** Fires before closing or reloading the browser  */
  @HostListener('window:beforeunload', ['$event'])
  async unloadHandler(event: Event) {
    console.log("Closing Galen App");

    //when the user is using the app with no "Remember me" option on the login page, This ensures removing the offline data before closing the browser
    if (localStorage.getItem("lpers") !== "1") {
        await this.loginService.logout(true, true, true);
    }
  }

  // END EVENTS LISTENER
  // =====================================


  async ngOnInit() {

    if(environment.local) {
      this.isLocal = true;
    }
    if(environment.development) {
      this.isDevelopment = true;
    }

    this.onWindowResize();

    //subscribe to user changes
    this.appInitService.subscribeToUserInfo();

    //This will detect whether there is a new app version or not, and notify the user
    this.serviceWorkerUpdateNotificationService;

    //Handle full screen loading indicator
    this.handleLoadingIndicator();

    //--------------------
    this.detectRouteChanges();


    //--------------------
    this.store.select(fromRoot.getAuthState).subscribe(authState => {
        this.showLoaderOnRouteChange = !(authState.isAuthenticated && (authState.customerInfo && authState.customerInfo.customerId !== ""));
    });
  }


  //=====================================
  //Just handles the full screen loader
  handleLoadingIndicator() {

    //Prevent changing this value too fast that it doesnt take time to display the first time.
    setTimeout(() => { this.initialLoad = false; }, 2000);

    //Show/hide the full screen loader
    const context = this;
    this.store.select(fromRoot.getShowFullScreenLoader).subscribe(loading => {
      this.loading = loading;

      //Show the loading spinner after elapsed time. The G from Galen is always displayed by default.
      clearTimeout(this.spinnerTimer);
      this.spinnerTimer = setTimeout(() => { context.showLoadingSpinner = loading; }, 800);
    });

  }


  //=====================================
  //To detect route changes, and modify the state. (NavigationEnd - NavigationCancel - NavigationError - RoutesRecognized)
  //Also it show the loader full screen but it does not show the spinner

  routeChangesObservableStarted = false; //Prevent calling twice.

  detectRouteChanges = () => {
    if (!this.routeChangesObservableStarted) {
      this.routeChangesObservableStarted = true;

      this.router.events.forEach(async (event) => {

        //Set state that route was changed
        if (event instanceof NavigationStart) {
          this.store.dispatch(new globalActions.RouteIsChanging());
          //Set loading indicator (full screen)
          //DONT remove this from here, because if you do so, it will affect pages that does not require authenticated user, i.e. Login, not found, etc.
          if(this.showLoaderOnRouteChange)
            this.store.dispatch(new globalActions.ShowFullScreenLoader(true));
        }

        //Store that the route was changed
        if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError)
          setTimeout(async () => {
            this.store.dispatch(new globalActions.RouteWasChanged());
            //Set loading indicator (full screen)
            //DONT remove this from here, because if you do so, it will affect pages that does not require authenticated user, i.e. Login, not found, etc.
            if(this.showLoaderOnRouteChange)
              this.store.dispatch(new globalActions.ShowFullScreenLoader(false));
          }, 200);

      });
    }
  }

}
